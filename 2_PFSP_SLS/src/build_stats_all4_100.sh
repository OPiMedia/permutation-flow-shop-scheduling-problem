#!/bin/sh

# Rebuild stats for algo 4 on all instances of size 100 (~16 minutes)

./build_stats_algo.sh '--init-srz --insert-permutation --first --max-duration 31' 100 ../stats_results/100/stats-algo4-1.txt
