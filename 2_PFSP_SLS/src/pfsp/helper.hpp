/* -*- coding: latin-1 -*- */

/** \file pfsp/helper.hpp (May 15, 2017)
 * \brief
 * Helper functions.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef SRC_PFSP_HELPER_HPP_
#define SRC_PFSP_HELPER_HPP_

#include <cassert>

#include <string>

#include "times.hpp"


namespace pfsp {

  /* ************
   * Prototypes *
   **************/
  /** \brief
   * Read the file bests_filename
   * to get and return the best known WCT
   * for the instance in the file instance_filename.
   *
   * If not found
   * then return 0.
   *
   * @param bests_filename
   * @param instance_filename
   *
   * @return the best known WCT or 0 if not found
   */
  time_type
  get_best_known_wct(std::string bests_filename, std::string instance_filename);



  /* ******************
   * Inline functions *
   ********************/
  inline
  double
  deviation(time_type wct, time_type best_known_wct) {
    assert(best_known_wct > 0);

    return ((static_cast<double>(wct) - static_cast<double>(best_known_wct))
            * 100 / best_known_wct);
  }


  /**
   * @param first < last
   * @param last
   *
   * @return random integer between first and last, included
   */
  inline
  unsigned int
  rand_between(unsigned int first, unsigned int last) {
    assert(first <= last);

    // There is an additional bias
    // when (last + 1 - first) is not a divisor of (RAND_MAX + 1)
    return first + rand() % (last + 1 - first);
  }

}  // namespace pfsp

#endif  // SRC_PFSP_HELPER_HPP_
