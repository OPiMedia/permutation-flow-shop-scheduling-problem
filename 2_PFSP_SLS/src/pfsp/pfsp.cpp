/* -*- coding: latin-1 -*- */

/** \file pfsp/pfsp.cpp (May 17, 2017)
 * \brief Class for Flow Shop Scheduling Problem (PFSP-WCT)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>

#include "helper.hpp"
#include "pfsp.hpp"


// To test acceleration with only partial recalculation of table
// #define ALL_RECALCULATE


namespace pfsp {

  /* ******************
   * Global constants *
   ********************/
  const std::string init_str[5] = {"identity", "reverse identity",
                                   "random", "sorted", "simplified RZ"};

  const std::string neighborhood_str[6] = {"transpose", "exchange", "insert",
                                           "VND transpose-exchange-insert",
                                           "VND transpose-insert-exchange",
                                           "insert-permutation"};

  const std::string pivoting_str[4] = {"first-improvement", "best-improvement",
                                       "nothing", "tabu-search"};



  /* *******************
   * Private functions *
   *********************/
  /** \brief
   * Add the item to tabu.
   *
   * If tabu exceed the tabu_tenure
   * then removes the oldest item.
   *
   * @param tabu_list
   * @param tabu_set
   * @param tabu_tenure
   * @param tabu_item
   */
  void
  tabu_add_(std::list<tabu_item_type> &tabu_list,
            std::set<tabu_item_type> &tabu_set,
            unsigned int tabu_tenure,
            tabu_item_type tabu_item) {
    // Add to tabu
    if (tabu_list.size() >= tabu_tenure) {
      tabu_set.erase(tabu_set.find(*tabu_list.begin()));
      tabu_list.pop_front();
    }

    assert(tabu_list.size() == tabu_set.size());
    assert(tabu_set.find(tabu_item) == tabu_set.end());

    if (tabu_tenure > 0) {
      tabu_set.insert(tabu_item);
      tabu_list.push_back(tabu_item);
    }

    assert(tabu_list.size() == tabu_set.size());
  }


  /** \brief
   * Swap randomly two items
   * until the result will be not tabu.
   *
   * The tabu item of the last result is added to tabu. 
   *
   * @param permutation properly initialized
   * @param tabu_list
   * @param tabu_set
   * @param tabu_tenure
   *
   * @return the minimum modified index of the permutation
   */
  unsigned int
  permutation_exchange_random_tabu_(permutation_type &permutation,
                                    std::list<tabu_item_type> &tabu_list,
                                    std::set<tabu_item_type> &tabu_set,
                                    unsigned int tabu_tenure) {
    const unsigned int size = permutation.size() - 1;

    tabu_item_type tabu_item;
    unsigned int min_i = permutation.size();
    unsigned int nb_repeat = size;

    do {
      const unsigned int i = rand_between(1, size - 1);
      const unsigned int j = i + rand_between(1, size - i);

      assert(1 <= i);
      assert(i < size);
      assert(i < j);
      assert(j <= size);

      min_i = std::min(min_i, i);

      const auto it_i = permutation.begin() + i;
      const auto it_j = permutation.begin() + j;

      tabu_item = *it_i;
      permutation_exchange(it_i, it_j);
    } while ((tabu_set.find(tabu_item) != tabu_set.end()) && (--nb_repeat > 0));

    if (nb_repeat > 0) {
      tabu_add_(tabu_list, tabu_set, tabu_tenure, tabu_item);
    }

    return min_i;
  }


  /** \brief
   * Removed and insert randomly one item
   * until the result will be not tabu.
   *
   * The tabu item of the last result is added to tabu. 
   *
   * @param permutation properly initialized
   * @param tabu_list
   * @param tabu_set
   * @param tabu_tenure
   *
   * @return the minimum modified index of the permutation
   */
  unsigned int
  permutation_insert_random_tabu_(permutation_type &permutation,
                                  std::list<tabu_item_type> &tabu_list,
                                  std::set<tabu_item_type> &tabu_set,
                                  unsigned int tabu_tenure) {
    const unsigned int size = permutation.size() - 1;

    tabu_item_type tabu_item;
    unsigned int min_i = permutation.size();
    unsigned int nb_repeat = size;

    do {
      const unsigned int i = rand_between(1, size);
      unsigned int j = rand_between(1, size - 1);

      if (i <= j) {
        ++j;
      }

      assert(1 <= i);
      assert(i <= size);
      assert(1 <= j);
      assert(j <= size);
      assert(i != j);

      min_i = std::min(min_i, i);

      const auto it_i = permutation.begin() + i;
      const auto it_j = permutation.begin() + j;

      tabu_item = *it_i;
      permutation_insert(it_i, it_j);
    } while ((tabu_set.find(tabu_item) != tabu_set.end()) && (--nb_repeat > 0));

    if (nb_repeat > 0) {
      tabu_add_(tabu_list, tabu_set, tabu_tenure, tabu_item);
    }

    return min_i;
  }



  /* *********
   * Methods *
   ***********/
  void
  Pfsp::calculate_completion_times_table
  (const permutation_type &permutation,
   completion_times_table_type &completion_times_table) const {
    permutation_type::const_iterator it_pi = permutation.cbegin() + 1;  // pi(1)

    // Job 1
    {
      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(1), j}
      auto it = it_job->begin() + 1;                // C_{pi(1), 1}

      auto it_ptime
        = (processing_times_.begin() + *it_pi)->begin() + 1;  // p_{pi(1), 1}

      *it = *it_ptime;  // C_{pi(1), 1} = p_{pi(1), 1}

      // j = 2...
      for (++it_ptime, ++it; it != it_job->end(); ++it_ptime, ++it) {
        // C_{pi(1), j} = C_{pi(1), j-1} + p_{pi(1), j}
        *it = *(it - 1) + *it_ptime;
      }
    }

    // Job 2 to nb_job_, i = 2...
    for (++it_pi; it_pi != permutation.end(); ++it_pi) {
      // C_{pi(i-1), 1}
      auto it_prev
        = (completion_times_table.begin() + *(it_pi - 1))->begin() + 1;

      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(i), j}
      auto it = it_job->begin() + 1;                // C_{pi(i), 1}

      // p_{pi(i), 1}
      auto it_ptime = (processing_times_.begin() + *it_pi)->begin() + 1;

      // C_{pi(i), 1} = C_{pi(i-1), 1} + p_{pi(i), 1}
      *it = *it_prev + *it_ptime;

      // j = 2...
      for (++it_ptime, ++it_prev, ++it; it != it_job->end();
           ++it_ptime, ++it_prev, ++it) {
        // C_{pi(i), j} = max(C_{pi(i-1), j}, C_{pi(i), j-1}) + p_{pi(i), j}
        *it = std::max(*it_prev, *(it - 1)) + *it_ptime;
      }
    }
  }


  void
  Pfsp::iterative_best_improvement_exchange(permutation_type &permutation)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;

      // Get *the* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi1 = last_it_pi;
           it_pi1 + 1 != permutation.end(); last_it_pi = it_pi1, ++it_pi1) {
        for (auto it_pi2 = it_pi1 + 1; it_pi2 != permutation.end(); ++it_pi2) {
          // Try exchange
          permutation_exchange(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (best_wct > trial_wct) {  // better, save it
            best_wct = trial_wct;
            std::copy(permutation.cbegin(), permutation.cend(), best.begin());
            improved = true;
          }

          // Go back to current permutation
          permutation_exchange(it_pi1, it_pi2);
        }
      }

      if (improved) {  // get better
        std::copy(best.cbegin(), best.cend(), permutation.begin());
      }
    } while (improved);

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_best_improvement_insert(permutation_type &permutation)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;

      // Get *the* better permutation
      auto last_it_pi1 = permutation.begin() + 1;  // last needed 1

      for (auto it_pi1 = last_it_pi1;
           it_pi1 != permutation.end(); last_it_pi1 = it_pi1, ++it_pi1) {
        auto last_it_pi2 = permutation.begin() + 1;  // last needed 2

        for (auto it_pi2 = last_it_pi2;
             it_pi2 != permutation.end(); last_it_pi2 = it_pi2, ++it_pi2) {
          if (it_pi1 == it_pi2) {
            continue;
          }

          // Try insert
          permutation_insert(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation,
                                             std::min(last_it_pi1, last_it_pi2),
                                             ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (best_wct > trial_wct) {  // better, save it
            best_wct = trial_wct;
            std::copy(permutation.cbegin(), permutation.cend(), best.begin());
            improved = true;
          }

          // Go back to current permutation
          permutation_insert(it_pi2, it_pi1);
        }
      }

      if (improved) {  // get better
        std::copy(best.cbegin(), best.cend(), permutation.begin());
      }
    } while (improved);

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_best_improvement_transpose(permutation_type &permutation)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;

      // Get *the* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi = last_it_pi;
           it_pi + 1 != permutation.end(); last_it_pi = it_pi, ++it_pi) {
        // Try transposition
        permutation_transpose(it_pi);

#ifdef ALL_RECALCULATE
        calculate_completion_times_table(permutation, ctable);
#else
        recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

        const time_type trial_wct = weighted_sum_completion_times(ctable);

        if (best_wct > trial_wct) {  // better, save it
          best_wct = trial_wct;
          std::copy(permutation.cbegin(), permutation.cend(), best.begin());
          improved = true;
        }

        // Go back to current permutation
        permutation_transpose(it_pi);
      }

      if (improved) {  // get better
        std::copy(best.cbegin(), best.cend(), permutation.begin());
      }
    } while (improved);

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_first_improvement_exchange(permutation_type &permutation,
                                             unsigned int max_nb_loop)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    unsigned int nb_loop = 0;

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type current_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;
      ++nb_loop;

      // Get *each* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi1 = last_it_pi;
           it_pi1 + 1 != permutation.end(); last_it_pi = it_pi1, ++it_pi1) {
        for (auto it_pi2 = it_pi1 + 1; it_pi2 != permutation.end(); ++it_pi2) {
          // Try exchange
          permutation_exchange(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (current_wct > trial_wct) {  // better, get it
            current_wct = trial_wct;
            improved = true;
          }
          else {                          // go back to current permutation
            permutation_exchange(it_pi1, it_pi2);
          }
        }
      }
    } while (improved && (nb_loop < max_nb_loop));

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_first_improvement_insert(permutation_type &permutation,
                                           unsigned int max_nb_loop)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    unsigned int nb_loop = 0;

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type current_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;
      ++nb_loop;

      // Get *each* better permutation
      auto last_it_pi1 = permutation.begin() + 1;  // last needed 1

      for (auto it_pi1 = last_it_pi1;
           it_pi1 != permutation.end(); last_it_pi1 = it_pi1, ++it_pi1) {
        auto last_it_pi2 = permutation.begin() + 1;  // last needed 2

        for (auto it_pi2 = last_it_pi2; it_pi2 != permutation.end(); ++it_pi2) {
          if (it_pi1 == it_pi2) {
            continue;
          }

          // Try insert
          permutation_insert(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation,
                                             std::min(last_it_pi1, last_it_pi2),
                                             ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (current_wct > trial_wct) {  // better, get it
            current_wct = trial_wct;
            last_it_pi2 = it_pi2 + 1;
            improved = true;
          }
          else {                          // go back to current permutation
            permutation_insert(it_pi2, it_pi1);
            last_it_pi2 = it_pi2;
          }
        }
      }
    } while (improved && (nb_loop < max_nb_loop));

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_first_improvement_insert_permutation
  (permutation_type &permutation,
   unsigned int start_zone_length,
   unsigned int max_duration,
   time_type best_known_wct,
   double quality_deviation) const {
    assert(pfsp::permutation_is_valid(permutation));

    const unsigned int size = permutation.size() - 1;
    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    const std::chrono::steady_clock::time_point
      start(std::chrono::steady_clock::now());

    for (unsigned int zone_length = start_zone_length; zone_length <= size;
         ++zone_length) {
    next_step:

      if ((std::chrono::duration<double>(std::chrono::steady_clock::now()
                                         - start).count() >= max_duration)
          || ((best_known_wct > 0)
              && (quality_deviation > 0)
              && (deviation(best_wct, best_known_wct) <= quality_deviation))) {
        // Duration or quality deviation reached
        //             (if best solution is known and one quality asked)

        break;
      }

      // Get the first better solution in the variable neighborhood
      auto last_it_pi1 = permutation.begin() + 1;  // last needed 1

      for (auto it_pi1 = last_it_pi1;
           it_pi1 != permutation.end(); last_it_pi1 = it_pi1, ++it_pi1) {
        auto last_it_pi2 = permutation.begin() + 1;  // last needed 2

        for (auto it_pi2 = last_it_pi2;
             it_pi2 != permutation.end(); last_it_pi2 = it_pi2, ++it_pi2) {
          if (it_pi1 == it_pi2) {
            continue;
          }

          if (zone_length == start_zone_length) {
            // Try insert
            permutation_insert(it_pi1, it_pi2);

            recalculate_completion_times_table(permutation,
                                               std::min(last_it_pi1,
                                                        last_it_pi2),
                                               ctable);

            const time_type trial_wct = weighted_sum_completion_times(ctable);

            if (best_wct > trial_wct) {  // better, save it
              best_wct = trial_wct;

              goto next_step;
            }

            // Go back to current solution
            permutation_insert(it_pi2, it_pi1);
          }


          const auto zone_begin = std::min(it_pi1, it_pi2);
          const unsigned int distance
            = std::distance(zone_begin,
                            std::max(it_pi1, it_pi2) + 1);

          if (distance > 2) {
            // Try each permutation in the inner zone
            // of length at most zone_length between it_pi1 and it_pi2
            const unsigned int zone_distance = std::min(zone_length,
                                                        distance);
            const auto it_zone_end = zone_begin + zone_distance;
            const permutation_type zone_keep(zone_begin, it_zone_end);

            std::sort(zone_begin, it_zone_end);  // set the first permutation

            while (std::next_permutation(zone_begin, it_zone_end)) {
              recalculate_completion_times_table(permutation,
                                                 std::min(last_it_pi1,
                                                          last_it_pi2),
                                                 ctable);

              const time_type trial_wct
                = weighted_sum_completion_times(ctable);

              if (best_wct > trial_wct) {  // better, save it
                best_wct = trial_wct;
                zone_length = start_zone_length;

                goto next_step;
              }

              if (std::chrono::duration<double>
                  (std::chrono::steady_clock::now() - start).count()
                  >= max_duration) {
                // Duration reached

                break;
              }
            }

            // Go back to current solution
            std::move(zone_keep.cbegin(), zone_keep.cend(), zone_begin);
          }
        }
      }
    }

    assert(pfsp::permutation_is_valid(permutation));

    // INV: permutation is a global optimal solution
  }


  void
  Pfsp::iterative_first_improvement_transpose(permutation_type &permutation,
                                              unsigned int max_nb_loop)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    unsigned int nb_loop = 0;

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type current_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;
      ++nb_loop;

      // Get *each* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi = last_it_pi; it_pi + 1 != permutation.end(); ++it_pi) {
        // Try transposition
        permutation_transpose(it_pi);

#ifdef ALL_RECALCULATE
        calculate_completion_times_table(permutation, ctable);
#else
        recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

        const time_type trial_wct = weighted_sum_completion_times(ctable);

        if (current_wct > trial_wct) {  // better, get it
          current_wct = trial_wct;
          last_it_pi = it_pi + 1;
          improved = true;
        }
        else {                          // go back to current permutation
          permutation_transpose(it_pi);
          last_it_pi = it_pi;
        }
      }
    } while (improved && (nb_loop < max_nb_loop));

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_tabu_exchange(permutation_type &permutation,
                                unsigned int tabu_tenure,
                                unsigned int nb_worst_without_perturbation,
                                unsigned int max_duration,
                                time_type best_known_wct,
                                double quality_deviation)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    permutation_type best(permutation);
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type wct = weighted_sum_completion_times(ctable);
    time_type best_wct(wct);

    std::list<tabu_item_type> tabu_list;
    std::set<tabu_item_type> tabu_set;

    unsigned int nb_worst = 0;

    const std::chrono::steady_clock::time_point
      start(std::chrono::steady_clock::now());

    do {
      best_exchange_tabu_(permutation, ctable, wct,
                          tabu_list, tabu_set, tabu_tenure);

      // INV: permutation is better on the neighborhood\tabu
      //      or all neighborhood was tabu

      if (best_wct > wct) {  // get this better
        best_wct = wct;
        std::copy(permutation.cbegin(), permutation.cend(), best.begin());
      }
      else {                 // no better found
        ++nb_worst;

        if (nb_worst > nb_worst_without_perturbation) {
          // Perturbation: random exchange
          nb_worst = 0;

          const unsigned int min_i
            = permutation_exchange_random_tabu_(permutation,
                                                tabu_list, tabu_set,
                                                tabu_tenure);

          recalculate_completion_times_table(permutation,
                                             permutation.begin() + min_i,
                                             ctable);
        }
      }

      if ((best_known_wct > 0)
          && (quality_deviation > 0)
          && (deviation(wct, best_known_wct) <= quality_deviation)) {
        // Quality deviation reached
        // (if best solution is known and one quality asked)
        break;
      }
    } while (std::chrono::duration<double>(std::chrono::steady_clock::now()
                                           - start).count() < max_duration);

    std::copy(best.cbegin(), best.cend(), permutation.begin());

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_tabu_insert(permutation_type &permutation,
                              unsigned int tabu_tenure,
                              unsigned int nb_worst_without_perturbation,
                              unsigned int max_duration,
                              time_type best_known_wct,
                              double quality_deviation)
    const {
    assert(pfsp::permutation_is_valid(permutation));

    permutation_type best(permutation);
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type wct = weighted_sum_completion_times(ctable);
    time_type best_wct(wct);

    std::list<tabu_item_type> tabu_list;
    std::set<tabu_item_type> tabu_set;

    unsigned int nb_worst = 0;

    const std::chrono::steady_clock::time_point
      start(std::chrono::steady_clock::now());

    do {
      best_insert_tabu_(permutation, ctable, wct,
                        tabu_list, tabu_set, tabu_tenure);

      // INV: permutation is better on the neighborhood\tabu
      //      or all neighborhood was tabu

      if (best_wct > wct) {  // get this better
        best_wct = wct;
        std::copy(permutation.cbegin(), permutation.cend(), best.begin());
      }
      else {                 // no better found
        ++nb_worst;

        if (nb_worst > nb_worst_without_perturbation) {
          // Perturbation: random insert
          nb_worst = 0;

          const unsigned int min_i
            = permutation_insert_random_tabu_(permutation,
                                              tabu_list, tabu_set,
                                              tabu_tenure);

          recalculate_completion_times_table(permutation,
                                             permutation.begin() + min_i,
                                             ctable);
        }
      }

      if ((best_known_wct > 0)
          && (quality_deviation > 0)
          && (deviation(wct, best_known_wct) <= quality_deviation)) {
        // Quality deviation reached
        // (if best solution is known and one quality asked)
        break;
      }
    } while (std::chrono::duration<double>(std::chrono::steady_clock::now()
                                           - start).count() < max_duration);

    std::copy(best.cbegin(), best.cend(), permutation.begin());

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_vnd_first_improvement_tranpose_exchange_insert
  (permutation_type &permutation) const {
    /*
      About goto:
      * https://xkcd.com/292/ ;-)
      * Frank Rubin, ""GOTO Considered Harmful" Considered Harmful", ACM 1987:
      https://web.archive.org/web/20090320002214/http://www.ecn.purdue.edu/ParaMount/papers/rubin87goto.pdf
    */

    assert(pfsp::permutation_is_valid(permutation));

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

  loop:
    // Transpose
    iterative_first_improvement_transpose(permutation, 1);

    {
      // If improved then restart with this neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Exchange
    iterative_first_improvement_exchange(permutation, 1);

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Insert
    iterative_first_improvement_exchange(permutation, 1);

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::iterative_vnd_first_improvement_tranpose_insert_exchange
  (permutation_type &permutation) const {
    /*
      About goto:
      * https://xkcd.com/292/ ;-)
      * Frank Rubin, ""GOTO Considered Harmful" Considered Harmful", ACM 1987:
      https://web.archive.org/web/20090320002214/http://www.ecn.purdue.edu/ParaMount/papers/rubin87goto.pdf
    */

    assert(pfsp::permutation_is_valid(permutation));

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

  loop:
    // Transpose
    iterative_first_improvement_transpose(permutation, 1);

    {
      // If improved then restart with this neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Insert
    iterative_first_improvement_exchange(permutation, 1);

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Exchange
    iterative_first_improvement_exchange(permutation, 1);

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }

    assert(pfsp::permutation_is_valid(permutation));
  }


  bool
  Pfsp::load_file(std::string filename) {
    assert(filename != "");

    std::ifstream fin(filename);

    if (!fin.is_open()) {
      return false;
    }

    fin >> nb_job_;
    fin >> nb_machine_;

    assert(nb_job_ > 0);
    assert(nb_machine_ > 0);

    resize_(nb_job_, nb_machine_);

    for (unsigned int j = 1; j <= nb_job_; ++j) {
      for (unsigned int m = 1; m <= nb_machine_; ++m) {
        unsigned int machine_number;

        fin >> machine_number;  // useless, assumed corresponding to the order

        assert(machine_number == m);

        processing_time_type processing_time;

        fin >> processing_time;

        assert(processing_time > 0);

        processing_times_[j][m] = processing_time;
      }
    }

    std::string str;

    fin >> str;  // this is not read

    assert(str == "Reldue");

    for (unsigned int j = 1; j <= nb_job_; ++j) {
      unsigned int due_date;

      fin >> due_date;  // -1

      assert(due_date == static_cast<unsigned int>(-1));

      fin >> due_date;

      assert(due_date > 0);

      unsigned int weight;

      fin >> weight;  // -1

      assert(weight == static_cast<unsigned int>(-1));

      fin >> weight;

      assert(weight > 0);

      weights_[j] = weight;
    }

    fin.close();

    return true;
  }


  void
  Pfsp::permutation_init(permutation_type &permutation, Init_type init) const {
    permutation.resize(nb_job_ + 1);

    switch (init) {
    case INIT_IDENTITY:
      permutation_set_identity(permutation);

      break;

    case INIT_REVERSE_IDENTITY:
      permutation_set_identity(permutation);
      permutation_reverse(permutation);

      break;

    case INIT_RANDOM:
      permutation_set_random(permutation);

      break;

    case INIT_SORTED:
      permutation_set_identity(permutation);
      sort_by_weighted_sum_processing_time(permutation);

      break;

    case INIT_SRZ:
      permutation_simplified_rz(permutation);

      break;

    default:
      assert(false);
    }
  }


  void
  Pfsp::permutation_simplified_rz(permutation_type &permutation) const {
    permutation_set_identity(permutation);
    sort_by_weighted_sum_processing_time(permutation);

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);

    permutation_type partial(1 + 1);

    // Step 1
    partial[1] = permutation[1];

    // Step 2 to ...
    for (unsigned int length = 2; length <= nb_job_; ++length) {
      unsigned int best_pos;
      time_type best_wct = std::numeric_limits<time_type>::max();

      for (unsigned int i = 1; i <= length; ++i) {
        partial.insert(partial.begin() + i, permutation[length]);

        calculate_completion_times_table(partial, ctable);

        time_type wct = weighted_sum_completion_times(ctable, partial);

        if (best_wct >= wct)  {
          best_wct = wct;
          best_pos = i;
        }

        partial.erase(partial.begin() + i);
      }

      partial.insert(partial.begin() + best_pos, permutation[length]);
    }

    std::copy(partial.cbegin(), partial.cend(), permutation.begin());

    assert(pfsp::permutation_is_valid(permutation));
  }


  void
  Pfsp::println_infos(std::ostream& out) const {
    out << "# jobs: " << nb_job_
        << "   # machines: " << nb_machine_ << std::endl;
  }


  void
  Pfsp::println_table(std::ostream& out, bool transpose) const {
    if (transpose) {
      out << "Table (transposed):" << std::endl;
      for (unsigned int j = 1; j <= nb_machine_; ++j) {
        for (unsigned int i = 1; i <= nb_job_; ++i) {
          out << processing_times_[i][j];
          if (i < nb_job_) {
            out << ' ';
          }
        }
        out << std::endl;
      }
    }
    else {
      out << "Table:" << std::endl;
      for (auto it_job = processing_times_.cbegin() + 1;
           it_job < processing_times_.cend(); ++it_job) {
        for (auto it_machine = it_job->cbegin() + 1;
             it_machine < it_job->cend(); ++it_machine) {
          out << *it_machine;
          if (it_machine + 1 < it_job->cend()) {
            out << ' ';
          }
        }
        out << std::endl;
      }
    }
  }


  void
  Pfsp::println_table(const permutation_type &permutation,
                      std::ostream& out) const {
    out << "Table:" << std::endl;
    for (auto it_pi = permutation.cbegin() + 1;
         it_pi < permutation.cend(); ++it_pi) {
      const auto it_job = processing_times_.cbegin() + *it_pi;

      for (auto it_machine = it_job->cbegin() + 1;
           it_machine < it_job->cend(); ++it_machine) {
        out << *it_machine;
        if (it_machine + 1 < it_job->cend()) {
          out << ' ';
        }
      }
      out << std::endl;
    }
  }


  void
  Pfsp::println_weights(std::ostream& out) const {
    out << "Weights:" << std::endl;
    for (auto it = weights_.cbegin() + 1; it < weights_.cend(); ++it) {
      out << *it << ' ';
    }
    out << std::endl;
  }


  void
  Pfsp::println_weights(const permutation_type &permutation,
                        std::ostream& out) const {
    out << "Weights:" << std::endl;
    for (auto it_pi = permutation.cbegin() + 1;
         it_pi < permutation.cend(); ++it_pi) {
      const auto it_w = weights_.cbegin() + *it_pi;

      out << *it_w << ' ';
    }
    out << std::endl;
  }


  void
  Pfsp::recalculate_completion_times_table
  (const permutation_type &permutation, permutation_type::const_iterator it_pi,
   completion_times_table_type &completion_times_table) const {
    assert(it_pi != permutation.cbegin());
    assert(it_pi != permutation.cend());

    // Job 1
    if (it_pi == permutation.cbegin() + 1) {  // pi(1)
      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(1), j}
      auto it = it_job->begin() + 1;                // C_{pi(1), 1}

      auto it_ptime
        = (processing_times_.begin() + *it_pi)->begin() + 1;  // p_{pi(1), 1}

      *it = *it_ptime;  // C_{pi(1), 1} = p_{pi(1), 1}

      // j = 2...
      for (++it_ptime, ++it; it != it_job->end(); ++it_ptime, ++it) {
        // C_{pi(1), j} = C_{pi(1), j-1} + p_{pi(1), j}
        *it = *(it - 1) + *it_ptime;
      }

      ++it_pi;
    }

    // Job 2 to nb_job_, i = 2...
    for ( ; it_pi != permutation.end(); ++it_pi) {
      // C_{pi(i-1), 1}
      auto it_prev
        = (completion_times_table.begin() + *(it_pi - 1))->begin() + 1;

      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(i), j}
      auto it = it_job->begin() + 1;                // C_{pi(i), 1}

      // p_{pi(i), 1}
      auto it_ptime = (processing_times_.begin() + *it_pi)->begin() + 1;

      // C_{pi(i), 1} = C_{pi(i-1), 1} + p_{pi(i), 1}
      *it = *it_prev + *it_ptime;

      // j = 2...
      for (++it_ptime, ++it_prev, ++it; it != it_job->end();
           ++it_ptime, ++it_prev, ++it) {
        // C_{pi(i), j} = max(C_{pi(i-1), j}, C_{pi(i), j-1}) + p_{pi(i), j}
        *it = std::max(*it_prev, *(it - 1)) + *it_ptime;
      }
    }
  }


  time_type
  Pfsp::weighted_sum_completion_times
  (const completion_times_table_type &completion_times_table) const {
    time_type weighted_sum = 0;

    auto it_ctime = completion_times_table.cbegin() + 1;

    for (auto it_w = weights_.cbegin() + 1; it_w != weights_.cend();
         ++it_w, ++it_ctime) {
      weighted_sum += it_ctime->back() * *it_w;
    }

    return weighted_sum;
  }


  time_type
  Pfsp::weighted_sum_completion_times
  (const completion_times_table_type &completion_times_table,
   const permutation_type &permutation) const {
    time_type weighted_sum = 0;

    for (auto it_pi = permutation.cbegin() + 1;
         it_pi != permutation.cend(); ++it_pi) {
      const auto it_ctime = completion_times_table.cbegin() + *it_pi;
      const auto it_w = weights_.cbegin() + *it_pi;

      weighted_sum += it_ctime->back() * *it_w;
    }

    return weighted_sum;
  }


  std::vector<double>
  Pfsp::weighted_sums_processing_time() const {
    std::vector<double> sums(nb_job_ + 1);

    for (unsigned int i = 1; i <= nb_job_; ++i) {
      sums[i] = weighted_sum_processing_time(i);
    }

    return sums;
  }



  /* ***************
   * Friend method *
   *****************/
  std::ostream&
  operator<<(std::ostream& out, const Pfsp &pfsp) {
    pfsp.println_infos(out);
    pfsp.println_table(out);
    pfsp.println_weights(out);

    return out;
  }



  /* ****************
   * Private method *
   ******************/
  void
  Pfsp::best_exchange_tabu_(permutation_type &permutation,
                            completion_times_table_type &ctable,
                            time_type &wct,
                            std::list<tabu_item_type> &tabu_list,
                            std::set<tabu_item_type> &tabu_set,
                            unsigned int tabu_tenure)
    const {
    time_type prev_wct = wct;
    time_type best_wct = std::numeric_limits<time_type>::max();
    auto best_it_pi1 = permutation.end();
    auto best_it_pi2 = permutation.end();

    auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

    for (auto it_pi1 = last_it_pi; it_pi1 + 1 != permutation.end(); ++it_pi1) {
      if (tabu_set.find(*it_pi1) == tabu_set.end()) {  // not tabu
        for (auto it_pi2 = it_pi1 + 1; it_pi2 != permutation.end(); ++it_pi2) {
          // Try exchange
          permutation_exchange(it_pi1, it_pi2);

          recalculate_completion_times_table(permutation, last_it_pi, ctable);
          last_it_pi = it_pi1;

          wct = weighted_sum_completion_times(ctable);

          if (best_wct > wct) {  // permutation tried is better
            // Save it
            best_wct = wct;
            best_it_pi1 = it_pi1;
            best_it_pi2 = it_pi2;
          }

          // Go back to current permutation
          permutation_exchange(it_pi1, it_pi2);
        }
      }
    }

    if (best_it_pi1 != permutation.end()) {  // set permutation to best
      tabu_add_(tabu_list, tabu_set, tabu_tenure, *best_it_pi1);

      permutation_exchange(best_it_pi1, best_it_pi2);
      recalculate_completion_times_table(permutation, best_it_pi1, ctable);
      wct = best_wct;
    }
    else {                                   // all neighborhood was tabu
      wct = prev_wct;
    }
  }


  void
  Pfsp::best_insert_tabu_(permutation_type &permutation,
                          completion_times_table_type &ctable,
                          time_type &wct,
                          std::list<tabu_item_type> &tabu_list,
                          std::set<tabu_item_type> &tabu_set,
                          unsigned int tabu_tenure)
    const {
    time_type prev_wct = wct;
    time_type best_wct = std::numeric_limits<time_type>::max();
    auto best_it_pi1 = permutation.end();
    auto best_it_pi2 = permutation.end();

    auto last_it_pi1 = permutation.begin() + 1;  // last needed 1 recalculation

    for (auto it_pi1 = last_it_pi1; it_pi1 + 1 != permutation.end(); ++it_pi1) {
      if (tabu_set.find(*it_pi1) == tabu_set.end()) {  // not tabu
        auto last_it_pi2 = permutation.begin() + 1;  // last needed 2

        for (auto it_pi2 = it_pi1 + 1; it_pi2 != permutation.end();
             last_it_pi2 = it_pi2, ++it_pi2) {
          // Try insert
          permutation_insert(it_pi1, it_pi2);

          recalculate_completion_times_table(permutation,
                                             std::min(last_it_pi1, last_it_pi2),
                                             ctable);
          last_it_pi1 = it_pi1;

          wct = weighted_sum_completion_times(ctable);

          if (best_wct > wct) {  // permutation tried is better
            // Save it
            best_wct = wct;
            best_it_pi1 = it_pi1;
            best_it_pi2 = it_pi2;
          }

          // Go back to current permutation
          permutation_insert(it_pi2, it_pi1);
        }
      }
    }

    if (best_it_pi1 != permutation.end()) {  // set permutation to best
      tabu_add_(tabu_list, tabu_set, tabu_tenure, *best_it_pi1);

      permutation_insert(best_it_pi1, best_it_pi2);
      recalculate_completion_times_table(permutation, best_it_pi1, ctable);
      wct = best_wct;
    }
    else {                                   // all neighborhood was tabu
      wct = prev_wct;
    }
  }


  void
  Pfsp::resize_(unsigned int nb_job, unsigned int nb_machine) {
    assert(nb_job > 0);
    assert(nb_machine > 0);

    nb_job_ = nb_job;
    nb_machine_ = nb_machine;


    // nb_job lines x nb_machine columns
    processing_times_.resize(nb_job + 1);  // + 1 because position 0 *not* used

    for (auto it_job = processing_times_.begin() + 1;
         it_job < processing_times_.end(); ++it_job) {
      it_job->resize(nb_machine + 1);  // + 1 because position 0 *not* used
    }

    assert(processing_times_[0].size() == 0);


    weights_.resize(nb_job + 1);  // + 1 because position 0 *not* used
  }

}  // namespace pfsp
