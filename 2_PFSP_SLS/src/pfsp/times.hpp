/* -*- coding: latin-1 -*- */

/** \file pfsp/times.hpp (April 2nd, 2017)
 * \brief
 * Types and operations for completion times.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef SRC_PFSP_TIMES_HPP_
#define SRC_PFSP_TIMES_HPP_

#include <cstdint>

#include <iostream>
#include <vector>

#include "permutation.hpp"


namespace pfsp {

  /* *******
   * Types *
   *********/
  /** \brief
   * Bigger number type for calculate on processing time datas.
   */
  typedef uint64_t time_type;


  /** \brief
   * Number type for each processing time data: \f$p_{ij}\f$
   * (one by pair job, machine).
   */
  typedef uint32_t processing_time_type;


  /** \brief
   * Line of partial completion times for a job.
   */
  typedef std::vector<processing_time_type> completion_times_job_type;


  /** \brief
   * Table of completion times for all jobs.
   */
  typedef std::vector<completion_times_job_type> completion_times_table_type;



  /* ************
   * Prototypes *
   **************/
  /** \brief
   * Init an zeroes table
   * with nb_job jobs and nb_machine machines.
   *
   * @param completion_times
   * @param nb_job
   * @param nb_machine
   */
  void
  completion_times_init(completion_times_table_type &completion_times,
                        unsigned int nb_job, unsigned nb_machine);


  /** \brief
   * Print the table to out.
   *
   * @param completion_times
   * @param out
   */
  void
  completion_times_println(const completion_times_table_type &completion_times,
                           std::ostream& out);


  /** \brief
   * Print the table to out,
   * in the order fixed by permutation.
   *
   * @param completion_times
   * @param permutation
   * @param out
   */
  void
  completion_times_println(const completion_times_table_type &completion_times,
                           const permutation_type &permutation,
                           std::ostream& out);

}  // namespace pfsp

#endif  // SRC_PFSP_TIMES_HPP_
