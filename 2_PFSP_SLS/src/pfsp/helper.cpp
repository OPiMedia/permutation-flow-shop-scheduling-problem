/* -*- coding: latin-1 -*- */

/** \file pfsp/helper.cpp (May 12, 2017)
 * \brief
 * Helper functions.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>

#include <fstream>
#include <iostream>
#include <string>

#include "helper.hpp"


namespace pfsp {

  /* ******************
   * Private function *
   ********************/
  std::string
  strip(std::string s) {
    std::string dest(s);

    // Remove whitespaces for tail
    while (!dest.empty() && isspace(dest[dest.size() - 1])) {
      dest.pop_back();
    }

    // Remove whitespaces for head
    while (!dest.empty() && isspace(dest[0])) {
      dest.erase(0, 1);
    }

    return dest;
  }



  /* ***********
   * Functions *
   *************/
  time_type
  get_best_known_wct(std::string bests_filename,
                     std::string instance_filename) {
    assert(bests_filename != "");
    assert(instance_filename != "");

    // Get instance name from instance_filename
    const std::string::size_type i = instance_filename.rfind('/');

    if (i == std::string::npos) {
      return 0;
    }

    const std::string instance_name = strip(instance_filename.substr(i + 1));


    // Read bests_filename
    std::ifstream fin(bests_filename);

    if (!fin.is_open()) {
      return 0;
    }

    time_type best = 0;
    std::string line;

    while (std::getline(fin, line)) {
      const std::string::size_type i = line.find(',');

      if (i != std::string::npos) {
        const std::string name = strip(line.substr(0, i));

        if (name == instance_name) {
          const std::string best_string = strip(line.substr(i + 1));

          if (line.find("e+") ==  std::string::npos) {
            best = stoul(best_string);
          }
          else {  // floating representation
            best = stod(best_string);
          }

          break;
        }
      }
    }

    fin.close();

    return best;
  }

}  // namespace pfsp
