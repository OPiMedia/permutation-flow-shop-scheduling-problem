/* -*- coding: latin-1 -*- */

/** \file pfsp/permutation.cpp (May 14, 2017)
 * \brief
 * Types and operations for permutations of \f$\{i\}_{1\leq i\leq N}\f$.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include "helper.hpp"
#include "permutation.hpp"


namespace pfsp {

  /* ***********
   * Functions *
   *************/
  void
  permutation_exchange_random(permutation_type &permutation) {
    const unsigned int size = permutation.size() - 1;
    const unsigned int i = rand_between(1, size - 1);
    const unsigned int j = i + rand_between(1, size - i);

    assert(1 <= i);
    assert(i < size);
    assert(i < j);
    assert(j <= size);

    permutation_exchange(permutation.begin() + i,
                         permutation.begin() + j);
  }


  bool
  permutation_is_valid(const permutation_type &permutation) {
    if (permutation.empty() || (permutation[0] != 0)) {
      return false;
    }

    const unsigned int n = permutation.size() - 1;
    std::vector<bool> already(permutation.size());

    for (auto it = permutation.cbegin() + 1; it < permutation.cend(); ++it) {
      if ((*it == 0) || (*it > n)) {
        return false;
      }

      if (already[*it]) {
        return false;
      }

      already[*it] = true;
    }

    return true;
  }


  void
  permutation_print(const permutation_type &permutation,
                    std::ostream& out) {
    assert(permutation.size() > 1);

    out << *(permutation.cbegin() + 1);
    for (auto it = permutation.cbegin() + 2; it < permutation.cend(); ++it) {
      out << ' ' << *it;
    }
  }


  void
  permutation_println(const permutation_type &permutation,
                      std::ostream& out) {
    assert(permutation.size() > 1);

    permutation_print(permutation, out);
    out << std::endl;
  }


  void
  permutation_set_identity(permutation_type &permutation) {
    assert(permutation.size() > 1);

    for (unsigned int i = 0; i < permutation.size(); ++i) {
      permutation[i] = i;
    }
  }


  void
  permutation_set_random(permutation_type &permutation) {
    assert(permutation.size() > 1);

    permutation_set_identity(permutation);
    std::random_shuffle(permutation.begin() + 1, permutation.end());
  }


  permuation_hash_type
  permutation_to_hash(permutation_type &permutation) {
    assert(permutation.size() > 1);

    // Hash function from
    // http://stackoverflow.com/questions/20511347/a-good-hash-function-for-a-vector/27216842#27216842

    permuation_hash_type hash = permutation.size();

    for (auto it = permutation.cbegin() + 1; it < permutation.cend(); ++it) {
      hash ^= *it + 0x9e3779b9ul + (hash << 6) + (hash >> 2);
    }

    return hash;
  }

}  // namespace pfsp
