/* -*- coding: latin-1 -*- */

/** \file pfsp/times.cpp (April 2nd, 2017)
 * \brief
 * Types and operations for completion times.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>

#include "times.hpp"


namespace pfsp {

  /* ***********
   * Functions *
   *************/
  void
  completion_times_init(completion_times_table_type &completion_times,
                        unsigned int nb_job, unsigned nb_machine) {
    assert(nb_job > 0);
    assert(nb_machine > 0);

    // nb_job lines x nb_machine columns
    completion_times.resize(nb_job + 1);  // + 1 because position 0 *not* used

    for (auto it_job = completion_times.begin() + 1;
         it_job < completion_times.end(); ++it_job) {
      it_job->resize(nb_machine + 1);  // + 1 because position 0 *not* used
    }

    assert(completion_times[0].size() == 0);
  }


  void
  completion_times_println(const completion_times_table_type &completion_times,
                           std::ostream& out) {
    for (auto it_job = completion_times.cbegin() + 1;
         it_job < completion_times.cend(); ++it_job) {
      for (auto it_machine = it_job->cbegin() + 1;
           it_machine < it_job->cend(); ++it_machine) {
        out << *it_machine;
        if (it_machine + 1 < it_job->cend()) {
          out << ' ';
        }
      }
      out << std::endl;
    }
  }


  void
  completion_times_println(const completion_times_table_type &completion_times,
                           const permutation_type &permutation,
                           std::ostream& out) {
    for (auto it_pi = permutation.cbegin() + 1;
         it_pi < permutation.cend(); ++it_pi) {
      const auto it_job = completion_times.cbegin() + *it_pi;

      for (auto it_machine = it_job->cbegin() + 1;
           it_machine < it_job->cend(); ++it_machine) {
        out << *it_machine;
        if (it_machine + 1 < it_job->cend()) {
          out << ' ';
        }
      }
      out << std::endl;
    }
  }

}  // namespace pfsp
