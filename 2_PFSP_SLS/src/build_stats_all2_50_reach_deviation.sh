#!/bin/sh

# Rebuild stats for algo 2 on 5 first instances of size 50 (~3 heures 30 minutes)
# Run until reach (2, 1, 0.5 and 0.1) of quality deviation (or stop after 60 seconds)

for i in $(seq 1 25)
do
    echo "============ Repetition $i ============"
    for quality in 2 1 0.5 0.1
    do
        ./build_stats_algo.sh "--init-srz --exchange --tabu --max-duration 4 --nb-worst 10 --tabu-tenure 15 --quality-deviation $quality --max-duration 60" 50 ../stats_results/50/reach/stats-algo2-reach-deviation-$quality-$i.txt 5
    done
done
