#!/bin/sh

# Rebuild stats for algo 2 on 5 first instances of size 50 (~10 minutes)
# Run until reach 1.5 of quality deviation (or stop after 60 seconds)

for i in $(seq 1 25)
do
    echo "============ Repetition $i ============"
    quality=1.5
    ./build_stats_algo.sh "--init-srz --exchange --tabu --max-duration 4 --nb-worst 10 --tabu-tenure 15 --quality-deviation $quality --max-duration 60" 50 ../stats_results/50/reach/stats-algo2-reach-deviation-$quality-$i.txt 5
done
