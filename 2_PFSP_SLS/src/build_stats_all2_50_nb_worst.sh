#!/bin/sh

# Rebuild stats for nb worst testing for algo 2 on all instances of size 50 (~2 heure 2 minutes)

for nb in $(seq 0 60)
do
    echo "============ nb worst $nb ============"
    ./build_stats_algo.sh "--init-srz --exchange --tabu --max-duration 4 --nb-worst $nb --random-seed 666 --tabu-tenure 15" 50 ../stats_results/50/nb_worst/stats-algo2-nb-worst-$nb.txt
done
