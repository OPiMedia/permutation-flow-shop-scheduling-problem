#!/bin/sh

# Rebuild stats for algo 4 on all instances of size 50 (~4 heures 6 minutes)
# Run until reach (2, 1, 0.5 and 0.1) of quality deviation (or stop after 200 seconds)

for quality in 2 1 0.5 0.1
do
    ./build_stats_algo.sh "--init-srz --insert-permutation --first --max-duration 4 --nb-worst 10 --tabu-tenure 15 --quality-deviation $quality --max-duration 200" 50 ../stats_results/50/reach/stats-algo4-reach-deviation-$quality-1.txt
done
