#!/bin/sh

# Rebuild stats for one algo on $nb first instances of size 50 or 100

options=$1
size=$2
output=$3
nb=$4

if [ -z "$nb" ]
then
    nb=30
fi

if [ -z "$options" ] || [ -z "$size" ] || [ -z "$output" ]
then
    echo "Usage: build_stats_algo.sh <options> <50|100> <output_file>"

    exit 1
fi


echo 'filename\ttime\twct\tbest known\tdeviation' > $output

for i in $(seq 1 $nb)
do
    if [ $i -lt 10 ]
    then
        file="../instances/${size}_20_0$i"
    else
        file="../instances/${size}_20_$i"
    fi
    echo "===== Instance $file ====="
    ./pfsp_stats $options $file >> $output
    echo
done
