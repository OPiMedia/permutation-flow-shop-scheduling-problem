/* -*- coding: latin-1 -*- */

/** \file dirty.cpp (May 17, 2017)
 * \brief Dirty way to quickly check if a solution is valid.
 * Copy-paste it to the array a and set macros FILENAME, SIZE and BEST_KNOWN.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <chrono>
#include <iomanip>
#include <iostream>

#include "pfsp/helper.hpp"
#include "pfsp/pfsp.hpp"
#include "pfsp/permutation.hpp"


#define FILENAME "../instances/50_20_19"
#define SIZE 50
#define BEST_KNOWN 465508

unsigned int a[SIZE] = {26, 15, 13, 11, 47, 29, 46, 17, 23, 16, 4, 14, 5, 39, 1, 25, 0, 35, 24, 41, 48, 20, 10, 3, 28, 37, 9, 19, 22, 30, 38, 49, 33, 12, 18, 34, 2, 6, 44, 42, 40, 36, 32, 21, 31, 8, 43, 7, 27, 45};


/* ******
 * Main *
 ********/
int
main() {
  pfsp::permutation_type permutation;

  permutation.resize(SIZE + 1);

  permutation[0] = 0;
  for (unsigned int i = 0; i < SIZE; ++i) {
    permutation[i + 1] = a[i] + 1;
    // + 1 if data from other program with indices that begin from 0
  }

  pfsp::permutation_println(permutation);


  pfsp::Pfsp pfsp_instance;

  // Read and set the instance
  if (!pfsp_instance.load_file(FILENAME)) {
    std::cerr << "Opening of file \"" << FILENAME << "\" failed!" << std::endl;

    exit(1);
  }


  // Calculate WCT
  pfsp::completion_times_table_type ctable;

  pfsp::completion_times_init(ctable,
                              pfsp_instance.nb_job(),
                              pfsp_instance.nb_machine());

  if (!pfsp::permutation_is_valid(permutation)) {
    std::cout << "Permutation INVALID!" << std::endl;

    exit(1);
  }

  pfsp_instance.calculate_completion_times_table(permutation, ctable);

  const pfsp::time_type wct
    = pfsp_instance.weighted_sum_completion_times(ctable);

  pfsp::time_type best_known_wct = BEST_KNOWN;

  std::cout << wct;
  if (best_known_wct > 0) {
    std::cout << '\t' << pfsp::deviation(wct, best_known_wct);
  }
  std::cout << std::endl;

  return EXIT_SUCCESS;
}
