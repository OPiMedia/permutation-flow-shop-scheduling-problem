#!/bin/sh

# Rebuild stats for start zone length testing for algo 4 on all instances of size 50 (~18 minutes)

for length in $(seq 2 10)
do
    echo "============ Start zone length $length ============"
    ./build_stats_algo.sh "--init-srz --insert-permutation --first --max-duration 4 --start-zone-length $length" 50 ../stats_results/50/start_zone_length/stats-algo4-start-zone-length-$length.txt
done
