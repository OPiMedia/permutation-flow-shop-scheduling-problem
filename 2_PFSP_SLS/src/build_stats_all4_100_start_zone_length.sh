#!/bin/sh

# Rebuild stats for start zone length testing for algo 4 on 10 first instances of size 100 (~47 minutes)

for length in $(seq 2 10)
do
    echo "============ Start zone length $length ============"
    ./build_stats_algo.sh "--init-srz --insert-permutation --first --max-duration 31 --start-zone-length $length" 100 ../stats_results/100/start_zone_length/stats-algo4-start-zone-length-$length.txt 10
done
