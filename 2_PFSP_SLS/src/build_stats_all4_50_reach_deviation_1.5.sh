#!/bin/sh

# Rebuild stats for algo 4 on all instances of size 50 (~15 minutes)
# Run until reach 1.5 of quality deviation (or stop after 200 seconds)

quality=1.5
./build_stats_algo.sh "--init-srz --insert-permutation --first --max-duration 4 --nb-worst 10 --tabu-tenure 15 --quality-deviation $quality --max-duration 200" 50 ../stats_results/50/reach/stats-algo4-reach-deviation-$quality-1.txt
