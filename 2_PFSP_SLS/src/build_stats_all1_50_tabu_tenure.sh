#!/bin/sh

# Rebuild stats for tabu tenure testing for algo 1 on all instances of size 50 (~1 heure 22 minutes)

for tenure in $(seq 0 40)
do
    echo "============ Tenure $tenure ============"
    ./build_stats_algo.sh "--init-random --exchange --tabu --max-duration 4 --nb-worst 10 --random-seed 666 --tabu-tenure $tenure" 50 ../stats_results/50/tabu_tenure/stats-algo1-tabu-tenure-$tenure.txt
done
