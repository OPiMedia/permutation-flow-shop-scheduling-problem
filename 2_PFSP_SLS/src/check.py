#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
check.py (May 18, 2017)

Make some simple verification in all files of results.

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import glob


############
# Function #
############
def check_file(filename):
    print(filename)

    with open(filename) as fin:
        line = fin.readline()
        assert line == 'filename\ttime\twct\tbest known\tdeviation\n', line

        for line in fin:
            columns = line.split()

            assert len(columns) == 5

            time = float(columns[1])
            wct = float(columns[2])
            best = float(columns[3])
            deviation = float(columns[4])

            assert 0 < time < 201, time
            assert 465508 <= wct <= 20010000, wct
            assert 465508 <= best <= 1992230, best
            assert 0 < deviation <= 6.36, deviation


########
# Main #
########
def main():
    for filename in sorted(glob.glob('../stats_results/*/*.txt')
                           + glob.glob('../stats_results/*/*/*.txt')):
        check_file(filename)

if __name__ == '__main__':
    main()
