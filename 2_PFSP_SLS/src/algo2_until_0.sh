#!/bin/sh

# Run algo 2 on one instances until found a quality deviation

size=$1
instance=$2
quality=$3

if [ -z "$quality" ]
then
    quality=0
fi

if [ -z "$size" ] || [ -z "$instance" ]
then
    echo "Usage: algo2_until_0.sh.sh <50|100> <instance_number> <quality>"

    exit 1
fi

if [ $instance -lt 10 ]
then
    file="../instances/${size}_20_0$instance"
else
    file="../instances/${size}_20_$instance"
fi

./pfsp_stats --init-srz --exchange --tabu --nb-worst 10 --tabu-tenure 15 --quality-deviation $quality --display-solution $file
