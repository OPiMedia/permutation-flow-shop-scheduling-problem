#!/bin/sh

# Rebuild stats for algo 4 on all instances of size 50 (~2 minutes)

./build_stats_algo.sh '--init-srz --insert-permutation --first --max-duration 4' 50 ../stats_results/50/stats-algo4-1.txt
