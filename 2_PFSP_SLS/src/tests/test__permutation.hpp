/* -*- coding: latin-1 -*- */

/** \file test__permutation.hpp (May 15, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include "../pfsp/permutation.hpp"

#include <iostream>
#include <set>


using namespace pfsp;


#define NOT_COVERED {                           \
    std::cout << "NOT COVERED!" << std::endl;   \
    std::cout.flush();                          \
  }


#define SHOW_FUNC_NAME {                                        \
    std::cout << std::endl                                      \
              << "=== " << __func__ << " ===" << std::endl;     \
    std::cout.flush();                                          \
  }



permutation_type identity7(7 + 1);  // permutation (1, 2, 3, 4, 5, 6, 7)
std::set<permutation_item_type> set7;  // set {1, 2, 3, 4, 5, 6, 7}



class Test__common : public CxxTest::TestSuite {
public:
  void test_init() {
    permutation_set_identity(identity7);
    identity7[0] = 0;

    std::copy(identity7.cbegin() + 1, identity7.cend(),
              std::inserter(set7, set7.end()));
  }



  void test_type() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sizeof(unsigned int), 4);
    TS_ASSERT_EQUALS(sizeof(permuation_hash_type), 8);
  }
  


  void test__permutation_exchange() {
    SHOW_FUNC_NAME;

    permutation_type p(identity7);

    for (unsigned int i = 1; i <= 6; ++i) {
      for (unsigned int j = i + 1; j <= 7; ++j) {
        permutation_exchange(p.begin() + i, p.begin() + j);

        for (unsigned int k = 1; k <= 7; ++k) {
          if (k == i) {
            TS_ASSERT_EQUALS(p[k], j);
          }
          else if (k == j) {
            TS_ASSERT_EQUALS(p[k], i);
          }
          else {
            TS_ASSERT_EQUALS(p[k], k);
          }
        }

        const std::set<permutation_item_type> s(p.cbegin() + 1, p.cend());

        TS_ASSERT_EQUALS(s, set7);

        permutation_exchange(p.begin() + i, p.begin() + j);
        TS_ASSERT_EQUALS(p, identity7);
      }
    }
  }


  void test__permutation_insert() {
    SHOW_FUNC_NAME;

    permutation_type p(identity7);

    for (unsigned int i = 1; i <= 7; ++i) {
      for (unsigned int j = 1; j <= 7; ++j) {
        if (i == j) {
          continue;
        }

        permutation_insert(p.begin() + i, p.begin() + j);

        if (i < j) {
          for (unsigned int k = 1; k <= 7; ++k) {
            if ((k < i) || (k > j)) {
              TS_ASSERT_EQUALS(p[k], k);
            }
            else if (k == j) {
              TS_ASSERT_EQUALS(p[k], i);
            }
            else {
              TS_ASSERT_EQUALS(p[k], k + 1);
            }
          }
        }
        else {
          for (unsigned int k = 1; k <= 7; ++k) {
            if ((k < j) || (k > i)) {
              TS_ASSERT_EQUALS(p[k], k);
            }
            else if (k == j) {
              TS_ASSERT_EQUALS(p[k], i);
            }
            else {
              TS_ASSERT_EQUALS(p[k], k - 1);
            }
          }
        }

        const std::set<permutation_item_type> s(p.cbegin() + 1, p.cend());

        TS_ASSERT_EQUALS(s, set7);

        permutation_insert(p.begin() + j, p.begin() + i);
        TS_ASSERT_EQUALS(p, identity7);
      }
    }
  }


  void test__permutation_print() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__permutation_println() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__permutation_reverse() {
    SHOW_FUNC_NAME;

    permutation_type p(identity7);

    permutation_reverse(p);

    for (unsigned int i = 1; i <= 7; ++i) {
      TS_ASSERT_EQUALS(p[i], 8 - i);
    }

    permutation_reverse(p);

    TS_ASSERT_EQUALS(p, identity7);
  }


  void test__permutation_set_identity() {
    SHOW_FUNC_NAME;

    permutation_type p(7 + 1);

    p[0] = 0;
    permutation_set_identity(p);

    for (unsigned int i = 1; i <= 7; ++i) {
      TS_ASSERT_EQUALS(p[i], i);
    }

    TS_ASSERT_EQUALS(p, identity7);
  }


  void test__permutation_set_random() {
    SHOW_FUNC_NAME;

    permutation_type p(7 + 1);

    permutation_set_random(p);

    const std::set<permutation_item_type> s(p.cbegin() + 1, p.cend());

    TS_ASSERT_EQUALS(s, set7);
  }


  void test__permutation_transpose() {
    SHOW_FUNC_NAME;

    permutation_type p(identity7);

    for (unsigned int i = 1; i <= 6; ++i) {
      permutation_transpose(p.begin() + i);

      for (unsigned int k = 1; k <= 7; ++k) {
        if (k == i) {
          TS_ASSERT_EQUALS(p[k], i + 1);
        }
        else if (k == i + 1) {
          TS_ASSERT_EQUALS(p[k], i);
        }
        else {
          TS_ASSERT_EQUALS(p[k], k);
        }
      }

      const std::set<permutation_item_type> s(p.cbegin() + 1, p.cend());

      TS_ASSERT_EQUALS(s, set7);

      permutation_transpose(p.begin() + i);
      TS_ASSERT_EQUALS(p, identity7);
    }
  }

};
