/* -*- coding: latin-1 -*- */

/** \file test__times.hpp (April 2nd, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include "../pfsp/times.hpp"

#include <iostream>


using namespace pfsp;


#define NOT_COVERED {                           \
    std::cout << "NOT COVERED!" << std::endl;   \
    std::cout.flush();                          \
  }


#define SHOW_FUNC_NAME {                                        \
    std::cout << std::endl                                      \
              << "=== " << __func__ << " ===" << std::endl;     \
    std::cout.flush();                                          \
  }



class Test__common : public CxxTest::TestSuite {
public:
  void test__completion_times_init() {
    SHOW_FUNC_NAME;

    completion_times_table_type table;

    completion_times_init(table, 7, 11);

    TS_ASSERT_EQUALS(table.size(), 7 + 1);

    TS_ASSERT_EQUALS(table[0].size(), 0);

    for (unsigned int i = 1; i <= 7; ++i) {
      TS_ASSERT_EQUALS(table[i].size(), 11 + 1);

      for (unsigned int j = 0; j <= 11; ++j) {
        TS_ASSERT_EQUALS(table[i][j], 0);
      }
    }
  }


  void test__completion_times_println__1() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__completion_times_println__2() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }

};
