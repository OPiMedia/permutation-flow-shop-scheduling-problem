#!/bin/sh

# Rebuild stats for algo 0 on all instances (~5 seconds)

for size in 50 100
do
    ./build_stats_algo.sh '--init-srz --insert --first' $size ../stats_results/$size/stats-algo0-1.txt
done
