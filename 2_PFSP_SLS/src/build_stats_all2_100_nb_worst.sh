#!/bin/sh

# Rebuild stats for nb worst testing for algo 2 on 10 first instances of size 100 (~2 heure 40 minutes)

for i in $(seq 0 30)
do
    nb=$(($i*2))
    echo "============ nb worst $nb ============"
    ./build_stats_algo.sh "--init-srz --exchange --tabu --max-duration 31 --nb-worst $nb --random-seed 666 --tabu-tenure 20" 100 ../stats_results/100/nb_worst/stats-algo2-nb-worst-$nb.txt 10
done
