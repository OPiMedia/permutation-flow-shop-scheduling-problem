/* -*- coding: latin-1 -*- */

/** \file pfsp_stats.cpp (May 16, 2017)
 * \brief Flow Shop Scheduling Problem (PFSP-WCT) solver
 * that produces strict informations.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <chrono>
#include <iomanip>
#include <iostream>

#include "pfsp/helper.hpp"
#include "pfsp/pfsp.hpp"
#include "pfsp/permutation.hpp"



/* *****************
 * Global constant *
 *******************/
const unsigned int DISPLAY_PRECISION_DEFAULT = 9;



/* ***********
 * Functions *
 *************/
/** \brief
 * Return the basename from the path.
 *
 * @param path
 */
std::string
basename(std::string path) {
  const auto pos = path.find_last_of('/');

  return (pos == std::string::npos
          ? path
          : path.substr(pos + 1));
}



/** \brief
 * Print a helping message and quit the program.
 */
void
help(int error_code = 0) {
  std::cerr << "Usage: pfsp_stats [options] <instance_file>"
            << std::endl
            << std::endl
            << "Initial solution options:" << std::endl
            << "  --init-identity (by default)" << std::endl
            << "  --init-ridentity" << std::endl
            << "  --init-random" << std::endl
            << "  --init-sorted" << std::endl
            << "  --init-srz" << std::endl
            << std::endl
            << "Neighborhood options:" << std::endl
            << "  --transpose (by default)" << std::endl
            << "  --exchange" << std::endl
            << "  --insert" << std::endl
            << "  --insert-permutation (only with --first)" << std::endl
            << "  --vnd-transpose-exchange-insert (only with --first)"
            << std::endl
            << "  --vnd-transpose-insert-exchange (only with --first)"
            << std::endl
            << std::endl
            << "Pivoting rule/solver options:" << std::endl
            << "  --first (by default)" << std::endl
            << "  --best" << std::endl
            << "  --nothing (keep the initial solution)" << std::endl
            << "  --tabu (only with --exchange or --insert)" << std::endl
            << std::endl
            << "Other options:" << std::endl
            << "  --display-precision n (" << DISPLAY_PRECISION_DEFAULT
            << " by default)" << std::endl
            << "  --display-solution" << std::endl
            << "  --max-duration n ("
            << std::numeric_limits<unsigned int>::max()
            << " by default) (only used by --insert-permutation and --tabu)"
            << std::endl
            << "  --nb-worst n (2 by default) (only used by --insert-permutation)"
            << std::endl
            << "  --quality-deviation x (0 by default) (only used by --insert-permutation and --tabu)"
            << std::endl
            << "  --random-seed n (0 by default)" << std::endl
            << "  --start-zone-length n (3 by default) (only used by --insert-permutation"
            << std::endl
            << "  --tabu-tenure n (sqrt(# jobs) default) (only used by --tabu)"
            << std::endl;

  exit(error_code);
}



/* ******
 * Main *
 ********/
int
main(int argc, char *argv[]) {
  pfsp::time_type best_known_wct;

  unsigned int display_precision = DISPLAY_PRECISION_DEFAULT;

  bool display_solution = false;

  std::string filename;

  pfsp::Init_type init = pfsp::INIT_IDENTITY;

  unsigned int max_duration = 0;

  unsigned int start_zone_length = 3;

  unsigned int nb_worst_without_perturbation = 2;

  pfsp::Neighborhood_type neighborhood = pfsp::NEIGHBORHOOD_TRANSPOSE;

  pfsp::permutation_type permutation;

  pfsp::Pivoting_type pivoting = pfsp::PIVOTING_FIRST;

  pfsp::Pfsp pfsp_instance;

  double quality_deviation = 0;

  unsigned int seed = 0;

  unsigned int tabu_tenure = 0;


  // Read command line parameters and set config.
  if (argc == 1) {
    help();
  }

  for (int i = 1; i < argc; ++i) {
    if ((strlen(argv[i]) > 0) && (argv[i][0] == '-')) {
      const std::string param = argv[i];

      if (param == "--best") {
        pivoting = pfsp::PIVOTING_BEST;
      }
      else if (param == "--display-precision") {  // --display-precision n
        ++i;
        if (i >= argc) {
          std::cerr << "--display-precision number missing!" << std::endl;

          help(1);
        }

        display_precision = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--exchange") {
        neighborhood = pfsp::NEIGHBORHOOD_EXCHANGE;
      }
      else if (param == "--display-solution") {
        display_solution = true;
      }
      else if (param == "--first") {
        pivoting = pfsp::PIVOTING_FIRST;
      }
      else if (param == "--help") {
        help();
      }
      else if (param == "--init-identity") {
        init = pfsp::INIT_IDENTITY;
      }
      else if (param == "--init-random") {
        init = pfsp::INIT_RANDOM;
      }
      else if (param == "--init-ridentity") {
        init = pfsp::INIT_REVERSE_IDENTITY;
      }
      else if (param == "--init-sorted") {
        init = pfsp::INIT_SORTED;
      }
      else if (param == "--init-srz") {
        init = pfsp::INIT_SRZ;
      }
      else if (param == "--insert") {
        neighborhood = pfsp::NEIGHBORHOOD_INSERT;
      }
      else if (param == "--insert-permutation") {
        neighborhood = pfsp::NEIGHBORHOOD_INSERT_PERMUTATION;
      }
      else if (param == "--max-duration") {  // --max-duration n
        ++i;
        if (i >= argc) {
          std::cerr << "--max-duration number missing!" << std::endl;

          help(1);
        }

        max_duration = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--nb-worst") {  // --nb-worst n
        ++i;
        if (i >= argc) {
          std::cerr << "--nb-worst number missing!" << std::endl;

          help(1);
        }

        nb_worst_without_perturbation = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--nothing") {
        pivoting = pfsp::PIVOTING_NOTHING;
      }
      else if (param == "--quality-deviation") {  // --quality-deviation x
        ++i;
        if (i >= argc) {
          std::cerr << "--quality-deviation number missing!" << std::endl;

          help(1);
        }

        quality_deviation = std::stod("0" + std::string(argv[i]));
      }
      else if (param == "--random-seed") {  // --random-seed n
        ++i;
        if (i >= argc) {
          std::cerr << "--random-seed number missing!" << std::endl;

          help(1);
        }

        seed = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--start-zone-length") {  // --start-zone-length n
        ++i;
        if (i >= argc) {
          std::cerr << "--start-zone-length number missing!" << std::endl;

          help(1);
        }

        start_zone_length = std::stoul("0" + std::string(argv[i]));
        if (start_zone_length < 2) {
          std::cerr << "--start-zone-length must be >= 2!" << std::endl;

          help(1);
        }
      }
      else if (param == "--tabu") {
        pivoting = pfsp::PIVOTING_TABU;
      }
      else if (param == "--tabu-tenure") {  // --tabu-tenure n
        ++i;
        if (i >= argc) {
          std::cerr << "--tabu-tenure number missing!" << std::endl;

          help(1);
        }

        tabu_tenure = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--transpose") {
        neighborhood = pfsp::NEIGHBORHOOD_TRANSPOSE;
      }
      else if (param == "--vnd-transpose-exchange-insert") {
        neighborhood = pfsp::NEIGHBORHOOD_VND_TRANSPOSE_EXCHANGE_INSERT;
      }
      else if (param == "--vnd-transpose-insert-exchange") {
        neighborhood = pfsp::NEIGHBORHOOD_VND_TRANSPOSE_INSERT_EXCHANGE;
      }
      else {
        std::cerr << "Param \"" << param << "\" unknown!" << std::endl;

        help(1);
      }
    }
    else {
      filename = argv[i];
    }
  }

  if (filename == "") {
    std::cerr << "Filename missing!" << std::endl;

    help(1);
  }

  std::cout << std::fixed << std::setprecision(display_precision);
  std::cerr << std::fixed << std::setprecision(display_precision);


  // Print config informations
  std::cerr << "=== Config ===" << std::endl;
#ifndef NDEBUG
  std::cerr << "Main program compiled in DEBUG mode" << std::endl;
#endif
  std::cerr
    << "Random seed: " << (seed == 0
                           ? "time(0)"
                           : std::to_string(seed)) << std::endl
    << "Initial permutation: " << pfsp::init_str[init] << std::endl
    << "Neighborhood: " << pfsp::neighborhood_str[neighborhood] << std::endl
    << "Pivoting rule: " << pfsp::pivoting_str[pivoting] << std::endl
    << std::endl
    << "Display precision: " << display_precision << std::endl
    << "Max duration: " << max_duration << " seconds" << std::endl
    << "Number worst without perturbation: " << nb_worst_without_perturbation
    << std::endl
    << "Quality deviation: " << quality_deviation << std::endl
    << "Start zone length: " << start_zone_length << std::endl
    << "Tabu tenure: " << tabu_tenure << std::endl;
  std::cerr.flush();


  // Get best known solution (if available)
  best_known_wct
    = pfsp::get_best_known_wct("../instances/bestSolutions.txt", filename);


  // Read and set the instance
  if (!pfsp_instance.load_file(filename)) {
    std::cerr << "Opening of file \"" << filename << "\" failed!" << std::endl;

    exit(1);
  }

  std::cerr << "=== Data ===" << std::endl
            << "File \"" << filename << "\" readed" << std::endl;
  pfsp_instance.println_infos(std::cerr);

  if (max_duration == 0) {
    max_duration = std::numeric_limits<unsigned int>::max();
    std::cerr << "Max duration: " << max_duration << " seconds"
              << std::endl;
  }

  if (tabu_tenure == 0) {
    tabu_tenure = round(sqrt(pfsp_instance.nb_job()));
    std::cerr << "Tabu tenure = sqrt(# job): " << tabu_tenure << std::endl;
  }

  std::cerr.flush();


  // Init the random generator
  srand(seed == 0
        ? time(0)
        : seed);


  std::chrono::steady_clock::time_point start(std::chrono::steady_clock::now());

  // Init the initial permutation
  pfsp_instance.permutation_init(permutation, init);

  double duration
    = std::chrono::duration<double>(std::chrono::steady_clock::now()
                                    - start).count();

  // Result of initial permutation
  {
    pfsp::completion_times_table_type ctable;

    pfsp::completion_times_init(ctable, pfsp_instance.nb_job(),
                                pfsp_instance.nb_machine());
    pfsp_instance.calculate_completion_times_table(permutation, ctable);

    const pfsp::time_type makespan = pfsp_instance.makespan(permutation,
                                                            ctable);
    const pfsp::time_type sum_completion_times
      = pfsp_instance.sum_completion_times(ctable);
    const pfsp::time_type wct
      = pfsp_instance.weighted_sum_completion_times(ctable);

    std::cerr << "=== Initial results ===" << std::endl
              << "Initial permutation:" << std::endl;
    pfsp::permutation_println(permutation, std::cerr);
    std::cerr << "Makespan: " << makespan << std::endl
              << "Sum of completion times: "
              << sum_completion_times << std::endl
              << "Weighted sum of completion times: "<< wct
              << " >= Best known: ";
    if (best_known_wct > 0) {
      std::cerr << best_known_wct;
    }
    else {
      std::cerr << 'x';
    }
    std::cerr << " (Deviation: ";
    if (best_known_wct > 0) {
      std::cerr << pfsp::deviation(wct, best_known_wct);
    }
    else {
      std::cerr << 'x';
    }
    std::cerr << ')' << std::endl
              << std::endl;
    std::cerr.flush();
  }


  // Solve with the determined method
  if (pivoting == pfsp::PIVOTING_FIRST) {
    // First
    if (neighborhood == pfsp::NEIGHBORHOOD_TRANSPOSE) {
      pfsp_instance.iterative_first_improvement_transpose(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_EXCHANGE) {
      pfsp_instance.iterative_first_improvement_exchange(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_INSERT) {
      pfsp_instance.iterative_first_improvement_insert(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_INSERT_PERMUTATION) {
      pfsp_instance.iterative_first_improvement_insert_permutation
        (permutation, start_zone_length,
         max_duration, best_known_wct, quality_deviation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_VND_TRANSPOSE_EXCHANGE_INSERT) {
      pfsp_instance.iterative_vnd_first_improvement_tranpose_exchange_insert
        (permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_VND_TRANSPOSE_INSERT_EXCHANGE) {
      pfsp_instance.iterative_vnd_first_improvement_tranpose_insert_exchange
        (permutation);
    }
    else {
      std::cerr  << std::endl
                 << "Neighborhood method NOT implemented!" << std::endl
                 << std::endl;

      help(1);
    }
  }
  else if (pivoting == pfsp::PIVOTING_BEST) {
    // Best
    if (neighborhood == pfsp::NEIGHBORHOOD_TRANSPOSE) {
      pfsp_instance.iterative_best_improvement_transpose(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_EXCHANGE) {
      pfsp_instance.iterative_best_improvement_exchange(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_INSERT) {
      pfsp_instance.iterative_best_improvement_insert(permutation);
    }
    else {
      std::cerr  << std::endl
                 << "Neighborhood method NOT implemented!" << std::endl
                 << std::endl;

      help(1);
    }
  }
  else if (pivoting == pfsp::PIVOTING_NOTHING) {
    // Nothing (keep the initial solution)
  }
  else {
    // Tabu
    if (neighborhood == pfsp::NEIGHBORHOOD_EXCHANGE) {
      assert(pivoting == pfsp::PIVOTING_TABU);

      pfsp_instance.iterative_tabu_exchange(permutation,
                                            tabu_tenure,
                                            nb_worst_without_perturbation,
                                            max_duration,
                                            best_known_wct,
                                            quality_deviation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_INSERT) {
      assert(pivoting == pfsp::PIVOTING_TABU);

      pfsp_instance.iterative_tabu_insert(permutation,
                                          tabu_tenure,
                                          nb_worst_without_perturbation,
                                          max_duration,
                                          best_known_wct,
                                          quality_deviation);
    }
    else {
      std::cerr  << std::endl
                 << "Neighborhood method NOT implemented!" << std::endl
                 << std::endl;

      help(1);
    }
  }


  duration += std::chrono::duration<double>(std::chrono::steady_clock::now()
                                            - start).count();


  // Results
  pfsp::completion_times_table_type ctable;

  pfsp::completion_times_init(ctable,
                              pfsp_instance.nb_job(),
                              pfsp_instance.nb_machine());
  if (!pfsp::permutation_is_valid(permutation)) {
    std::cout << "Permutation INVALID!" << std::endl;
    pfsp::permutation_println(permutation);

    exit(1);
  }
  pfsp_instance.calculate_completion_times_table(permutation, ctable);

  const pfsp::time_type wct
    = pfsp_instance.weighted_sum_completion_times(ctable);

  const double deviation
    = (best_known_wct > 0
       ? pfsp::deviation(wct, best_known_wct)
       : 0);

  std::cout
    << basename(filename) << '\t'
    << duration << '\t'
    << wct << '\t';

  if (best_known_wct) {
    std::cout << best_known_wct << '\t' << deviation;
  }
  else {
    std::cout << "x\tx";
  }
  std::cout << std::endl;
  if (display_solution) {
    pfsp::permutation_println(permutation);
  }
  std::cout.flush();

  return EXIT_SUCCESS;
}
