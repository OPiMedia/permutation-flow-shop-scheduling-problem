#!/bin/sh

# Rebuild stats for algo 3 on all instances of size 50 (~10 minutes)

for i in $(seq 1 5)
do
    echo "============ Repetition $i ============"
    ./build_stats_algo.sh '--init-srz --insert --tabu --max-duration 4 --nb-worst 10 --tabu-tenure 15' 50 ../stats_results/50/stats-algo3-$i.txt
done
