#!/bin/sh

# Rebuild stats for tabu tenure testing for algo 1 on 10 first instances of size 100 (~46 minutes)

for i in $(seq 0 8)
do
    tenure=$(($i*5))
    echo "============ Tenure $tenure ============"
    ./build_stats_algo.sh "--init-random --exchange --tabu --max-duration 31 --nb-worst 10 --random-seed 666 --tabu-tenure $tenure" 100 ../stats_results/100/tabu_tenure/stats-algo1-tabu-tenure-$tenure.txt 10
done
