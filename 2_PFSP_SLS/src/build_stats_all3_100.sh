#!/bin/sh

# Rebuild stats for algo 3 on all instances of size 100 (~1 heure 18 minutes)

for i in $(seq 1 5)
do
    echo "============ Repetition $i ============"
    ./build_stats_algo.sh '--init-srz --insert --tabu --max-duration 31 --nb-worst 10 --tabu-tenure 20' 100 ../stats_results/100/stats-algo3-$i.txt
done
