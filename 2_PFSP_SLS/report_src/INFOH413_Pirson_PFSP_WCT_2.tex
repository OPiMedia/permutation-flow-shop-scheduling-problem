% -*- coding: utf-8 -*-
\documentclass[a4paper,ps2pdf,11pt]{article}
\usepackage{report}

\def\EMAILOPi{opi@opimedia.be}

\newcommand{\emailOPi}{\href{mailto:\EMAILOPi?subject=[INFO-H413 ex 2]}{\color{ColorEmail}\path|\EMAILOPi|}}

\newcommand{\course}{INFO-H413 Heuristic optimization}

\title{Implementation exercise 2\\
  \rule{\linewidth}{0.5mm}\\[1ex]
  {\Huge Permutation flow shop\\
    scheduling problem\\
    with the sum\\
    weighted completion time objective\\
    (PFSP-WCT)}\\
  \rule{\linewidth}{0.5mm}\\[3ex]
  Report}

\author{\begin{tabular}{r@{ }c@{ }l}%
    \href{http://www.opimedia.be/}{\color{black}\bfseries{Olivier \textsc{Pirson}}} & -- & \emailOPi
\end{tabular}}

\date{May 19, 2017}

\hypersetup{pdfauthor={Olivier Pirson <\EMAILOPi>}}
\hypersetup{pdftitle={INFO-H413 -- Implementation exercise 2 -- PFSP-WCT}}
\hypersetup{pdfsubject={Implementation and comparaison of two stochastic local search (SLS) algorithms for the permutation flow-shop problem (PFSP) with the sum weighted completion time objective (PFSP-WCT).}}
\hypersetup{pdfkeywords={heuristic,optimization,scheduling,PFSP,WCT,improvement,neighborhood,exchange,tabu search,TS}}

\lhead{INFO-H413 -- Implementation exercise 2 -- PFSP-WCT}
\rhead{Olivier \textsc{Pirson}}

\hyphenpenalty=10000
\sloppy

\setlength{\leftmargini}{1em}

\begin{document}
\begin{titlepage}%
  \newgeometry{margin=3cm,left=0cm,right=0cm}\center
  \textsc{\LARGE Université Libre de Bruxelles}\\[9ex]
  \textsc{\Large Computer Science Department}\\[2ex]
  \href{http://iridia.ulb.ac.be/~stuetzle/Teaching/HO/}{\color{black}\textsc{\Large\course}}\\[2ex]

  \bfseries{\LARGE\makeatletter\@title\makeatother}\\[6ex]

  \vspace*{3ex}%
  \Large\makeatletter\@author\makeatother\\[3ex]
  \vfill
  \large\makeatletter\@date\makeatother
  \vfill
  \includegraphics[width=4cm,height=4cm]{img/ULB}%
  \vfill
\end{titlepage}
\restoregeometry



\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}%
Implementation and comparaison of two stochastic local search (SLS) algorithms
for the permutation flow-shop problem (PFSP)
with the sum weighted completion time objective (PFSP-WCT).

The first one is a Hybrid method,
a Tabu Search (TS) with random perturbation.

The second one is a Very Large Scale Neighborhood (VLSN),
an Iterative First-Improvement method
with Insert neighborhood that increases with all permutations of increasing size.

\medskip
For tests, we use $30$ instances of {\color{Color50}size $50$}
and $30$ instance of {\color{Color100} size $100$}.

\medskip
Here a summary of computation times of all VND from the \nth{1} project.
\begin{table}[H]
  \begin{tabular}{|lll|r|r|}
    \hline
    Initial solution & Pivoting rule & Neighborhood & Time & Time\,$\times 500$\\
    \hline
    Random & first & VND-transpose-exchange-insert & 0.008 & 3.952\\
    Random & first & VND-transpose-insert-exchange & 0.008 & 3.816\\
    Simplified RZ & first & VND-transpose-exchange-insert & 0.006 & 3.174\\
    Simplified RZ & first & VND-transpose-insert-exchange & 0.006 & 3.233\\
    \hline
    Global average & & & 0.007 & 3.544\\
    \hline
  \end{tabular}
  \caption{Averages computation times (in seconds) for all VND from the \nth{1} project\\
    on instances of {\color{Color50}size $50$}.}
\end{table}

\begin{table}[H]
  \begin{tabular}{|lll|r|r|}
    \hline
    Initial solution & Pivoting rule & Neighborhood & Time & Time\,$\times 500$\\
    \hline
    Random & first & VND-transpose-exchange-insert & 0.068 & 33.926\\
    Random & first & VND-transpose-insert-exchange & 0.066 & 32.833\\
    Simplified RZ & first & VND-transpose-exchange-insert & 0.056 & 28.125\\
    Simplified RZ & first & VND-transpose-insert-exchange & 0.057 & 28.589\\
    \hline
    Global average & & & 0.062 & 30.868\\
    \hline
  \end{tabular}
  \caption{Averages computation times (in seconds) for all VND from the \nth{1} project\\
    on instances of {\color{Color100}size $100$}.}
\end{table}

For this project we fixe the computing time termination criterion
to $500$ times the result of these VND.
I rounded the two global averages
and I set the \textbf{computing time termination criterion} to:
\begin{itemize}
\item
  \textbf{$\mathbf{4}$ seconds} for instances of {\color{Color50}size $50$}
\item
  \textbf{$\mathbf{31}$ seconds} for instances of {\color{Color100}size $100$}
\end{itemize}


\newpage
\section{Implementation}
The two algorithm are implemented in C++,
with the general functionalities implemented for the \nth{1} project.
The structure remained the same.
Sources are available in \texttt{src/} directory.
In this directory are available these commands (\textit{among others}):
\begin{itemize}
\item
  \texttt{make ndebug all}
  --- compile all in \textbf{production mode}
  (before that, do \texttt{make distclean} to be sure to erase all development mode elements)
\item
  \texttt{make all}
  --- compile all in \textbf{development mode} (a lot of assertions check code correction)
\item
  \texttt{make tests}
  --- compile and run \textit{partial} unit tests (require \href{http://cxxtest.com/}{CxxTest}
  \footnote{
    \hrefShow{http://cxxtest.com/}})

\bigskip
\item
  \texttt{pfsp\_stats}
  --- (after compilation) \textbf{main program} to produce statistics on one instance (see \texttt{pfsp\_stats -{}-help}).
  Some useful informations are printed to the standard error stream,
  and main informations
  (filename, calculation time, WCT, best known WCT, deviation)
  are printed to the standard output stream separated by tabulation.
  \lstinputlisting[language=,basicstyle=\footnotesize]{pfsp_stats_help.txt}
\end{itemize}

One usage example of the program for each two algorithm:

\noindent
\texttt{pfsp\_stats -{}-init-srz -{}-exchange -{}-tabu -{}-tabu-tenure 15 -{}-nb-worst 10\\
  \null\hfill-{}-max-duration 4 ../instances/50\_20\_24}

Run the first
\footnote{
  Be careful when you look in directories
  because this is the algo 2 in all data results files and script files.}
algorithm on the instance \texttt{../instances/50\_20\_24} with parameters:
\begin{itemize}
\item
  A tabu tenure (the size of the tabu) of $15$.
\item
  A value of $10$ for the $nb\_worst\_without\_perturbation$ parameter.
  See the description of the algorithm.
\item
  After $4$ seconds the program stops.
\end{itemize}

\noindent
\texttt{pfsp\_stats -{}-init-srz -{}-insert-permutation -{}-first -{}-start-zone-length 5\\
  \null\hfill-{}-quality-deviation 1.5 ../instances/100\_20\_03}

Run the second
\footnote{
  Be careful when you look in directories
  because this is the algo 4 in all data results files and script files.}
algorithm on the instance \texttt{../instances/100\_20\_03} with parameters:
\begin{itemize}
\item
  A value of $5$ for the $start\_zone\_length$ parameter. See the description of the algorithm.
\item
  When a solution with quality of average relative \% deviation $\leq 1.5$ is founded,
  then the program stops.
\end{itemize}


Other directories:
\begin{itemize}
\item
  \texttt{src/docs/html/} contains an automatically generated source code
  \href{src/docs/html/index.html}{HTML documentation}
  \footnote{
    If this PDF document is preserved in its original directory,
    then this kind of link is available to open HTML page of this documentation.}
\item
  \texttt{instances/} contains all instances and the \texttt{bestSolutions.txt} file.
\item
  \texttt{stats\_project\_1/} contains VND results from the \nth{1} project
\item
  \texttt{stats\_results/} contains all results obtained by a multitude of \texttt{pfsp\_stats} executions
\item
  \texttt{stats/} contains processed results
\item
  \texttt{graph/} contains scripts to process results and build graphs
\item
  \texttt{report\_src/} contains \LaTeX{} sources for this document
\end{itemize}


\subsubsection{Environment used to obtain results}
All results was calculated on an old Dell Precision T3500:
\begin{itemize}
\item
  Processor Intel Xeon quad core W3520 \textbf{2,8 GHz}
  (only one core was used by the implementation)
\item
  \begin{tabular}[t]{@{}l@{\ }r@{\ }l}
    Cache L$1$d: & $32$ & Kb\\
    Cache L$1$i: & $32$ & Kb\\
    Cache L$2$: & $256$ & Kb\\
    Cache L$3$: & $8$ & Mb
  \end{tabular}
\item
  RAM: 6 Gb
\item
  Operating system: Debian GNU/Linux 8 (Jessie) \textbf{64 bits}.
\item
  C++ compiler: \textbf{g++ 4.9.2} with options \texttt{-DNDEBUG -O3 -s}.
\end{itemize}



\newpage
\section{First algorithm -- Hybrid Tabu Search (TS), with random perturbation}
I have tried several version of Tabu Search.
The first was save entire solutions in tabu
\footnote{
  In fact a hash of solution, to have good performance.}.
With this only action, the tabu search was just an Iterative Improvement (II)
with best improvement.
So I have added a random perturbation,
with the idea of the $nb\_worst$ counter found in \cite{GLS2010} p.$124$.

\medskip
After that I have tried to save the values swapped instead the entire solutions,
or only one of theses values.

I have had better results by saving the most left value swapped.
In the implementation it is the outermost loop of the double loop,
so the innermost loop is avoided when the item is tabu.

I chosen this method.

\medskip
I have also tried two neighborhoods: exchange and insert.
The exchange neighborhood give me better results.

\medskip
Finally I have tried random and simplified RZ initial solution.
Without surprise simplified RZ initial solution give better results.

\bigskip
Pseudocode of the algorithm
(implemented by the
\href{src/docs/html/pfsp_8cpp_source.html#l00704}{Pfsp::iterative\_tabu\_exchange} method):

\begin{lstlisting}
tabu tenure $\leftarrow$ the chosen value of tabu size
$nb\_worst\_without\_perturbation \leftarrow$ the chosen value of the number of consecutive steps
                    without improvement before to perform a random perturbation
$s \leftarrow$ initial solution by Simplified RZ  // $s$ is the current solution
$nb\_worst \leftarrow 0$  // the number of consecutive steps without improvement
Repeat
    $s' \leftarrow$ best non-tabu solution in exchange neighborhood ($a$ and $b$ swapped)

    add $a$ to tabu
    If size of tabu > tabu tenure then
        remove oldest item from tabu

    If $s > s'$ then  // better solution
        $s \leftarrow s'$
    else         // no better found
        $nb\_worst \leftarrow nb\_worst + 1$
        If $nb\_worst > nb\_worst\_without\_perturbation$
            $nb\_worst \leftarrow 0$
            Repeat
                $s \leftarrow$ random exchange $a$ and $b$ in $s$  // random perturbation
            until ($a$ is not tabu) or (number of tries > size of problem)

            add $a$ to tabu
            If size of tabu > tabu tenure then
                remove oldest item from tabu

until (computing time exceeded) or (quality deviation reached)
\end{lstlisting}

The tabu structure is implemented with a list \textit{and} a set.
The list is used to keeping the order of insertion, and to remove the oldest item when is necessary.
The set is used to check quickly if an item is tabu or not.


\subsection{Determination of a good Tabu Tenure}
By default the program set the Tabu Tenure to the square root of the size of the problem.

To determine a better value I ran the algorithm on instances with value between $0$ and $40$.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{../graph/50_100/graph_tabu_tenure-2.eps}%
  %
  \caption{Average relative \% deviation depending of tabu tenure\\
    on $10$ first instances of {\color{Color50}size $50$}
    and on $5$ first instances of {\color{Color100}size $100$}\\
    (with the $nb\_worst\_without\_perturbation$ parameter fixed to $10$).}
\end{figure}

For instances of {\color{Color50}size $50$} the result show expected behaviour:
with very small values, the memory of the tabu is not enough to keep good informations.
And with bigger values, too much items are forbidden.

For instances of {\color{Color100}size $100$} the difference is less clear,
but it seems to have the same behaviour.

I fixed the \textbf{Tabu Tenure} to:
\begin{itemize}
\item
  \textbf{$\mathbf{15}$} for instances of {\color{Color50}size $50$}
\item
  \textbf{$\mathbf{20}$} for instances of {\color{Color100}size $100$}
\end{itemize}



\subsection{Determination of a good $nb\_worst\_without\_perturbation$ parameter}
\enlargethispage{\baselineskip}%
In the same way, to determine a better value for the
$\mathbf{nb\_worst\_without\_perturbation}$ parameter
I ran the algorithm on the instances
with the value between $0$ and $60$.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{../graph/50_100/graph_nb_worst-2.eps}%
  %
  \caption{Average relative \% deviation depending of the $nb\_worst\_without\_perturbation$ parameter
    on all instances of {\color{Color50}size $50$}
    and on $10$ first instances of {\color{Color100}size $100$}.}
\end{figure}

For all instances results are rather stable.
With a value of $0$
(the algorithm make a random perturbation each times
that it not found a better non-tabu solution in the neighborhood)
the result is cleary less good.
Beyond, results are stable, with a slightly increasing.

I fixed the $\mathbf{nb\_worst\_without\_perturbation}$ parameter to
\textbf{$\mathbf{10}$} for all instances.



\section[Second algorithm -- Very Large Scale Neighborhood (VLSN)]
        {Second algorithm -- Very Large Scale Neighborhood (VLSN):\\
          Iterative First-Improvement with Insert and increasing permutations neighborhood}
I chosen to use the best algorithm from the \nth{1} project,
the Iterative First-Improvement with Insert neighborhood
\footnote{
  The result of Iterative \textit{Best}-Improvement with Insert neighborhood
  was quite similar, but less fast.}
and to increase this neighborhood with some exhaustive walk
on all permutations of size $3$
\footnote{
  See below for the determination of this parameter.}, then size $4$, and so forth.

\medskip
The First pivoting rule is an important choice,
to avoid a complete tour of the neighborhood,
because it increases exponentially with the size of permutations.
And when a better solution is found, the size of permutations is reseted to $3$.

\medskip
Finally I start with a Simplified RZ initial
to have quickly a good enough starting solution.

\medskip
So the entire algorithm is deterministic.

\medskip
Remarks that this algorithm is an exact algorithm,
it give always the optimal solution\dots{}
if we give it enough (finite) computing time\dots{}
that is impossible as soon as the size of the problem is not trivial.

\medskip
For an Insertion from position $i$ to position $j$ with $i < j$,
the permutations are performed in the zone $(i, i + 1, \dots, j)$,
starting at position $i$ with a size $l$
and with respect to the end of this zone at position $j$.

\smallskip
If $i > j$,
it is the same but starting at position $j$
in the zone $(j, j + 1, \dots, i)$.

\medskip
The idea of the choice of this zone to perform permutations
is to take advantage of the implementation of the evaluation function of WCT.
The implementation can calculate new value from ancien value
and recomputes only from the first modified position of the solution.
So we can modify the solution starting at position $\min(i, j)$
without require more calculation for this function.
\footnote{
  For all first-improvement like algorithms,
  I think that iterate on indices in the reverse order
  would take a better advantage of this implementation of the evalution function.
  Indeed if we begin from the right,
  all tests are faster than if we begin from the left.

  I think about that too late.
  I already had a lot of results and not time to recompute them.
  So I do not try this possible optimization.}

\bigskip
Pseudocode of the algorithm\\
(implemented by the
\href{src/docs/html/pfsp_8cpp_source.html#l00528}
     {Pfsp::iterative\_first\_improvement\_insert\_permutation} method):

\begin{lstlisting}
$start\_zone\_length \leftarrow 3$  // minimal length for the permutations zone
$l \leftarrow start\_zone\_length$  // length of the permutations zone
$s \leftarrow$ initial solution by Simplified RZ  // $s$ is the current solution
Repeat
  next:
    For $i \leftarrow 1$ to $n$
       For $j \leftarrow 1$ to $n$ with $j \neq i$
            $s' \leftarrow s$ with item at position $i$ removed and inserted to position $j$
            If $s > s'$ then  // better solution
                $s \leftarrow s'$
                $l \leftarrow start\_zone\_length$

                Jump to next
            else
                // Permutation neighborhood on size (at most) $l$
                // in the zone between $i$ and $j$ positions
                $a \leftarrow \min(i, j)$
                $b \leftarrow \min(a + l, \max(i, j))$
                For each permutation of value in the zone $(a, a + 1, \dots, b)$
                    $s' \leftarrow s$ with the corresponding permutation
                    If $s > s'$ then  // better solution
                        $s \leftarrow s'$
                        $l \leftarrow start\_zone\_length$

                        Jump to next

                $l \leftarrow l + 1$  // Will be try with larger size of permutations

until (computing time exceeded) or (quality deviation reached) or ($l > n$)
\end{lstlisting}


\subsection{Determination of a good $start\_zone\_length$ parameter}
When not better solution was found
the length of the permutation zone is incremented
and this larger neighborhood is tried.

Of course in this case the previous neighborhood is a subset of the new neighborhood.
So when the algorithm loops without found better solution,
it check several times the same some solutions.

Again, to determine a better value for this parameter
I ran the algorithm on the instances
with the value between $2$ (it is necessary to have at least 2 elements to consider permutation)
and $10$ (no more, because the number of permutations increases exponentially compared to the length).

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{../graph/50_100/graph_start_zone_length-4.eps}%
  %
  \caption{Average relative \% deviation depending of the $start\_zone\_length$ parameter
    on all instances of {\color{Color50}size $50$}
    and on $10$ first instances of {\color{Color100}size $100$}.}
\end{figure}

I fixed the $\mathbf{start\_zone\_length}$ parameter to \textbf{$\mathbf{3}$} for all instances.
So, if the Insert operation do not give a better solution,
the algorithm check directly the $3! = 6$ permutations of length $3$.



\newpage
\section{Results}
The two algorithms was executed on all instances of size {\color{Color50}$50$} and {\color{Color100}$100$}.
The first one was executed five times on each instance, and results are the average.
The second one is completely deterministic so it was executed only once on each instance.
\footnote{
  The difference of computing time between two execution is negligible.}


\subsection{Average relative \% deviation}
Only the two first algorithms in following tables are really considered in this project.
Other results are given as additional informations.

The number ``\# algo in files'' is the identifiant for each algorithm
in all data results files and script files.

\begin{table}[H]
  \null\hspace*{-0.5cm}%
  \begin{tabular}{|llll|r|r|r|}
    \hline
    Type & Initial solution & Pivoting & Neighborhood & \textbf{Deviation} & Time & \# algo in files\\
    \hline
    \input{../stats/50/table.tex}
    \hline
  \end{tabular}
  \caption{Average relative \% deviation and average computation time (in seconds)\\
    sorted by average relative \% deviation on instances of {\color{Color50}size $50$}.}
\end{table}

\begin{table}[H]
  \null\hspace*{-0.5cm}%
  \begin{tabular}{|llll|r|r|r|}
    \hline
    Type & Initial solution & Pivoting & Neighborhood & \textbf{Deviation} & Time & \# algo in files\\
    \hline
    \input{../stats/100/table.tex}
    \hline
  \end{tabular}
  \caption{Average relative \% deviation and average computation time (in seconds)\\
    sorted by average relative \% deviation on instances of {\color{Color100}size $100$}.}
\end{table}

The Tabu Search is a little better on instances of {\color{Color50}size $50$}
and the VLSN is a little better on instances of {\color{Color100}size $100$}
and on the global average on all instances,
but the two studied algorithms have quite similar results.

Nevertheless this two algorithms are better than all other algorithms tried
(and all algorithms developped in the \nth{1} project).

\begin{table}[H]
  \null\hspace*{-0.5cm}%
  \begin{tabular}{|llll|r|r|r|}
    \hline
    Type & Initial solution & Pivoting & Neighborhood & \textbf{Deviation} & Time & \# algo in files\\
    \hline
    \input{../stats/50_100/table.tex}
    \hline
  \end{tabular}
  \caption{Average relative \% deviation and average computation time (in seconds)\\
    sorted by average relative \% deviation
    on instances of size {\color{Color50}$50$} and {\color{Color100}$100$}
    (like in the \nth{1} project).}
\end{table}


\subsection{Correlation plot of the average relative \% deviation}
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../graph/50_100/graph_correlation-2-4.eps}%
  \caption{Correlation of the average relative \% deviation
    on all instances of size {\color{Color50}$50$} and {\color{Color100}$100$}.}
\end{figure}

The correlation plot confirms the similarity of results for the two algorithms
with a slightly preference for the VLSN,
for each size, and globally.

We see there is one instance of each size
({\color{Color50}50\_20\_14} and {\color{Color100}100\_20\_07})
that obtains clearly a better result with the VLSN.
Strangely the result for theses two instances are very close.


\subsection{Statistical tests}
$p$-values for \textsc{Student} and \textsc{Wilcoxon} statistical tests
on average relative \% deviation

\medskip
\begin{tabular}{|l|l|l|}
  \hline
  Algorithm & \textsc{Student} & \textsc{Wilcoxon}\\
  \hline
  Tabu Search & 0.613339 & 0.477106\\
  VLSN & 0.393360 & 0.338742\\
  \hline
\end{tabular}

\medskip
For any reasonable significance level $\alpha$ (for example $0.01$ or $0.05$),
the statistical tests confirm the similarity of results of these two algorithms.


\subsection{Run-time distributions to reach quality solutions}
To study run-time distributions necessary to reach sufficiently high quality solutions
it was asked to execute $25$ times each algorithm
on the $5$ first instances of {\color{Color50}size $50$}.
During $10$ times the time termination criterion used before,
that is to say $4$ seconds $\times 10 = \mathbf{40}$ seconds.

I did test during $\mathbf{200}$ seconds to see a larger evolution.

Like before, the VLSN was executed only once by instance, because it is deterministic.


\newpage
\subsubsection{For average relative \% deviation $\leq 2$}
The probability to have good enough solutions with VLSN rises quickly.
But after that it no longer progresses, for a long time.

\medskip
The probability for the Tabu Search is more slow in the beginning,
but it continue to increase until the maximum probability.

\medskip
The VLSN is long time in a plateau before reached the maximum probability.
In fact the size of neighborhood of this algorithm increase exponentially
when it do not found better solution directly. That explains these successive plateaus.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-2-short.eps}\\
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-2.eps}%
  %
  \caption{Run-time distributions to reach $\mathbf{2}$\% of quality solution\\
    on the $5$ first instances of {\color{Color50}size $50$}.\\
    (There is no more evolution after $60$ seconds,\\
    the maximum probability is reached for the two algorithms.)}
\end{figure}


\newpage
\subsubsection{For average relative \% deviation $\leq 1.5$}
For this quality of average relative \% deviation constatations are the same,
with respect that it is less probable to reach the necessary quality.

\medskip
For the VLSN, until the $200$ seconds studied, it not reached the quality for all instances.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-1.5-short.eps}\\
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-1.5.eps}%
  %
  \caption{Run-time distributions to reach $\mathbf{1.5}$\% of quality solution\\
    on the $5$ first instances of {\color{Color50}size $50$}.\\
    (There is no more evolution after $120$ seconds, until $200$ seconds.)}
\end{figure}


\newpage
\subsubsection{For average relative \% deviation $\leq 1$}
Same constatations.
But the Tabu Search nor can reach the quality for all instances.

And the VLSN reach quality once every two just before $200$ seconds.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-1-short.eps}\\
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-1.eps}%
  %
  \caption{Run-time distributions to reach $\mathbf{1}$\% of quality solution\\
    on the $5$ first instances of {\color{Color50}size $50$}.}
\end{figure}


\newpage
\subsubsection{For average relative \% deviation $\leq 0.5$}
For this more highly quality of solutions,
the behavior changes.

The Tabu Search is the first to find a good enough solution.
But very quickly the VLSN finds much more.

And the two algorithms is in a plateau until to the $200$ seconds studied.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-0.5-short.eps}\\
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-0.5.eps}%
  %
  \caption{Run-time distributions to reach $\mathbf{0.5}$\% of quality solution\\
    on the $5$ first instances of {\color{Color50}size $50$}.\\
    (There is no more evolution after $120$ seconds, until $200$ seconds.)}
\end{figure}


\newpage
\subsubsection{For average relative \% deviation $\leq 0.1$}
For this very high quality of solutions,
neither of the two algorithms found a good enough solution
during the $200$ seconds.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{../graph/50_100/graph_reach_deviation-0.1.eps}%
  %
  \caption{Run-time distributions to reach $\mathbf{0.1}$\% of quality solution\\
    on the $5$ first instances of {\color{Color50}size $50$}.\\
    (There is no evolution at all.)}
\end{figure}



\newpage
\section{Conclusions}
I implemented this VLSN about a simple personal idea,
without know if it was a good idea.

After all these results, it appears that is better than my Tabu Search implementation.

It quickly found good solution.
Maybe it would be interesting to modify it such
that it restarts to a random solution when the size of permutations reaches some value,
instead to get lost in too big exhaustif research.

\medskip
Several ideas tried during the developpement of this projet
showed to me the difficulty to quit local optimum.
\footnote{
  I known that before, but it is more concrete now.}
It is always a tradeoff between to explore locally
that increases the quality of the solution until a local optimum,
and to ``restart'' somewhere else, without guarantee to obtain a better local optimum.

\medskip
A final remark about the problem to be solved.
In fact all this study neglects the specificity of the problem tested.
Indeed, algorithms developed only care about permutations
\footnote{
  And of course uses the evaluation function of the problem,
  that is specific to this problem.
  But all other problem formalized in term of permutations,
  with its proper evaluation function,
  could be studied in the exact same manner.}.
The algorithms are indirectly customized for the PFSP-WCT
by choice of its parameters in function of results on test instances.


\newpage
\section*{Appendices}
\addcontentsline{toc}{section}{Appendices}%
\subsection{Some correlation plots with other algorithms}
Comparisons between the best algorithm from the \nth{1} project
and the two algorithms considered in this project.

\begin{figure}[H]
  \centering
  \null\hspace*{-3cm}%
  \includegraphics[width=0.75\textwidth]{../graph/50_100/graph_correlation-0-1.eps}%
  \hspace*{-4cm}%
  \includegraphics[width=0.75\textwidth]{../graph/50_100/graph_correlation-0-2.eps}%
  \hspace*{-3cm}\null
  %
  \caption{Correlation of the average relative \% deviation
    on all instances of size {\color{Color50}$50$} and {\color{Color100}$100$}}
\end{figure}

\bigskip
Comparisons between Tabu Search considered in this project
and two alternative versions less efficient tried
(with a random initial solution
or with an insert neighborhood).

\begin{figure}[H]
  \centering
  \null\hspace*{-3cm}%
  \includegraphics[width=0.75\textwidth]{../graph/50_100/graph_correlation-1-2.eps}%
  \hspace*{-4cm}%
  \includegraphics[width=0.75\textwidth]{../graph/50_100/graph_correlation-2-3.eps}%
  \hspace*{-3cm}\null
  %
  \caption{Correlation of the average relative \% deviation
    on all instances of size {\color{Color50}$50$} and {\color{Color100}$100$}}
\end{figure}



\newpage
\begin{thebibliography}{9}%
\addcontentsline{toc}{section}{References}%
\bibitem[\textsc{GLS} 2010]{GLS2010}
  Lin-Yu \textsc{Tseng},
  Ya-Tai \textsc{Lin}.\\
  \href{http://www.sciencedirect.com/science/article/pii/S0925527310001830}{\textbf{\textit{A genetic local search algorithm for minimizing total flowtime in the permutation}}}
  \href{http://www.sciencedirect.com/science/article/pii/S0925527310001830}{\textbf{\textit{flowshop scheduling problem}}}
  \footnote{
    \hrefShow{http://www.sciencedirect.com/science/article/pii/S0925527310001830}}.\\
  \textit{International Journal Production Economics}, 127:121--128, 2010.

\bibitem[\textsc{HO} 2017]{HO2017}
  Thomas \textsc{Stützle}.\\
  \href{http://iridia.ulb.ac.be/~stuetzle/Teaching/HO/}{\textbf{\textit{Heuristic Optimization}}}
  \footnote{
    \hrefShow{http://iridia.ulb.ac.be/~stuetzle/Teaching/HO/}}.\\
  2017.

\bibitem[\textsc{SLS} 2004]{SLS2004}
  Holger \textsc{Hoos},
  Thomas \textsc{Stützle}.\\
  \href{http://www.sls-book.net/}{\textbf{\textit{Stochastic Local Search: Foundations and Applications}}}
  \footnote{
    \hrefShow{http://www.sls-book.net/}}.\\
  Elsevier, 2004.

\end{thebibliography}



\listoftodos

\tableofcontents

\bigskip
\null
\hrule

\newpage
\null\vfill
\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{img/xkcd_303_compiling}%
  \caption[\href{https://xkcd.com/303/}{xkcd $303$: \textit{Compiling}}]
          {\href{https://xkcd.com/303/}{xkcd $303$: \textit{Compiling}}
           \footnotemark}
\end{figure}
\footnotetext{
  \hrefShow{https://xkcd.com/303/}}
\vfill\null

\end{document}
