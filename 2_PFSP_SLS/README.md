# Permutation Flow Shop Scheduling Problem (PFSP-WCT)

In the `src/` directory:

  - `make ndebug all`: build all program in **production mode** (before that, made `make distclean`)
  - `make`: build all programs in **development mode** (the code contains a lot of assertions)
  - `make tests`: build and run *partial* unit tests (require CxxTest)
  - `pfsp_stats`: **main program** to solve and produce stats on one instance (see `pfsp_stats --help`)

Tested with *g++ (Debian 4.9.2-10) 4.9.2* on *Debian GNU/Linux 8 (jessie) 64 bits*
