deviations <- function(name) {
    data <- read.csv(paste("../stats/", name, ".txt", sep=""), sep="\t")
    data <- data[order(data$filename), ]

    a <- data$deviation
}


test1 <- function(data, nameA, nameB) {
    a <- data(nameA)
    b <- data(nameB)

    student <- t.test(a, b, paired=T)$p.value
    wilcoxon <- wilcox.test(a, b, paired=T)$p.value

    student <- sprintf("%f", student)
    wilcoxon <- sprintf("%f", wilcoxon)
    print(wilcoxon)

    return (c(student, wilcoxon))
}


test <- function(nameA, nameB) {
    tDeviation <- test1(deviations, nameA, nameB)

    result <- c(nameA, nameB, tDeviation, "\n")

    return(cat(result, sep=" & "))
}



test("50/stats-algo2", "50/stats-algo4")

test("100/stats-algo2", "100/stats-algo4")
