#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
vnd_average.py (May 17, 2017)

Build 'vnd_averages.txt'
with stats of all VND results from project 1.

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import glob

SCALE = 500


############
# Function #
############
def read_datas_to_average(filename, size):
    time = 0.0
    nb = 0

    with open(filename) as fin:
        assert (fin.readline()
                == 'filename\ttime\twsct\n')

        for line in fin:
            columns = line.split()

            assert len(columns) == 3

            name = columns[0]
            if name.split('_')[0] == str(size):
                time += float(columns[1])
                nb += 1

    time /= nb

    return time


########
# Main #
########
def main():
    for size in (50, 100):
        print('===== size {} ====='.format(size))
        global_avg = 0
        nb = 0

        for filename in sorted(glob.glob('stats-*.txt')):
            time = read_datas_to_average(filename, size)
            print('{}\t{:.3f}\t{:.3f}'.format(filename, time, time*SCALE))
            global_avg += time
            nb += 1

        global_avg /= nb
        print('                                                \t{:.3f}\t{:.3f}'
              .format(global_avg, global_avg*SCALE))

if __name__ == '__main__':
    main()
