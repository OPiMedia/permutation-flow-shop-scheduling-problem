# PFSP-WCT - project 2 stats results

All results was calculated on a Dell Precision T3500
with a processor Intel Xeon quad core W3520 2,8 GHz
(only one core was used by the implementation)
with the C++ program pfsp_stats.

Operating system: Debian GNU/Linux 8 (Jessie) 64 bits.

C++ compiler: `g++ 4.9.2 with options -DNDEBUG -O3 -s`.
