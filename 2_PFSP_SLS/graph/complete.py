#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
complete.py (May 19, 2017)

From '../stats_results/[SIZE]/stats-algo*.txt' data files
build files:
  '../stats/[SIZE]/stats-algo*.txt'
  '../stats/[SIZE]/correlation-*.txt'
  '../stats/[SIZE]/table.tex'
with [SIZE] = 50 and 100.

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import glob
import sys


#############
# Functions #
#############
def average(datas):
    averages = dict()

    for name, data in sorted(datas.items()):
        best_known = None

        avg_time = 0
        avg_wct = 0
        avg_deviation = 0
        for time, wct, this_best_known, deviation in data:
            if best_known is None:
                best_known = this_best_known
            else:
                assert best_known == this_best_known

            avg_time += time
            avg_wct += wct
            avg_deviation += deviation

        nb = len(data)
        avg_time /= nb
        avg_wct /= nb
        avg_deviation /= nb

        if (round(avg_deviation*100)
                != round(percentage_deviation(avg_wct, best_known)*100)):
            print('{} != {}'.format(avg_deviation,
                                    percentage_deviation(avg_wct, best_known)))

        averages[name] = (avg_time, avg_wct, avg_deviation)

    return averages


def int_if(x):
    return (int(x) if int(x) == x
            else x)


def percentage_deviation(wct, best_known):
    assert wct >= best_known, (wct, best_known)

    return float((wct - best_known)*100)/best_known


def read_datas(filename, size, datas):
    def to_number(s):
        if s.isdigit():
            n = int(s)
        else:
            n = int_if(float(s))

        return n

    with open(filename) as fin:
        assert (fin.readline()
                == 'filename\ttime\twct\tbest known\tdeviation\n')

        for line in fin:
            columns = line.split()

            assert len(columns) == 5

            name = columns[0]
            if name[:len(str(size)) + 1] == '{}_'.format(size):
                if name not in datas:
                    datas[name] = []

                datas[name].append(tuple(to_number(value)
                                         for value in columns[1:]))


def write_averages(filename, averages):
    with open(filename, 'w') as fout:
        print('filename\ttime\twct\tdeviation', file=fout)

        for name, avg in sorted(averages.items()):
            print('{}\t{:.7f}\t{:.7f}\t{:.7f}'.format(name, *avg),
                  file=fout)


def write_average_tex(filename, all_averages):
    avgs = []
    for algo, averages in enumerate(all_averages):
        time = 0.0
        deviation = 0.0
        for _, avg in sorted(averages.items()):
            time += avg[0]
            deviation += avg[2]

        nb = len(averages)
        time /= nb
        deviation /= nb

        avgs.append((deviation, time, algo))

    avgs.sort()

    algo_names = ('Simple & Simplifed RZ & first & insert',
                  'Hybrid & Random & tabu & exchange',
                  'Hybrid & Simplifed RZ & tabu & exchange',
                  'Hybrid & Simplifed RZ & tabu & insert',
                  'Simple VLSN & Simplifed RZ & first & insert-permutation')

    with open(filename, 'w') as fout:
        for avg in avgs:
            color = ('' if avg[2] in (2, 4)
                     else r'\color{gray}')
            print(r'{color}{name} & {color}{0:.3f} & {color}{1:.3f} & {color}{2}\\'
                  .format(*avg,
                          color=color,
                          name=algo_names[avg[2]].replace('& ',
                                                          '& {}'.format(color))),
                  file=fout)


def write_correlation(filename, all_averages, i, j):
    with open(filename, 'w') as fout:
        print('filename\tdeviation algo 1\tdeviation algo 2', file=fout)

        for name, avg in sorted(all_averages[i].items()):
            print('{}\t{:.7f}\t{:.7f}'.format(name, avg[2],
                                              all_averages[j][name][2]),
                  file=fout)


########
# Main #
########
def main():
    all_size = []

    for size in (50, 100):
        all_averages = []

        for algo in range(5):
            datas = dict()

            print('=== Size {}, Algo {} ==='.format(size, algo))
            for filename in sorted(glob.glob('../stats_results/{}/stats-algo{}-*.txt'
                                             .format(size, algo))):
                read_datas(filename, size, datas)

            averages = average(datas)

            write_averages('../stats/{}/stats-algo{}.txt'.format(size, algo),
                           averages)

            all_averages.append(averages)

        all_size.append(all_averages)

        write_correlation('../stats/{}/correlation-0-1.txt'.format(size),
                          all_averages, 0, 1)
        write_correlation('../stats/{}/correlation-0-2.txt'.format(size),
                          all_averages, 0, 2)
        write_correlation('../stats/{}/correlation-1-2.txt'.format(size),
                          all_averages, 1, 2)
        write_correlation('../stats/{}/correlation-2-3.txt'.format(size),
                          all_averages, 2, 3)
        write_correlation('../stats/{}/correlation-2-4.txt'.format(size),
                          all_averages, 2, 4)

        write_average_tex('../stats/{}/table.tex'.format(size), all_averages)

        correlation_min = sys.maxsize
        correlation_max = -1
        for averages in all_averages:
            for avg in averages.values():
                correlation_min = min(correlation_min, avg[2])
                correlation_max = max(correlation_max, avg[2])

        print("Correlation size {}\tMin: {}\tMax: {}"
              .format(size, correlation_min, correlation_max))

    for k in range(len(all_size[0])):
        for name in sorted(all_size[0][k]):
            data50 = all_size[0][k][name]
            data100 = all_size[1][k]['100' + name[2:]]
            avg = tuple((data50[i] + data100[i])/2 for i in range(3))
            all_size[0][k][name] = avg

    write_average_tex('../stats/50_100/table.tex', all_size[0])

if __name__ == '__main__':
    main()
