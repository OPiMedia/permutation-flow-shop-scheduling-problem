#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
start_zone_length.py (May 16, 2017)

From '../stats_results/[SIZE]/start_zone_length/stats-algo4-start-zone-length-*.txt' data files
build files:
  'stats/[SIZE]/start_zone_length/stats-algo4-start-zone-length.txt'
with [SIZE] = 50 and 100.

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import glob
import os.path


############
# Function #
############
def read_datas_to_average(filename):
    time = 0.0
    wct = 0.0
    best = 0.0
    deviation = 0.0
    nb = 0

    with open(filename) as fin:
        assert (fin.readline()
                == 'filename\ttime\twct\tbest known\tdeviation\n')

        for line in fin:
            nb += 1
            columns = line.split()

            assert len(columns) == 5

            time += float(columns[1])
            wct += float(columns[2])
            best += float(columns[3])
            deviation += float(columns[4])

    time /= nb
    wct /= nb
    best /= nb
    deviation /= nb

    return (time, wct, best, deviation)


########
# Main #
########
def main():
    for size in (50, 100):
        algo = 4
        datas = dict()

        print('=== Size {}, Algo {} ==='.format(size, algo))

        output = []

        for filename in sorted(glob.glob('../stats_results/{}/start_zone_length/stats-algo{}-start-zone-length-*.txt'
                                         .format(size, algo))):
            start_zone_length = int(os.path.splitext(filename)[0]
                           .split('-')[-1])

            time, wct, best, deviation = read_datas_to_average(filename)

            output.append((start_zone_length, time, wct, best, deviation))

        with open('../stats/{}/start_zone_length/stats-algo{}-start-zone-length.txt'
                  .format(size, algo), 'w') as fout:
            print('length\ttime\twct\tbest known\tdeviation', file=fout)

            for line in sorted(output):
                print('{}\t{:.9f}\t{:.9f}\t{:.9f}\t{:.9f}'.format(*line),
                      file=fout)

if __name__ == '__main__':
    main()
