#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
reach4_deviation.py (May 17, 2017)

From '../stats_results/50/reach/stats-algo[ALGO]-reach-deviation-2-[ITERATION]' data files
build files:
  'stats/50/reach/stats-algo[ALGO]-reach-deviation.txt'.

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import glob


############
# Function #
############
def read_datas_to_distribution(filename, quality_deviation):
    nb = 0

    data = []
    for filename in sorted(glob.glob(filename.replace('-1.txt', '-*.txt'))):
        with open(filename) as fin:
            assert (fin.readline()
                    == 'filename\ttime\twct\tbest known\tdeviation\n')

            for line in fin:
                nb += 1
                columns = line.split()

                assert len(columns) == 5

                data.append((float(columns[1]), float(columns[4])))

    distribution = []
    for i in range(200*1000 + 1):
        time_step = i*0.001
        success = 0

        for time, deviation in data:
            if (time <= time_step) and (deviation <= quality_deviation):
                success += 1

        distribution.append((time_step, float(success)/nb))

    return distribution


########
# Main #
########
def main():
    for algo in (2, 4):
        print('===== {} ====='.format(algo))

        for quality_deviation in ('0.1', '0.5', 1, '1.5', 2):
            for filename in sorted(glob.glob('../stats_results/50/reach/stats-algo{}-reach-deviation-{}-1.txt'
                                             .format(algo, quality_deviation))):
                distribution = read_datas_to_distribution(filename,
                                                          float(quality_deviation))

            with open('../stats/50/reach/stats-algo{}-reach-deviation-{}.txt'
                      .format(algo, quality_deviation), 'w') as fout:
                print('time\tprobability', file=fout)

                for d in distribution:
                    print('{:.9f}\t{:.9f}'.format(*d), file=fout)

if __name__ == '__main__':
    main()
