# *Permutation Flow-shop Scheduling Problem* (PFSP)
Implementations in C++
for two projects of the *INFO-H413 Heuristic optimisation* course. (17/20 and 18/20)

  - Project 1: [1_PFSP/](https://bitbucket.org/OPiMedia/permutation-flow-shop-scheduling-problem/src/master/1_PFSP/)
    - Report: [1_PFSP/report/INFOH413_Pirson_PFSP.pdf](https://bitbucket.org/OPiMedia/permutation-flow-shop-scheduling-problem/raw/master/1_PFSP/report/INFOH413_Pirson_PFSP.pdf)
  - Project 2: [2_PFSP_SLS/](https://bitbucket.org/OPiMedia/permutation-flow-shop-scheduling-problem/src/master/2_PFSP_SLS/)
    - Report: [INFOH413_Pirson_PFSP_WCT_2.pdf](https://bitbucket.org/OPiMedia/permutation-flow-shop-scheduling-problem/raw/master/2_PFSP_SLS/INFOH413_Pirson_PFSP_WCT_2.pdf)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

  - 📧 <olivier.pirson.opi@gmail.com>
  - Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
  - other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](http://www.gnu.org/licenses/gpl.html) ![GPLv3](http://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2017 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.



![PFSP](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2019/Oct/30/2590784777-6-permutation-flow-shop-scheduling-prob_avatar.png)
