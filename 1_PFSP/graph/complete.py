#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
complete.py

From '../stats_*/*.txt' data files and '../instances/bestSolutions.txt'
build files:
  '../stats/*.txt'
  '../stats/*_freq_cumul.txt'
  '../stats/*_time_cumul.txt'
  '../stats/average.tex'
  '../stats/average_time.tex'

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import glob
import os.path


AVERAGE_DEVIATION = dict()
AVERAGE_TIME = dict()


#############
# Functions #
#############
def add_average(data_name, datas):
    deviation = 0
    time = 0
    for _, values in sorted(datas.items()):
        deviation += values[3]
        time += values[0]

    deviation = float(deviation)/len(datas)
    time = float(time)/len(datas)

    AVERAGE_DEVIATION[data_name] = deviation
    AVERAGE_TIME[data_name] = time


def complete_datas(datas, bests):
    for name, values in datas.items():
        wct = values[1]
        best = bests[name]
        values.append(best)
        values.append(percentage_deviation(wct, best))


def percentage_deviation(wct, best):
    assert wct >= best, (wct, best)

    return float((wct - best)*100)/best


def read_bests(filename):
    datas = dict()

    with open(filename) as fin:
        assert fin.readline() == 'Problem , BS\n'

        for line in fin:
            columns = line.strip().split(' , ')
            datas[columns[0]] = to_number(columns[1])

    return datas


def read_datas(filename):
    datas = dict()

    with open(filename) as fin:
        assert fin.readline() == 'filename\ttime\twsct\n'

        for line in fin:
            columns = line.split()
            datas[columns[0]] = [to_number(value) for value in columns[1:]]

    return datas


def to_number(s):
    n = (int(s) if s.isdigit()
         else float(s))

    if int(n) == n:
        n = int(n)

    return n


def write_average():
    def pretty(s):
        print(s)
        if s[:4] == 'srz-':
            init = 'Simplified RZ'
            s = s[4:]
        else:
            init = 'Random'
            s = s[5:]

        if s[-6:] == '-first':
            pivoting = 'first'
            s = s[:-6]
        else:
            pivoting = 'best'
            s = s[:-5]

        neighboorod = s.replace('vnd', 'VND')
            
        return '{} & {} & {}'.format(init, pivoting, neighboorod)
    
    with open('../stats/average.tex', 'w') as fout:
        for data_name, average in sorted(AVERAGE_DEVIATION.items(),
                                         key=lambda n_avg: n_avg[1]):
            print('{} & {:.3f} & {:.3f}\\\\'.format(pretty(data_name),
                                                    average,
                                                    AVERAGE_TIME[data_name]),
                  file=fout)

    with open('../stats/average_time.tex', 'w') as fout:
        for data_name, average in sorted(AVERAGE_TIME.items(),
                                         key=lambda n_avg: n_avg[1]):
            print('{} & {:.3f} & {:.3f}\\\\'.format(pretty(data_name),
                                                    AVERAGE_DEVIATION[data_name],
                                                    average),
                  file=fout)


def write_freq_cumul(datas, filename):
    with open(filename, 'w') as fout:
        scale = 1
        for i in range(100*scale + 1):
            nb = 0
            for values in datas.values():
                if values[3]*scale <= i:
                    nb += 1

            print('{}\t{}'.format(float(i)/scale, nb/len(datas)), file=fout)


def write_time_cumul(datas, filename):
    with open(filename, 'w') as fout:
        scale = 10
        for i in range(int(2.5*scale) + 1):
            nb = 0
            for values in datas.values():
                if values[0]*scale <= i:
                    nb += 1

            print('{}\t{}'.format(float(i)/scale, nb/len(datas)), file=fout)


def write_datas(datas, filename):
    with open(filename, 'w') as fout:
        print('filename\ttime\twsct\tbest\tdeviation', file=fout)
        for name, values in sorted(datas.items()):
            print('{}\t{}'.format(name, '\t'.join(str(value)
                                                  for value in values)),
                  file=fout)


########
# Main #
########
def main():
    for filename in glob.glob('../stats_*/*.txt'):
        basename = os.path.basename(filename).split('.')[0]

        bests = read_bests('../instances/bestSolutions.txt')

        datas = read_datas(filename)
        complete_datas(datas, bests)
        write_datas(datas, '../stats/{}.txt'.format(basename))
        write_freq_cumul(datas, '../stats/{}_freq_cumul.txt'.format(basename))
        write_time_cumul(datas, '../stats/{}_time_cumul.txt'.format(basename))

        assert basename[:6] == 'stats-', basename

        add_average(basename[6:], datas)

    write_average()

if __name__ == '__main__':
    main()
