# Permutation Flow Shop Scheduling Problem (PFSP)

  - `make ndebug all`: build all program in **production mode** (before that, made `make distclean`)
  - `make`: build all programs in **development mode**
  - `make tests`: build and run *partial* unit tests (require CxxTest)

  - `pfsp_stats`: program to produce stats on one instance (see `pfsp_stats --help`)
  - `pfsp_solver`: program to solve one instance (see `pfsp_solver --help`)

  - `build_stats_1_all.sh`: rebuild stats for exercise 1.1
  - `build_stats_2_all.sh`: rebuild stats for exercise 1.2

Tested with *g++ (Debian 4.9.2-10) 4.9.2* on *Debian GNU/Linux 8 (jessie) 64 bits*
