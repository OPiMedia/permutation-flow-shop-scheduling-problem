/* -*- coding: latin-1 -*- */

/** \file test__pfsp.hpp (April 2nd, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include "../pfsp/pfsp.hpp"

#include <iostream>


using namespace pfsp;


#define NOT_COVERED {                           \
    std::cout << "NOT COVERED!" << std::endl;   \
    std::cout.flush();                          \
  }


#define SHOW_FUNC_NAME {                                        \
    std::cout << std::endl                                      \
              << "=== " << __func__ << " ===" << std::endl;     \
    std::cout.flush();                                          \
  }



Pfsp ex_5_3;



class Test__common : public CxxTest::TestSuite {
public:
  void test__load_file() {
    SHOW_FUNC_NAME;

    TS_ASSERT(ex_5_3.load_file("../instances/5_3")
              || ex_5_3.load_file("../../instances/5_3"));

    TS_ASSERT_EQUALS(ex_5_3.nb_job(), 5);
    TS_ASSERT_EQUALS(ex_5_3.nb_machine(), 3);
  }



  void test__calculate_completion_times_by_job() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__iterative_best_improvement_exchange() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__iterative_best_improvement_insert() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__iterative_best_improvement_transpose() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__iterative_first_improvement_exchange() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__iterative_first_improvement_insert() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__makespan() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__nb_job() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__nb_machine() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__permutation_init() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__permutation_simplified_rz() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__println_infos() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__println_weights() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__println_table() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__processing_time() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__recalculate_completion_times_table() {
    SHOW_FUNC_NAME;

    permutation_type permutation(ex_5_3.nb_job() + 1);

    permutation_set_identity(permutation);

    completion_times_table_type ctable_ex_5_3;
    completion_times_table_type ctable1;
    completion_times_table_type ctable2;

    completion_times_init(ctable_ex_5_3, ex_5_3.nb_job(), ex_5_3.nb_machine());
    ex_5_3.calculate_completion_times_table(permutation, ctable_ex_5_3);

    completion_times_init(ctable1, ex_5_3.nb_job(), ex_5_3.nb_machine());
    ex_5_3.calculate_completion_times_table(permutation, ctable1);

    completion_times_init(ctable2, ex_5_3.nb_job(), ex_5_3.nb_machine());
    ex_5_3.calculate_completion_times_table(permutation, ctable2);

    TS_ASSERT_EQUALS(ctable_ex_5_3, ctable1);
    TS_ASSERT_EQUALS(ctable1, ctable2);

    for (unsigned int i = 1; i < ex_5_3.nb_job(); ++i) {
      for (unsigned int k = 1; k <= i; ++k) {
        permutation_transpose(permutation.begin() + i);

        ex_5_3.calculate_completion_times_table(permutation, ctable1);

        ex_5_3.recalculate_completion_times_table(permutation,
                                                  permutation.begin() + k,
                                                  ctable2);

        TS_ASSERT_EQUALS(ctable1, ctable2);

        permutation_transpose(permutation.begin() + i);

        ex_5_3.recalculate_completion_times_table(permutation,
                                                  permutation.begin() + k,
                                                  ctable2);

        TS_ASSERT_EQUALS(ctable2, ctable_ex_5_3);
      }
    }
  }


  void test__sort_by_weighted_sum_processing_time() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__sum_completion_times() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__weighted_sum_completion_times__1() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__weighted_sum_completion_times__2() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__weighted_sum_processing_time() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__weighted_sums_processing_time() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }



  void test__friend_operator_out() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }

};
