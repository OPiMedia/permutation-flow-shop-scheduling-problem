/* -*- coding: latin-1 -*- */

/** \file pfsp_solver.cpp (April 4, 2017)
 * \brief Flow Shop Scheduling Problem (PFSP) solver
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <chrono>
#include <iomanip>
#include <iostream>

#include "pfsp/pfsp.hpp"
#include "pfsp/permutation.hpp"



/************
 * Function *
 ************/
void
help(int error_code = 0) {
  std::cerr << "Usage: pfsp_solver [options] <instance_file>" << std::endl
            << "Options:" << std::endl
            << "  --random-seed n (0 by default)" << std::endl
            << std::endl
            << "  --init-identity (by default)" << std::endl
            << "  --init-ridentity" << std::endl
            << "  --init-random" << std::endl
            << "  --init-sorted" << std::endl
            << "  --init-srz" << std::endl
            << std::endl
            << "  --transpose (by default)" << std::endl
            << "  --exchange" << std::endl
            << "  --insert" << std::endl
            << "  --vnd-transpose-exchange-insert (only with --first)"
            << std::endl
            << "  --vnd-transpose-insert-exchange (only with --first)"
            << std::endl
            << std::endl
            << "  --first (by default)" << std::endl
            << "  --best" << std::endl
            << std::endl
            << "  --display-precision n (2 by default)" << std::endl;

  exit(error_code);
}



/********
 * Main *
 ********/
int
main(int argc, char *argv[]) {
  // Read parameters
  if (argc == 1) {
    help();
  }

  std::string filename;

  unsigned int display_precision = 2;

  pfsp::Init_type init = pfsp::INIT_IDENTITY;

  pfsp::Neighborhood_type neighborhood = pfsp::NEIGHBORHOOD_TRANSPOSE;

  pfsp::Pivoting_type pivoting = pfsp::PIVOTING_FIRST;

  unsigned int seed = 0;


  for (int i = 1; i < argc; ++i) {
    if ((strlen(argv[i]) > 0) && (argv[i][0] == '-')) {
      const std::string param = argv[i];

      if (param == "--best") {
        pivoting = pfsp::PIVOTING_BEST;
      }
      else if (param == "--display-precision") {  // --display-precision n
        ++i;
        if (i >= argc) {
          std::cerr << "--display-precision number missing!" << std::endl;

          help(1);
        }

        display_precision = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--exchange") {
        neighborhood = pfsp::NEIGHBORHOOD_EXCHANGE;
      }
      else if (param == "--first") {
        pivoting = pfsp::PIVOTING_FIRST;
      }
      else if (param == "--help") {
        help();
      }
      else if (param == "--init-identity") {
        init = pfsp::INIT_IDENTITY;
      }
      else if (param == "--init-random") {
        init = pfsp::INIT_RANDOM;
      }
      else if (param == "--init-ridentity") {
        init = pfsp::INIT_REVERSE_IDENTITY;
      }
      else if (param == "--init-sorted") {
        init = pfsp::INIT_SORTED;
      }
      else if (param == "--init-srz") {
        init = pfsp::INIT_SRZ;
      }
      else if (param == "--insert") {
        neighborhood = pfsp::NEIGHBORHOOD_INSERT;
      }
      else if (param == "--random-seed") {  // --random-seed n
        ++i;
        if (i >= argc) {
          std::cerr << "--random-seed number missing!" << std::endl;

          help(1);
        }

        seed = std::stoul("0" + std::string(argv[i]));
      }
      else if (param == "--transpose") {
        neighborhood = pfsp::NEIGHBORHOOD_TRANSPOSE;
      }
      else if (param == "--vnd-transpose-exchange-insert") {
        neighborhood = pfsp::NEIGHBORHOOD_VND_TRANSPOSE_EXCHANGE_INSERT;
      }
      else if (param == "--vnd-transpose-insert-exchange") {
        neighborhood = pfsp::NEIGHBORHOOD_VND_TRANSPOSE_INSERT_EXCHANGE;
      }
      else {
        std::cerr << "Param \"" << param << "\" unknown!" << std::endl;

        help(1);
      }
    }
    else {
      filename = argv[i];
    }
  }

  if (filename == "") {
    std::cerr << "Filename missing!" << std::endl;

    help(1);
  }

  std::cerr
    << "=== Config ===" << std::endl
    << "Random seed: " << (seed == 0
                           ? "time(0)"
                           : std::to_string(seed)) << std::endl
    << "Initial permutation: " << pfsp::init_str[init] << std::endl
    << "Neighborhood: " << pfsp::neighborhood_str[neighborhood] << std::endl
    << "Pivoting rule: " << pfsp::pivoting_str[pivoting] << std::endl
    << "Display precision: " << display_precision << std::endl;
  std::cerr.flush();


  // Init and read instance
  pfsp::Pfsp pfsp;

  if (!pfsp.load_file(filename)) {
    std::cerr << "Opening of file \"" << filename << "\" failed!" << std::endl;

    return 1;
  }

  std::cerr << "=== Data ===" << std::endl
            << "File \"" << filename << "\" readed" << std::endl;
  pfsp.println_infos(std::cerr);
  std::cerr.flush();


  // Init random generator
  srand(seed == 0
        ? time(0)
        : seed);


  std::chrono::steady_clock::time_point start(std::chrono::steady_clock::now());

  // Init permutation
  pfsp::permutation_type permutation;

  pfsp.permutation_init(permutation, init);

  double duration
    = std::chrono::duration<double>(std::chrono::steady_clock::now()
                                    - start).count();

  // Result of initial permutation
  {
    pfsp::completion_times_table_type ctable;

    pfsp::completion_times_init(ctable, pfsp.nb_job(), pfsp.nb_machine());
    pfsp.calculate_completion_times_table(permutation, ctable);

    const pfsp::time_type makespan = pfsp.makespan(permutation, ctable);
    const pfsp::time_type sum_completion_times
      = pfsp.sum_completion_times(ctable);
    const pfsp::time_type weighted_sum_completion_times
      = pfsp.weighted_sum_completion_times(ctable);

    std::cerr << "=== Initial results ===" << std::endl
              << "Initial permutation:" << std::endl;
    pfsp::permutation_println(permutation, std::cerr);
    std::cerr << "Makespan: " << makespan << std::endl
              << "Sum of completion times: "
              << sum_completion_times << std::endl
              << "Weighted sum of completion times: "
              << weighted_sum_completion_times << std::endl;
    std::cerr.flush();
  }


  // Solve
  std::cerr << "=== Solve... ===" << std::endl;
  std::cerr.flush();

#ifndef NDEBUG
  std::pair<unsigned int, unsigned int> nb_loop_improvement;
#endif

  start = std::chrono::steady_clock::now();

  if (pivoting == pfsp::PIVOTING_FIRST) {
    if (neighborhood == pfsp::NEIGHBORHOOD_TRANSPOSE) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_first_improvement_transpose(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_EXCHANGE) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_first_improvement_exchange(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_INSERT) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_first_improvement_insert(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_VND_TRANSPOSE_EXCHANGE_INSERT) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_vnd_first_improvement_tranpose_exchange_insert
        (permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_VND_TRANSPOSE_INSERT_EXCHANGE) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_vnd_first_improvement_tranpose_insert_exchange
        (permutation);
    }
    else {
      assert(false);
    }
  }
  else {
    assert(pivoting == pfsp::PIVOTING_BEST);

    if (neighborhood == pfsp::NEIGHBORHOOD_TRANSPOSE) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_best_improvement_transpose(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_EXCHANGE) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_best_improvement_exchange(permutation);
    }
    else if (neighborhood == pfsp::NEIGHBORHOOD_INSERT) {
#ifndef NDEBUG
      nb_loop_improvement =
#endif
        pfsp.iterative_best_improvement_insert(permutation);
    }
    else {
      assert(false);
    }
  }

  duration
    += std::chrono::duration<double>(std::chrono::steady_clock::now()
                                     - start).count();


  // Results
  pfsp::completion_times_table_type ctable;

  pfsp::completion_times_init(ctable, pfsp.nb_job(), pfsp.nb_machine());
  pfsp.calculate_completion_times_table(permutation, ctable);

  const pfsp::time_type makespan = pfsp.makespan(permutation, ctable);
  const pfsp::time_type sum_completion_times
    = pfsp.sum_completion_times(ctable);
  const pfsp::time_type weighted_sum_completion_times
    = pfsp.weighted_sum_completion_times(ctable);

  std::cerr
    << std::fixed << std::setprecision(display_precision)
    << "=== Results === (calculated in " << duration << "s)" << std::endl
#ifndef NDEBUG
    << "# loops: " << nb_loop_improvement.first << std::endl
    << "# improvement steps: " << nb_loop_improvement.second << std::endl
#endif
    << "Makespan: " << makespan << std::endl
    << "Sum of completion times: "
    << sum_completion_times << std::endl
    << "Weighted sum of completion times: "
    << weighted_sum_completion_times << std::endl
    << "Final permutation:" << std::endl;
  pfsp::permutation_println(permutation, std::cerr);

  std::cerr << "=== end ===" << std::endl;
  std::cerr.flush();

  std::cout
    << std::fixed << std::setprecision(display_precision)
    << duration
#ifndef NDEBUG
    << ' ' << nb_loop_improvement.first << ' ' << nb_loop_improvement.second
#endif
    << std::endl
    << makespan << std::endl
    << sum_completion_times << std::endl
    << weighted_sum_completion_times << std::endl;
  pfsp::permutation_println(permutation);

  return EXIT_SUCCESS;
}
