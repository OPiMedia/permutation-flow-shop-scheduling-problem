=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: transpose
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 5
# improvement steps: 7
Makespan: 21
Sum of completion times: 70
Weighted sum of completion times: 146
Final permutation:
3 5 2 4 1
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: exchange
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 2
# improvement steps: 6
Makespan: 21
Sum of completion times: 70
Weighted sum of completion times: 146
Final permutation:
3 5 4 2 1
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: insert
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 3
# improvement steps: 6
Makespan: 21
Sum of completion times: 66
Weighted sum of completion times: 144
Final permutation:
4 3 5 2 1
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: transpose
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 8
# improvement steps: 8
Makespan: 21
Sum of completion times: 70
Weighted sum of completion times: 146
Final permutation:
3 5 2 4 1
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: exchange
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 3
# improvement steps: 7
Makespan: 21
Sum of completion times: 66
Weighted sum of completion times: 144
Final permutation:
5 4 3 2 1
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: insert
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 5
# improvement steps: 7
Makespan: 21
Sum of completion times: 66
Weighted sum of completion times: 144
Final permutation:
5 4 3 2 1
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: VND transpose-exchange-insert
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 5
# improvement steps: 7
Makespan: 21
Sum of completion times: 70
Weighted sum of completion times: 146
Final permutation:
3 5 2 4 1
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: VND transpose-insert-exchange
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/5_3" readed
# jobs: 5   # machines: 3
=== Initial results ===
Initial permutation:
1 2 3 4 5
Makespan: 21
Sum of completion times: 73
Weighted sum of completion times: 186
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 5
# improvement steps: 7
Makespan: 21
Sum of completion times: 70
Weighted sum of completion times: 146
Final permutation:
3 5 2 4 1
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: transpose
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 6
# improvement steps: 60
Makespan: 4411
Sum of completion times: 143019
Weighted sum of completion times: 790129
Final permutation:
1 3 2 4 7 5 8 10 12 6 11 9 14 13 15 16 18 20 17 22 21 25 19 23 28 24 26 27 30 31 35 34 32 29 33 36 37 43 41 39 38 44 40 47 42 46 45 48 49 50
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: exchange
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 6
# improvement steps: 167
Makespan: 4258
Sum of completion times: 134651
Weighted sum of completion times: 611606
Final permutation:
40 3 43 6 31 35 41 12 30 49 37 19 50 25 4 23 16 47 39 22 27 13 5 8 48 46 36 1 42 14 7 34 15 33 29 24 26 10 38 9 44 20 2 45 28 17 32 21 11 18
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: insert
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 1s)
# loops: 8
# improvement steps: 240
Makespan: 4127
Sum of completion times: 130497
Weighted sum of completion times: 608798
Final permutation:
40 50 25 49 39 12 6 22 41 4 37 19 30 31 3 47 43 23 34 42 24 35 16 5 8 13 26 33 36 1 27 45 48 7 10 14 46 15 29 9 44 28 20 38 11 18 2 17 32 21
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: transpose
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 38
# improvement steps: 71
Makespan: 4529
Sum of completion times: 143052
Weighted sum of completion times: 789520
Final permutation:
1 3 2 4 7 5 8 10 12 6 9 11 13 14 15 16 17 20 18 22 21 25 23 19 24 30 26 27 35 29 31 28 32 34 33 36 37 41 38 39 40 43 42 44 45 46 47 50 48 49
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: exchange
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 1s)
# loops: 32
# improvement steps: 107
Makespan: 4250
Sum of completion times: 131454
Weighted sum of completion times: 623827
Final permutation:
40 1 3 43 16 6 7 37 30 49 24 12 39 31 15 5 47 35 19 4 23 22 50 25 13 8 27 41 29 9 34 42 33 26 48 36 14 46 20 45 28 2 44 32 10 38 17 11 18 21
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: insert
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 5s)
# loops: 58
# improvement steps: 403
Makespan: 4109
Sum of completion times: 129511
Weighted sum of completion times: 609184
Final permutation:
40 50 24 1 3 43 35 41 12 5 6 31 49 37 30 8 25 13 4 10 47 39 19 22 23 46 16 7 34 42 14 27 44 36 29 48 26 20 45 15 38 33 28 9 18 2 17 32 21 11
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: VND transpose-exchange-insert
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 17
# improvement steps: 215
Makespan: 4210
Sum of completion times: 133157
Weighted sum of completion times: 610148
Final permutation:
40 50 6 30 43 35 37 31 3 49 12 39 19 22 41 5 47 25 23 46 27 4 16 7 34 1 8 13 42 26 48 36 14 20 33 9 44 28 10 32 45 29 38 24 15 11 18 2 17 21
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: VND transpose-insert-exchange
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/50_20_01" readed
# jobs: 50   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
Makespan: 4672
Sum of completion times: 153680
Weighted sum of completion times: 855306
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 17
# improvement steps: 215
Makespan: 4210
Sum of completion times: 133157
Weighted sum of completion times: 610148
Final permutation:
40 50 6 30 43 35 37 31 3 49 12 39 19 22 41 5 47 25 23 46 27 4 16 7 34 1 8 13 42 26 48 36 14 20 33 9 44 28 10 32 45 29 38 24 15 11 18 2 17 21
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: transpose
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 9
# improvement steps: 139
Makespan: 7509
Sum of completion times: 444353
Weighted sum of completion times: 2422968
Final permutation:
1 3 2 4 7 5 8 9 11 12 10 6 14 15 13 16 18 20 17 22 21 25 19 23 28 24 26 27 29 30 31 32 33 35 38 37 34 43 39 42 44 36 41 45 49 47 46 40 51 50 48 52 55 54 53 57 58 56 60 59 61 63 64 65 62 66 67 70 68 71 69 72 76 75 78 79 73 80 77 82 74 83 84 86 87 81 89 91 88 94 92 97 90 96 100 93 99 98 95 85
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: exchange
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 1s)
# loops: 4
# improvement steps: 416
Makespan: 7207
Sum of completion times: 416216
Weighted sum of completion times: 1857648
Final permutation:
53 5 63 32 82 75 89 99 9 54 94 39 55 36 34 43 7 100 19 31 73 30 79 44 77 84 83 42 10 59 87 16 24 92 62 57 8 25 20 91 46 78 12 15 47 22 23 4 28 65 1 90 64 3 35 6 11 17 70 88 86 33 58 37 26 41 60 96 80 49 68 98 18 66 50 14 95 27 38 97 21 48 52 29 76 93 2 85 45 72 67 69 13 56 51 40 81 61 74 71
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: insert
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 6s)
# loops: 9
# improvement steps: 553
Makespan: 7186
Sum of completion times: 407855
Weighted sum of completion times: 1862105
Final permutation:
53 24 5 15 42 10 58 73 88 16 99 43 91 75 89 100 30 94 47 77 39 63 87 19 22 78 84 83 79 1 57 65 28 6 34 55 44 66 32 82 11 59 9 54 46 37 36 31 86 70 23 17 62 3 7 64 20 90 12 35 92 4 68 8 25 27 41 33 21 49 60 18 14 48 96 80 50 26 98 95 85 93 97 76 52 45 29 38 40 69 61 56 13 72 2 81 67 74 51 71
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: transpose
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 0s)
# loops: 87
# improvement steps: 239
Makespan: 7575
Sum of completion times: 447825
Weighted sum of completion times: 2448334
Final permutation:
1 3 2 4 7 5 6 8 9 10 12 11 14 15 16 17 21 13 20 18 19 22 23 24 26 27 25 28 29 30 31 32 33 35 37 34 38 39 36 41 43 44 40 42 46 49 45 47 51 50 48 53 52 55 57 54 61 56 60 58 59 63 64 62 65 66 68 67 71 70 69 72 75 73 76 79 74 77 78 82 80 81 83 86 84 87 85 88 91 90 89 94 92 100 96 99 93 98 97 95
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: exchange
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 28s)
# loops: 80
# improvement steps: 335
Makespan: 7280
Sum of completion times: 413699
Weighted sum of completion times: 1874627
Final permutation:
5 9 53 75 24 73 7 12 55 10 91 54 94 31 15 16 89 1 19 39 99 57 23 30 87 25 79 28 77 63 3 32 33 49 37 78 34 83 59 43 84 44 88 42 62 46 17 35 82 11 92 100 47 8 6 4 22 58 96 60 20 36 41 64 65 66 86 68 98 70 90 50 18 48 26 27 95 97 21 80 14 52 38 81 85 45 76 13 67 29 72 69 61 93 56 2 51 40 71 74
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: insert
Pivoting rule: best-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 69s)
# loops: 99
# improvement steps: 712
Makespan: 7151
Sum of completion times: 405828
Weighted sum of completion times: 1846939
Final permutation:
53 5 24 1 75 73 39 7 31 3 30 99 43 9 54 11 89 4 28 78 10 12 91 87 15 83 16 17 63 82 8 19 22 20 25 79 94 59 77 32 65 34 46 42 86 55 84 35 92 57 33 36 37 62 88 44 47 100 64 58 70 41 68 49 6 60 90 26 48 96 27 80 23 14 52 18 50 98 97 21 66 95 38 85 29 76 67 69 45 56 72 93 61 13 2 51 40 81 71 74
=== end ===
============================================================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: VND transpose-exchange-insert
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 2s)
# loops: 20
# improvement steps: 530
Makespan: 7202
Sum of completion times: 419410
Weighted sum of completion times: 1886298
Final permutation:
63 78 73 39 53 7 5 75 34 89 94 55 54 47 79 43 16 15 87 32 10 42 30 9 44 77 19 62 24 82 59 65 31 83 58 22 84 99 88 25 3 28 12 35 92 1 57 23 11 46 36 8 4 20 91 17 100 33 70 68 6 86 66 64 90 41 96 49 60 37 27 98 95 80 26 18 50 48 21 14 76 97 52 13 74 29 85 40 81 38 61 93 67 45 56 72 2 51 71 69
=== end ===
==============================
=== Config ===
Random seed: time(0)
Initial permutation: identity
Neighborhood: VND transpose-insert-exchange
Pivoting rule: first-improvment
Display precision: 0
=== Data ===
File "../instances/100_20_01" readed
# jobs: 100   # machines: 20
=== Initial results ===
Initial permutation:
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
Makespan: 7968
Sum of completion times: 475498
Weighted sum of completion times: 2609094
=== Solve... ===
=== Results === (calculated in 2s)
# loops: 20
# improvement steps: 530
Makespan: 7202
Sum of completion times: 419410
Weighted sum of completion times: 1886298
Final permutation:
63 78 73 39 53 7 5 75 34 89 94 55 54 47 79 43 16 15 87 32 10 42 30 9 44 77 19 62 24 82 59 65 31 83 58 22 84 99 88 25 3 28 12 35 92 1 57 23 11 46 36 8 4 20 91 17 100 33 70 68 6 86 66 64 90 41 96 49 60 37 27 98 95 80 26 18 50 48 21 14 76 97 52 13 74 29 85 40 81 38 61 93 67 45 56 72 2 51 71 69
=== end ===
============================================================
