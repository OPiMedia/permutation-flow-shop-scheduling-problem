/* -*- coding: latin-1 -*- */

/** \file pfsp/permutation.cpp (April 2nd, 2017)
 * \brief
 * Types and operations for permutations of \f$\{i\}_{1\leq i\leq N}\f$.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "permutation.hpp"


namespace pfsp {

  /*************
   * Functions *
   *************/
  void
  permutation_print(const permutation_type &permutation,
                    std::ostream& out) {
    assert(permutation.size() > 1);

    out << *(permutation.cbegin() + 1);
    for (auto it = permutation.cbegin() + 2; it < permutation.cend(); ++it) {
      out << ' ' << *it;
    }
  }


  void
  permutation_println(const permutation_type &permutation,
                      std::ostream& out) {
    assert(permutation.size() > 1);

    permutation_print(permutation, out);
    out << std::endl;
  }


  void
  permutation_set_identity(permutation_type &permutation) {
    assert(permutation.size() > 1);

    for (unsigned int i = 0; i < permutation.size(); ++i) {
      permutation[i] = i;
    }
  }


  void
  permutation_set_random(permutation_type &permutation) {
    assert(permutation.size() > 1);

    permutation_set_identity(permutation);
    std::random_shuffle(permutation.begin() + 1, permutation.end());
  }

}  // namespace pfsp
