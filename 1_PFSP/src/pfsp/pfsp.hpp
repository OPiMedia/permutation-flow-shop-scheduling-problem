/* -*- coding: latin-1 -*- */

/** \file pfsp/pfsp.hpp (April 4, 2017)
 * \brief Class for Flow Shop Scheduling Problem (PFSP)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef SRC_PFSP_PFSP_HPP_
#define SRC_PFSP_PFSP_HPP_

#include <cstdint>

#include <algorithm>
#include <limits>
#include <string>
#include <utility>
#include <vector>

#include "permutation.hpp"
#include "times.hpp"


namespace pfsp {

  /* ******
   * Type *
   ********/
  /** \brief
   * Number type for weight data: \f$w_i\f$ (one by job).
   */
  typedef uint32_t weights_type;



  /* *******
   * Enums *
   *********/
  /** \brief
   * Constants for possibilities to build initial solution.
   */
  typedef enum Init_type {
    INIT_IDENTITY = 0,
    INIT_REVERSE_IDENTITY = 1,
    INIT_RANDOM = 2,
    INIT_SORTED = 3,
    INIT_SRZ = 4
  } Init_type;


  /** \brief
   * Constants for possibilities of neighborhoods.
   */
  typedef enum Neighborhood_type {
    NEIGHBORHOOD_TRANSPOSE = 0,
    NEIGHBORHOOD_EXCHANGE = 1,
    NEIGHBORHOOD_INSERT = 2,
    NEIGHBORHOOD_VND_TRANSPOSE_EXCHANGE_INSERT = 3,
    NEIGHBORHOOD_VND_TRANSPOSE_INSERT_EXCHANGE = 4
  } Neighborhood_type;


  /** \brief
   * Constants for possibilities of pivoting rules.
   */
  typedef enum Pivoting_type {
    PIVOTING_FIRST = 0,
    PIVOTING_BEST = 1
  } Pivoting_type;



  /* ******************
   * Global constants *
   ********************/
  /** \brief
   * Constant names for possibilities to build initial solution.
   */
  extern const std::string init_str[5];


  /** \brief
   * Constant names for possibilities of neighborhoods.
   */
  extern const std::string neighborhood_str[5];


  /** \brief
   * Constant names for possibilities of pivoting rules.
   */
  extern const std::string pivoting_str[2];



  /* *******
   * Class *
   *********/
  class Pfsp {
   public:
    /** \brief
     * Set completion times table for all jobs
     * for the permutation.
     *
     * @param permutation
     * @param completion_times_table
     */
    void
    calculate_completion_times_table
    (const permutation_type &permutation,
     completion_times_table_type &completion_times_table) const;


    /** \brief
     * Iterative improvement with
     *   * best-improvement pivoting rule
     *   * exchange neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return 0 and 0.
     *
     * @param permutation valid initial solution
     */
    std::pair<unsigned int, unsigned int>
    iterative_best_improvement_exchange(permutation_type &permutation) const;


    /** \brief
     * Iterative improvement with
     *   * best-improvement pivoting rule
     *   * insert neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return 0 and 0.
     *
     * @param permutation valid initial solution
     */
    std::pair<unsigned int, unsigned int>
    iterative_best_improvement_insert(permutation_type &permutation) const;


    /** \brief
     * Iterative improvement with
     *   * best-improvement pivoting rule
     *   * transpose neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return 0 and 0.
     *
     * @param permutation valid initial solution
     */
    std::pair<unsigned int, unsigned int>
    iterative_best_improvement_transpose(permutation_type &permutation) const;


    /** \brief
     * Iterative improvement with
     *   * first-improvement pivoting rule
     *   * exchange neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return number of outside loop and 0.
     *
     * @param permutation valid initial solution
     * @param max_nb_loop
     */
    std::pair<unsigned int, unsigned int>
    iterative_first_improvement_exchange
    (permutation_type &permutation,
     unsigned int max_nb_loop = std::numeric_limits<unsigned int>::max()) const;


    /** \brief
     * Iterative improvement with
     *   * first-improvement pivoting rule
     *   * insert neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return number of outside loop and 0.
     *
     * @param permutation valid initial solution
     * @param max_nb_loop
     */
    std::pair<unsigned int, unsigned int>
    iterative_first_improvement_insert
    (permutation_type &permutation,
     unsigned int max_nb_loop = std::numeric_limits<unsigned int>::max()) const;


    /** \brief
     * Iterative improvement with
     *   * first-improvement pivoting rule
     *   * transpose neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return number of outside loop and 0.
     *
     * @param permutation valid initial solution
     * @param max_nb_loop
     *
     * @return pair<number of outside loop, number of improvements>
     */
    std::pair<unsigned int, unsigned int>
    iterative_first_improvement_transpose
    (permutation_type &permutation,
     unsigned int max_nb_loop = std::numeric_limits<unsigned int>::max()) const;


    /** \brief
     * Iterative improvement
     *   * first-improvement pivoting rule
     *   * VND (variable neighborhood descent) neighborhood:
     *     - transpose neighborhood
     *     - exchange neighborhood
     *     - insert neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return 0 and 0.
     *
     * @param permutation valid initial solution
     */
    std::pair<unsigned int, unsigned int>
    iterative_vnd_first_improvement_tranpose_exchange_insert
    (permutation_type &permutation) const;


    /** \brief
     * Iterative improvement
     *   * first-improvement pivoting rule
     *   * VND (variable neighborhood descent) neighborhood:
     *     - transpose neighborhood
     *     - insert neighborhood
     *     - exchange neighborhood
     *
     * Set permutation with best solution found.
     *
     * If compiled in debug mode
     * then return number of outside loop and number of improvements,
     * else return 0 and 0.
     *
     * @param permutation valid initial solution
     */
    std::pair<unsigned int, unsigned int>
    iterative_vnd_first_improvement_tranpose_insert_exchange
    (permutation_type &permutation) const;


    /**
     * Load and set data from file filename
     * in this format:
     * n m
     * 1 p_{1, 1} 2 p_{1, 2} 3 p_{1, 3} ... m p_{1, m}
     * 1 p_{2, 1} 2 p_{2, 2} 3 p_{2, 3} ... m p_{2, m}
     * 1 p_{3, 1} 2 p_{3, 2} 3 p_{3, 3} ... m p_{3, m}
     * ...
     * 1 p_{n, 1} 2 p_{n, 2} 3 p_{n, 3} ... m p_{n, m}
     * Reldue
     * -1 d_1 -1 w_1
     * -1 d_2 -1 w_2
     * -1 d_3 -1 w_3
     * ...
     * -1 d_n -1 w_n
     *
     * where n: number of jobs
     *       m: number of machines
     *       p_{i, j}: processing time for job i
     *                                     and operation j (on machine j)
     *       d_i: useless
     *       w_i: weight for job i
     *
     * @return false if the file can't be open, else true
     */
    bool
    load_file(std::string filename);


    /** \brief
     * Return the makespan calculate from a completion times table.
     *
     * @param permutation
     * @param completion_times_table
     *
     * @return makespan
     */
    inline
    time_type
    makespan(const permutation_type &permutation,
             const completion_times_table_type &completion_times_table) const {
      return (completion_times_table.cbegin() + permutation.back())->back();
    }


    /** \brief
     * Return number of jobs.
     */
    inline
    unsigned int
    nb_job() const {
      return nb_job_;
    }


    /** \brief
     * Return number of machines.
     */
    inline
    unsigned int
    nb_machine() const {
      return nb_machine_;
    }


    /** \brief
     * Set permutation corresponding to the method specified by init.
     *
     * @param permutation
     * @param init INIT_IDENTITY, INIT_REVERSE_IDENTITY, INIT_RANDOM,
     *             INIT_SORTED or INIT_SRZ
     */
    void
    permutation_init(permutation_type &permutation,
                     Init_type init = INIT_IDENTITY) const;


    /** \brief
     * Set permutation with result of the simplified RZ heuristic.
     *
     * @param permutation
     */
    void
    permutation_simplified_rz(permutation_type &permutation) const;


    /** \brief
     * Print number of jobs and machines to out.
     *
     * @param out
     */
    void
    println_infos(std::ostream& out) const;


    /** \brief
     * Print processing times table to out.
     *
     * If tranpose
     * then print table transposed.
     *
     * @param out
     * @param transpose
     */
    void
    println_table(std::ostream& out, bool transpose = false) const;


    /** \brief
     * Print processing times table to out,
     * in the order fixed by permutation.
     *
     * @param permutation
     * @param out
     */
    void
    println_table(const permutation_type &permutation, std::ostream& out) const;


    /** \brief
     * Print weights list to out.
     *
     * @param out
     */
    void
    println_weights(std::ostream& out) const;


    /** \brief
     * Print weights list to out,
     * in the order fixed by permutation.
     *
     * @param permutation
     * @param out
     */
    void
    println_weights(const permutation_type &permutation,
                    std::ostream& out) const;


    /** \brief
     * Set completion times table for all jobs
     * for the permutation.
     * Recalculates only from it_pi.
     *
     * @param permutation
     * @param it_pi valid position on permutation
     * @param completion_times_table correct for this permutation until it_pi
     */
    void
    recalculate_completion_times_table
    (const permutation_type &permutation,
     permutation_type::const_iterator it_pi,
     completion_times_table_type &completion_times_table) const;


    /** \brief
     * Set permutation corresponding to sorted jobs
     * by increase weighted sum processing time.
     *
     * @param permutation valid solution
     */
    void
    sort_by_weighted_sum_processing_time(permutation_type &permutation) const {
      std::vector<double> sums(weighted_sums_processing_time());

      std::sort(permutation.begin() + 1, permutation.end(),
                [sums] (const unsigned int i, const unsigned int j) {
                  assert(i > 0);
                  assert(j > 0);

                  return sums[i] < sums[j];
                });
    }


    /** \brief
     * Return the sum of completion times
     * calculate from a completion times table.
     *
     * @param completion_times_table
     *
     * @return sum of completion times
     */
    time_type sum_completion_times
    (const completion_times_table_type &completion_times_table) const {
      return accumulate(completion_times_table.cbegin() + 1,
                        completion_times_table.cend(),
                        static_cast<time_type>(0),
                        [](const time_type sum,
                           const completion_times_job_type ctimes) {
                          return sum + ctimes.back();
                        });
    }


    /** \brief
     * Return the weighted sum of completion times
     * from a completion times table.
     *
     * @param completion_times_table
     *
     * @return weighted sum of completion times
     */
    time_type
    weighted_sum_completion_times
    (const completion_times_table_type &completion_times_table) const;


    /** \brief
     * Return the weighted sum of completion times
     * from a completion times table
     * with respect to the permutation
     *
     * @param completion_times_table
     * @param permutation
     *
     * @return weighted sum of completion times
     */
    time_type
    weighted_sum_completion_times
    (const completion_times_table_type &completion_times_table,
     const permutation_type &permutation) const;


    /** \brief
     * Return weighted sum of processing times for each job.
     *
     * @return vector of weighted sum of processing times
     */
    inline
    double
    weighted_sum_processing_time(unsigned int i) const {
      assert(0 < i);
      assert(i <= nb_job_);

      return (static_cast<double>
              (accumulate(processing_times_[i].cbegin() + 1,
                          processing_times_[i].cend(),
                          static_cast<time_type>(0)))
              / weights_[i]);
    }


    /** \brief
     * Return weighted sum of processing times for each job.
     *
     * @return vector of weighted sum of processing times
     */
    std::vector<double>
    weighted_sums_processing_time() const;



    /** \brief
     * Print all datas to out.
     *
     * @param out
     * @param pfsp
     */
    friend
    std::ostream&
    operator<<(std::ostream& out, const Pfsp &pfsp);



   private:
    /** \brief
     * Set number of jobs and machines
     * and reset all datas.
     *
     * @param nb_job > 0
     * @param nb_machine > 0
     *
     * @warning The position 0 most of vectors are *not* used,
     *          so for k items the size of the vector is k + 1.
     */
    void
    resize_(unsigned int nb_job, unsigned int nb_machine);


    /** \brief
     * Number of jobs.
     */
    unsigned int nb_job_ = 0;


    /** \brief
     * Number of machines.
     */
    unsigned int nb_machine_ = 0;


    /** \brief
     * Processing time for each pair job, machine: \f$p_{ij}\f$.
     */
    std::vector<std::vector<processing_time_type>> processing_times_;


    /** \brief
     * Weight for each job: \f$w_i\f$.
     */
    std::vector<weights_type> weights_;
  };

}  // namespace pfsp

#endif  // SRC_PFSP_PFSP_HPP_



/** \mainpage
 * Flow Shop Scheduling Problem (PFSP)
 *
 * Set of n jobs
 * where each job consists of operations on m machines (in fixed order)
 * with processing time \f$p_{i, j}\f$.
 *
 * For each job a weight \f$w_i\f$.
 *
 * Problem:
 * Find a permutation of jobs
 * that minimize the weighted sum of completion times
 * \f$\sum\limits_{i=1}^n w_i C_i\f$
 * where \f$C_i\f$ is the completion time of each job.
 *
 * Example of data:
 * \verbatim
     | p_{i, 1} | p_{i, 3} | p_{i, 3} || w_i
 ----+----------+----------+----------++----
 J_1 | 3        | 2        | 4        || 1
 J_2 | 3        | 1        | 2        || 2
 J_3 | 4        | 3        | 1        || 4
 J_4 | 2        | 3        | 2        || 2
 J_5 | 3        | 1        | 3        || 3
 \endverbatim
 *
 * Completion times table:
 * \verbatim
          | C_{i, 1} | C_{i, 3} | C_{i, 3} = C_i || w_i C_i
 ---------+----------+----------+----------------++---------
 C_{1, j} |  3       |  5       |  9             ||  9
 C_{2, j} |  6       |  7       | 11             || 22
 C_{3, j} | 10       | 13       | 14             || 56  
 C_{4, j} | 12       | 16       | 18             || 36
 C_{5, j} | 15       | 17       | 21             || 63
 \endverbatim
 *
 * GPLv3 &mdash; Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */
