/* -*- coding: latin-1 -*- */

/** \file pfsp/pfsp.cpp (April 4, 2017)
 * \brief Class for Flow Shop Scheduling Problem (PFSP)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>

#include <fstream>
#include <iostream>

#include "pfsp.hpp"


// To test acceleration with only partial recalculation of table
// #define ALL_RECALCULATE


namespace pfsp {

  /* ******************
   * Global constants *
   ********************/
  const std::string init_str[5] = {"identity", "reverse identity",
                                   "random", "sorted", "simplified RZ"};

  const std::string neighborhood_str[5] = {"transpose", "exchange", "insert",
                                           "VND transpose-exchange-insert",
                                           "VND transpose-insert-exchange"};

  const std::string pivoting_str[2] = {"first-improvment", "best-improvment"};



  /***********
   * Methods *
   ***********/
  void
  Pfsp::calculate_completion_times_table
  (const permutation_type &permutation,
   completion_times_table_type &completion_times_table) const {
    permutation_type::const_iterator it_pi = permutation.cbegin() + 1;  // pi(1)

    // Job 1
    {
      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(1), j}
      auto it = it_job->begin() + 1;                // C_{pi(1), 1}

      auto it_ptime
        = (processing_times_.begin() + *it_pi)->begin() + 1;  // p_{pi(1), 1}

      *it = *it_ptime;  // C_{pi(1), 1} = p_{pi(1), 1}

      // j = 2...
      for (++it_ptime, ++it; it != it_job->end(); ++it_ptime, ++it) {
        // C_{pi(1), j} = C_{pi(1), j-1} + p_{pi(1), j}
        *it = *(it - 1) + *it_ptime;
      }
    }

    // Job 2 to nb_job_, i = 2...
    for (++it_pi; it_pi != permutation.end(); ++it_pi) {
      // C_{pi(i-1), 1}
      auto it_prev
        = (completion_times_table.begin() + *(it_pi - 1))->begin() + 1;

      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(i), j}
      auto it = it_job->begin() + 1;                // C_{pi(i), 1}

      // p_{pi(i), 1}
      auto it_ptime = (processing_times_.begin() + *it_pi)->begin() + 1;

      // C_{pi(i), 1} = C_{pi(i-1), 1} + p_{pi(i), 1}
      *it = *it_prev + *it_ptime;

      // j = 2...
      for (++it_ptime, ++it_prev, ++it; it != it_job->end();
           ++it_ptime, ++it_prev, ++it) {
        // C_{pi(i), j} = max(C_{pi(i-1), j}, C_{pi(i), j-1}) + p_{pi(i), j}
        *it = std::max(*it_prev, *(it - 1)) + *it_ptime;
      }
    }
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_best_improvement_exchange(permutation_type &permutation)
    const {
    unsigned int nb_loop = 0;  // used in debug mode
    unsigned int nb_improvement = 0;  // used in debug mode

    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;

#ifndef NDEBUG
      ++nb_loop;
#endif

      // Get *the* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi1 = last_it_pi;
           it_pi1 + 1 != permutation.end(); last_it_pi = it_pi1, ++it_pi1) {
        for (auto it_pi2 = it_pi1 + 1; it_pi2 != permutation.end(); ++it_pi2) {
          // Try exchange
          permutation_exchange(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (best_wct > trial_wct) {  // better, save it
            best_wct = trial_wct;
            std::copy(permutation.cbegin(), permutation.cend(), best.begin());
#ifndef NDEBUG
            ++nb_improvement;
#endif
            improved = true;
          }

          // Go back to current permutation
          permutation_exchange(it_pi1, it_pi2);
        }
      }

      if (improved) {  // get better
        std::copy(best.cbegin(), best.cend(), permutation.begin());
      }
    } while (improved);

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_best_improvement_insert(permutation_type &permutation)
    const {
    unsigned int nb_loop = 0;  // used in debug mode
    unsigned int nb_improvement = 0;  // used in debug mode

    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;

#ifndef NDEBUG
      ++nb_loop;
#endif

      // Get *the* better permutation
      auto last_it_pi1 = permutation.begin() + 1;  // last needed 1

      for (auto it_pi1 = last_it_pi1;
           it_pi1 != permutation.end(); last_it_pi1 = it_pi1, ++it_pi1) {
        auto last_it_pi2 = permutation.begin() + 1;  // last needed 2

        for (auto it_pi2 = last_it_pi2;
             it_pi2 != permutation.end(); last_it_pi2 = it_pi2, ++it_pi2) {
          if (it_pi1 == it_pi2) {
            continue;
          }

          // Try insert
          permutation_insert(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation,
                                             std::min(last_it_pi1, last_it_pi2),
                                             ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (best_wct > trial_wct) {  // better, save it
            best_wct = trial_wct;
            std::copy(permutation.cbegin(), permutation.cend(), best.begin());
#ifndef NDEBUG
            ++nb_improvement;
#endif
            improved = true;
          }

          // Go back to current permutation
          permutation_insert(it_pi2, it_pi1);
        }
      }

      if (improved) {  // get better
        std::copy(best.cbegin(), best.cend(), permutation.begin());
      }
    } while (improved);

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_best_improvement_transpose(permutation_type &permutation)
    const {
    unsigned int nb_loop = 0;  // used in debug mode
    unsigned int nb_improvement = 0;  // used in debug mode

    permutation_type best(permutation.size());
    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;

#ifndef NDEBUG
      ++nb_loop;
#endif

      // Get *the* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi = last_it_pi;
           it_pi + 1 != permutation.end(); last_it_pi = it_pi, ++it_pi) {
        // Try transposition
        permutation_transpose(it_pi);

#ifdef ALL_RECALCULATE
        calculate_completion_times_table(permutation, ctable);
#else
        recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

        const time_type trial_wct = weighted_sum_completion_times(ctable);

        if (best_wct > trial_wct) {  // better, save it
          best_wct = trial_wct;
          std::copy(permutation.cbegin(), permutation.cend(), best.begin());
#ifndef NDEBUG
          ++nb_improvement;
#endif
          improved = true;
        }

        // Go back to current permutation
        permutation_transpose(it_pi);
      }

      if (improved) {  // get better
        std::copy(best.cbegin(), best.cend(), permutation.begin());
      }
    } while (improved);

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_first_improvement_exchange(permutation_type &permutation,
                                             unsigned int max_nb_loop)
    const {
    unsigned int nb_loop = 0;
    unsigned int nb_improvement = 0;  // used in debug mode

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type current_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;
      ++nb_loop;

      // Get *each* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi1 = last_it_pi;
           it_pi1 + 1 != permutation.end(); last_it_pi = it_pi1, ++it_pi1) {
        for (auto it_pi2 = it_pi1 + 1; it_pi2 != permutation.end(); ++it_pi2) {
          // Try exchange
          permutation_exchange(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (current_wct > trial_wct) {  // better, get it
            current_wct = trial_wct;
#ifndef NDEBUG
            ++nb_improvement;
#endif
            improved = true;
          }
          else {                          // go back to current permutation
            permutation_exchange(it_pi1, it_pi2);
          }
        }
      }
    } while (improved && (nb_loop < max_nb_loop));

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_first_improvement_insert(permutation_type &permutation,
                                           unsigned int max_nb_loop)
    const {
    unsigned int nb_loop = 0;
    unsigned int nb_improvement = 0;  // used in debug mode

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type current_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;
      ++nb_loop;

      // Get *each* better permutation
      auto last_it_pi1 = permutation.begin() + 1;  // last needed 1

      for (auto it_pi1 = last_it_pi1;
           it_pi1 != permutation.end(); last_it_pi1 = it_pi1, ++it_pi1) {
        auto last_it_pi2 = permutation.begin() + 1;  // last needed 2

        for (auto it_pi2 = last_it_pi2; it_pi2 != permutation.end(); ++it_pi2) {
          if (it_pi1 == it_pi2) {
            continue;
          }

          // Try insert
          permutation_insert(it_pi1, it_pi2);

#ifdef ALL_RECALCULATE
          calculate_completion_times_table(permutation, ctable);
#else
          recalculate_completion_times_table(permutation,
                                             std::min(last_it_pi1, last_it_pi2),
                                             ctable);
#endif

          const time_type trial_wct = weighted_sum_completion_times(ctable);

          if (current_wct > trial_wct) {  // better, get it
            current_wct = trial_wct;
            last_it_pi2 = it_pi2 + 1;
#ifndef NDEBUG
            ++nb_improvement;
#endif
            improved = true;
          }
          else {                          // go back to current permutation
            permutation_insert(it_pi2, it_pi1);
            last_it_pi2 = it_pi2;
          }
        }
      }
    } while (improved && (nb_loop < max_nb_loop));

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_first_improvement_transpose(permutation_type &permutation,
                                              unsigned int max_nb_loop)
    const {
    unsigned int nb_loop = 0;
    unsigned int nb_improvement = 0;  // used in debug mode

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type current_wct = weighted_sum_completion_times(ctable);

    bool improved;

    do {
      improved = false;
      ++nb_loop;

      // Get *each* better permutation
      auto last_it_pi = permutation.begin() + 1;  // last needed recalculation

      for (auto it_pi = last_it_pi; it_pi + 1 != permutation.end(); ++it_pi) {
        // Try transposition
        permutation_transpose(it_pi);

#ifdef ALL_RECALCULATE
        calculate_completion_times_table(permutation, ctable);
#else
        recalculate_completion_times_table(permutation, last_it_pi, ctable);
#endif

        const time_type trial_wct = weighted_sum_completion_times(ctable);

        if (current_wct > trial_wct) {  // better, get it
          current_wct = trial_wct;
          last_it_pi = it_pi + 1;
#ifndef NDEBUG
          ++nb_improvement;
#endif
          improved = true;
        }
        else {                          // go back to current permutation
          permutation_transpose(it_pi);
          last_it_pi = it_pi;
        }
      }
    } while (improved && (nb_loop < max_nb_loop));

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_vnd_first_improvement_tranpose_exchange_insert
  (permutation_type &permutation) const {
    /*
      About goto:
      * https://xkcd.com/292/ ;-)
      * Frank Rubin, ""GOTO Considered Harmful" Considered Harmful", ACM 1987:
      https://web.archive.org/web/20090320002214/http://www.ecn.purdue.edu/ParaMount/papers/rubin87goto.pdf
    */

    unsigned int nb_loop = 0;  // used in debug mode
    unsigned int nb_improvement = 0;  // used in debug mode

#ifndef NDEBUG
    std::pair<unsigned int, unsigned int> nb_loop_improvement;
#endif

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

  loop:
#ifndef NDEBUG
    ++nb_loop;
#endif

    // Transpose
#ifndef NDEBUG
    nb_loop_improvement =
#endif
      iterative_first_improvement_transpose(permutation, 1);
#ifndef NDEBUG
    nb_improvement += nb_loop_improvement.second;
#endif

    {
      // If improved then restart with this neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Exchange
#ifndef NDEBUG
    nb_loop_improvement =
#endif
      iterative_first_improvement_exchange(permutation, 1);
#ifndef NDEBUG
    nb_improvement += nb_loop_improvement.second;
#endif

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Insert
#ifndef NDEBUG
    nb_loop_improvement =
#endif
      iterative_first_improvement_exchange(permutation, 1);
#ifndef NDEBUG
    nb_improvement += nb_loop_improvement.second;
#endif

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }

    return std::make_pair(nb_loop, nb_improvement);
  }


  std::pair<unsigned int, unsigned int>
  Pfsp::iterative_vnd_first_improvement_tranpose_insert_exchange
  (permutation_type &permutation) const {
    /*
      About goto:
      * https://xkcd.com/292/ ;-)
      * Frank Rubin, ""GOTO Considered Harmful" Considered Harmful", ACM 1987:
      https://web.archive.org/web/20090320002214/http://www.ecn.purdue.edu/ParaMount/papers/rubin87goto.pdf
    */

    unsigned int nb_loop = 0;  // used in debug mode
    unsigned int nb_improvement = 0;  // used in debug mode

#ifndef NDEBUG
    std::pair<unsigned int, unsigned int> nb_loop_improvement;
#endif

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);
    calculate_completion_times_table(permutation, ctable);

    time_type best_wct = weighted_sum_completion_times(ctable);

  loop:
#ifndef NDEBUG
    ++nb_loop;
#endif

    // Transpose
#ifndef NDEBUG
    nb_loop_improvement =
#endif
      iterative_first_improvement_transpose(permutation, 1);
#ifndef NDEBUG
    nb_improvement += nb_loop_improvement.second;
#endif

    {
      // If improved then restart with this neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Insert
#ifndef NDEBUG
    nb_loop_improvement =
#endif
      iterative_first_improvement_exchange(permutation, 1);
#ifndef NDEBUG
    nb_improvement += nb_loop_improvement.second;
#endif

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }


    // Exchange
#ifndef NDEBUG
    nb_loop_improvement =
#endif
      iterative_first_improvement_exchange(permutation, 1);
#ifndef NDEBUG
    nb_improvement += nb_loop_improvement.second;
#endif

    {
      // If improved then restart with the first neighborhood
      calculate_completion_times_table(permutation, ctable);

      const time_type current_wct = weighted_sum_completion_times(ctable);

      if (best_wct > current_wct) {
        best_wct = current_wct;

        goto loop;
      }
    }

    return std::make_pair(nb_loop, nb_improvement);
  }


  bool
  Pfsp::load_file(std::string filename) {
    std::ifstream fin(filename);

    if (!fin.is_open()) {
      return false;
    }

    fin >> nb_job_;
    fin >> nb_machine_;

    assert(nb_job_ > 0);
    assert(nb_machine_ > 0);

    resize_(nb_job_, nb_machine_);

    for (unsigned int j = 1; j <= nb_job_; ++j) {
      for (unsigned int m = 1; m <= nb_machine_; ++m) {
        unsigned int machine_number;

        fin >> machine_number;  // useless, assumed corresponding to the order

        assert(machine_number == m);

        processing_time_type processing_time;

        fin >> processing_time;

        assert(processing_time > 0);

        processing_times_[j][m] = processing_time;
      }
    }

    std::string str;

    fin >> str;  // this is not read

    assert(str == "Reldue");

    for (unsigned int j = 1; j <= nb_job_; ++j) {
      unsigned int due_date;

      fin >> due_date;  // -1

      assert(due_date == static_cast<unsigned int>(-1));

      fin >> due_date;

      assert(due_date > 0);

      unsigned int weight;

      fin >> weight;  // -1

      assert(weight == static_cast<unsigned int>(-1));

      fin >> weight;

      assert(weight > 0);

      weights_[j] = weight;
    }

    fin.close();

    return true;
  }


  void
  Pfsp::permutation_init(permutation_type &permutation, Init_type init) const {
    permutation.resize(nb_job_ + 1);

    switch (init) {
    case INIT_IDENTITY:
      permutation_set_identity(permutation);

      break;

    case INIT_REVERSE_IDENTITY:
      permutation_set_identity(permutation);
      permutation_reverse(permutation);

      break;

    case INIT_RANDOM:
      permutation_set_random(permutation);

      break;

    case INIT_SORTED:
      permutation_set_identity(permutation);
      sort_by_weighted_sum_processing_time(permutation);

      break;

    case INIT_SRZ:
      permutation_simplified_rz(permutation);

      break;

    default:
      assert(false);
    }
  }


  void
  Pfsp::permutation_simplified_rz(permutation_type &permutation) const {
    permutation_set_identity(permutation);
    sort_by_weighted_sum_processing_time(permutation);

    completion_times_table_type ctable;

    completion_times_init(ctable, nb_job_, nb_machine_);

    permutation_type partial(1 + 1);

    // Step 1
    partial[1] = permutation[1];

    // Step 2 to ...
    for (unsigned int length = 2; length <= nb_job_; ++length) {
      unsigned int best_pos;
      time_type best_wct = std::numeric_limits<time_type>::max();

      for (unsigned int i = 1; i <= length; ++i) {
        partial.insert(partial.begin() + i, permutation[length]);

        calculate_completion_times_table(partial, ctable);

        time_type wct = weighted_sum_completion_times(ctable, partial);

        if (best_wct >= wct)  {
          best_wct = wct;
          best_pos = i;
        }

        partial.erase(partial.begin() + i);
      }

      partial.insert(partial.begin() + best_pos, permutation[length]);
    }

    std::copy(partial.cbegin(), partial.cend(), permutation.begin());
  }


  void
  Pfsp::println_infos(std::ostream& out) const {
    out << "# jobs: " << nb_job_
        << "   # machines: " << nb_machine_ << std::endl;
  }


  void
  Pfsp::println_table(std::ostream& out, bool transpose) const {
    if (transpose) {
      out << "Table (transposed):" << std::endl;
      for (unsigned int j = 1; j <= nb_machine_; ++j) {
        for (unsigned int i = 1; i <= nb_job_; ++i) {
          out << processing_times_[i][j];
          if (i < nb_job_) {
            out << ' ';
          }
        }
        out << std::endl;
      }
    }
    else {
      out << "Table:" << std::endl;
      for (auto it_job = processing_times_.cbegin() + 1;
           it_job < processing_times_.cend(); ++it_job) {
        for (auto it_machine = it_job->cbegin() + 1;
             it_machine < it_job->cend(); ++it_machine) {
          out << *it_machine;
          if (it_machine + 1 < it_job->cend()) {
            out << ' ';
          }
        }
        out << std::endl;
      }
    }
  }


  void
  Pfsp::println_table(const permutation_type &permutation,
                      std::ostream& out) const {
    out << "Table:" << std::endl;
    for (auto it_pi = permutation.cbegin() + 1;
         it_pi < permutation.cend(); ++it_pi) {
      const auto it_job = processing_times_.cbegin() + *it_pi;

      for (auto it_machine = it_job->cbegin() + 1;
           it_machine < it_job->cend(); ++it_machine) {
        out << *it_machine;
        if (it_machine + 1 < it_job->cend()) {
          out << ' ';
        }
      }
      out << std::endl;
    }
  }


  void
  Pfsp::println_weights(std::ostream& out) const {
    out << "Weights:" << std::endl;
    for (auto it = weights_.cbegin() + 1; it < weights_.cend(); ++it) {
      out << *it << ' ';
    }
    out << std::endl;
  }


  void
  Pfsp::println_weights(const permutation_type &permutation,
                        std::ostream& out) const {
    out << "Weights:" << std::endl;
    for (auto it_pi = permutation.cbegin() + 1;
         it_pi < permutation.cend(); ++it_pi) {
      const auto it_w = weights_.cbegin() + *it_pi;

      out << *it_w << ' ';
    }
    out << std::endl;
  }


  void
  Pfsp::recalculate_completion_times_table
  (const permutation_type &permutation, permutation_type::const_iterator it_pi,
   completion_times_table_type &completion_times_table) const {
    assert(it_pi != permutation.cbegin());
    assert(it_pi != permutation.cend());

    // Job 1
    if (it_pi == permutation.cbegin() + 1) {  // pi(1)
      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(1), j}
      auto it = it_job->begin() + 1;                // C_{pi(1), 1}

      auto it_ptime
        = (processing_times_.begin() + *it_pi)->begin() + 1;  // p_{pi(1), 1}

      *it = *it_ptime;  // C_{pi(1), 1} = p_{pi(1), 1}

      // j = 2...
      for (++it_ptime, ++it; it != it_job->end(); ++it_ptime, ++it) {
        // C_{pi(1), j} = C_{pi(1), j-1} + p_{pi(1), j}
        *it = *(it - 1) + *it_ptime;
      }

      ++it_pi;
    }

    // Job 2 to nb_job_, i = 2...
    for ( ; it_pi != permutation.end(); ++it_pi) {
      // C_{pi(i-1), 1}
      auto it_prev
        = (completion_times_table.begin() + *(it_pi - 1))->begin() + 1;

      const auto it_job
        = completion_times_table.begin() + *it_pi;  // line C_{pi(i), j}
      auto it = it_job->begin() + 1;                // C_{pi(i), 1}

      // p_{pi(i), 1}
      auto it_ptime = (processing_times_.begin() + *it_pi)->begin() + 1;

      // C_{pi(i), 1} = C_{pi(i-1), 1} + p_{pi(i), 1}
      *it = *it_prev + *it_ptime;

      // j = 2...
      for (++it_ptime, ++it_prev, ++it; it != it_job->end();
           ++it_ptime, ++it_prev, ++it) {
        // C_{pi(i), j} = max(C_{pi(i-1), j}, C_{pi(i), j-1}) + p_{pi(i), j}
        *it = std::max(*it_prev, *(it - 1)) + *it_ptime;
      }
    }
  }


  time_type
  Pfsp::weighted_sum_completion_times
  (const completion_times_table_type &completion_times_table) const {
    time_type weighted_sum = 0;

    auto it_ctime = completion_times_table.cbegin() + 1;

    for (auto it_w = weights_.cbegin() + 1; it_w != weights_.cend();
         ++it_w, ++it_ctime) {
      weighted_sum += it_ctime->back() * *it_w;
    }

    return weighted_sum;
  }


  time_type
  Pfsp::weighted_sum_completion_times
  (const completion_times_table_type &completion_times_table,
   const permutation_type &permutation) const {
    time_type weighted_sum = 0;

    for (auto it_pi = permutation.cbegin() + 1;
         it_pi != permutation.cend(); ++it_pi) {
      const auto it_ctime = completion_times_table.cbegin() + *it_pi;
      const auto it_w = weights_.cbegin() + *it_pi;

      weighted_sum += it_ctime->back() * *it_w;
    }

    return weighted_sum;
  }


  std::vector<double>
  Pfsp::weighted_sums_processing_time() const {
    std::vector<double> sums(nb_job_ + 1);

    for (unsigned int i = 1; i <= nb_job_; ++i) {
      sums[i] = weighted_sum_processing_time(i);
    }

    return sums;
  }



  /*****************
   * Friend method *
   *****************/
  std::ostream&
  operator<<(std::ostream& out, const Pfsp &pfsp) {
    pfsp.println_infos(out);
    pfsp.println_table(out);
    pfsp.println_weights(out);

    return out;
  }



  /******************
   * Private method *
   ******************/
  void
  Pfsp::resize_(unsigned int nb_job, unsigned int nb_machine) {
    assert(nb_job > 0);
    assert(nb_machine > 0);

    nb_job_ = nb_job;
    nb_machine_ = nb_machine;


    // nb_job lines x nb_machine columns
    processing_times_.resize(nb_job + 1);  // + 1 because position 0 *not* used

    for (auto it_job = processing_times_.begin() + 1;
         it_job < processing_times_.end(); ++it_job) {
      it_job->resize(nb_machine + 1);  // + 1 because position 0 *not* used
    }

    assert(processing_times_[0].size() == 0);


    weights_.resize(nb_job + 1);  // + 1 because position 0 *not* used
  }

}  // namespace pfsp
