/* -*- coding: latin-1 -*- */

/** \file pfsp/permutation.hpp (April 3rd, 2017)
 * \brief
 * Types and operations for permutations of \f$\{i\}_{1\leq i\leq N}\f$.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef SRC_PFSP_PERMUTATION_HPP_
#define SRC_PFSP_PERMUTATION_HPP_

#include <cassert>

#include <algorithm>
#include <iostream>
#include <string>
#include <utility>
#include <vector>


namespace pfsp {

  /* *******
   * Types *
   *********/
  /** \brief
   * Number type for item of a permutation.
   */
  typedef uint32_t permutation_item_type;


  /** \brief
   * Vector of N permutation_item_type.
   *
   * \verbatim
   | 0 |  1  |  2  |  3  | ... |  N  |
   +---+-----+-----+-----+-----+-----+
   | ? | p_1 | p_2 | p_3 | ... | p_N |
   \endverbatim
   * where \f$\{p_i\}_{1\leq i\leq N}\ \f$
   * must be a permutation of \f$\{i\}_{1\leq i\leq N}\f$
   *
   * @warning The position 0 is *not* used,
   *          so for N items the size of the vector is N + 1.
   *
   * @warning The size must always be greater than or equal to 1.
   */
  typedef std::vector<permutation_item_type> permutation_type;



  /* ************
   * Prototypes *
   **************/
  /** \brief
   * Swap two items.
   *
   * \verbatim
   | 0 | ... |    i    | ... |  j  | ...
   +---+-----+---------+-----+-----+-----
   | ? |     |   p_i   |     | p_j |
   \endverbatim
   * becomes
   * \verbatim
   | 0 | ... |    i    | ... |  j  | ...
   +---+-----+---------+-----+-----+-----
   | ? |     |   p_j   |     | p_i |
   \endverbatim
   *
   * @param it_i iterator on one valid position i in {1, ..., j - 1}
   * @param it_j iterator on one valid position j in {i + 1, ..., N}
   */
  inline
  void
  permutation_exchange(permutation_type::iterator it_i,
                       permutation_type::iterator it_j) {
    assert(it_i < it_j);

    std::swap(*it_i, *it_j);
  }


  /** \brief
   * Remove the item from the index i
   * and insert it to index j.
   *
   * If i < j
   * \verbatim
   | 0 | ... |    i    |   i+1   | ... |   j-1   |    j    | ...
   +---+-----+---------+---------+-----+---------+---------+-----
   | ? |     |   p_i   | p_{i+1} |     | p_{j-1} |   p_j   |
   \endverbatim
   * becomes
   * \verbatim
   | 0 | ... |    i    |   i+1   | ... |   j-1   |    j    | ...
   +---+-----+---------+---------+-----+---------+---------+-----
   | ? |     | p_{i+1} | p_{i+2} |     |   p_j   |   p_i   |
   \endverbatim
   *
   * If i > j
   * \verbatim
   | 0 | ... |  j  |   j+1   |   ...   |   i-1   |    i    | ...
   +---+-----+-----+---------+---------+---------+---------+-----
   | ? |     | p_j | p_{j+1} |         | p_{i-1} |   p_i   |
   \endverbatim
   * becomes
   * \verbatim
   | 0 | ... |  j  |   j+1   |   ...   |   i-1   |    i    | ...
   +---+-----+-----+---------+---------+---------+---------+-----
   | ? |     | p_i |   p_j   | p_{j+1} |         | p_{i-1} |
   \endverbatim
   *
   * @param it_i iterator on one valid position i in {1, ..., N}
   * @param it_j iterator on one valid position j in {1, ..., N}, i != j
   */
  inline
  void
  permutation_insert(permutation_type::iterator it_i,
                     permutation_type::iterator it_j) {
    assert(it_i != it_j);

    const permutation_item_type item_i = *it_i;

    if (it_i < it_j) {
      std::move(it_i + 1, it_j + 1, it_i);
    }
    else {
      std::move_backward(it_j, it_i, it_i + 1);
    }

    *it_j = item_i;
  }


  /** \brief
   * Print the permutation to out.
   *
   * @param permutation properly initialized
   * @param out
   */
  void
  permutation_print(const permutation_type &permutation,
                    std::ostream& out = std::cout);


  /** \brief
   * Print the permutation like permutation_print()
   * but with an end of line.
   *
   * @param permutation properly initialized
   * @param out
   */
  void
  permutation_println(const permutation_type &permutation,
                      std::ostream& out = std::cout);


  /** \brief
   * Reverse the permutation.
   *
   * \verbatim
   | 0 |  1  |    2    |    3    | ... |  N  |
   +---+-----+---------+---------+-----+-----+
   | ? | p_N | p_{n-1} | p_{n-2} | ... | p_1 |
   \endverbatim
   *
   * @param permutation properly initialized
   */
  inline
  void
  permutation_reverse(permutation_type &permutation) {
    assert(permutation.size() > 1);

    std::reverse(permutation.begin() + 1, permutation.end());
  }


  /** \brief
   * Set all values to make a identity permutation.
   *
   * \verbatim
   | 0 | 1 | 2 | 3 | ... | N |
   +---+---+---+---+-----+---+
   | ? | 1 | 2 | 3 | ... | N |
   \endverbatim
   *
   * @param permutation properly initialized
   */
  void
  permutation_set_identity(permutation_type &permutation);


  /** \brief
   * Set all values by random.
   *
   * \verbatim
   | 0 |  1  |  2  |  3  | ... |  N  |
   +---+-----+-----+-----+-----+-----+
   | ? | p_1 | p_2 | p_3 | ... | p_N |
   \endverbatim
   * where \f$\{p_i\}_{1\leq i\leq n}\ \f$
   * is a random permutation of \f$\{i\}_{1\leq i\leq n}\f$
   *
   * @param permutation properly initialized
   */
  void
  permutation_set_random(permutation_type &permutation);


  /** \brief
   * Swap two consecutive items.
   *
   * \verbatim
   | 0 | ... |    i    |   i+1   | ...
   +---+-----+---------+---------+-----
   | ? |     |   p_i   | p_{i+1} |
   \endverbatim
   * becomes
   * \verbatim
   | 0 | ... |    i    |   i+1   | ...
   +---+-----+---------+---------+-----
   | ? |     | p_{i+1} |   p_i   |
   \endverbatim
   *
   * @param it_i iterator on one valid position i in {1, ..., N - 1}
   */
  inline
  void
  permutation_transpose(permutation_type::iterator it_i) {
    std::swap(*it_i, *(it_i + 1));
  }

}  // namespace pfsp

#endif  // SRC_PFSP_PERMUTATION_HPP_
