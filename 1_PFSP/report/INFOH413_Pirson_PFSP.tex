% !TeX spellcheck = en
% -*- coding: utf-8 -*-
\documentclass[a4paper,ps2pdf,11pt]{article}
\usepackage{report}

\def\EMAILOPi{opi@opimedia.be}

\newcommand{\emailOPi}{\href{mailto:\EMAILOPi?subject=[INFO-H413 ex 1]}{\color{email}\path|\EMAILOPi|}}

\newcommand{\course}{INFO-H413 Heuristic optimization}

\title{Implementation exercise 1\\
  \rule{\linewidth}{0.5mm}\\[1ex]
  {\Huge Permutation Flow Shop\\
    Scheduling Problem\\
    (PFSP)}\\
  \rule{\linewidth}{0.5mm}\\[3ex]
  Report}

\author{\begin{tabular}{r@{ }c@{ }l}%
    \href{http://www.opimedia.be/}{\color{black}\bfseries{Olivier \textsc{Pirson}}} & -- & \emailOPi
\end{tabular}}

\date{April 4, 2017}

\hypersetup{pdfauthor={Olivier Pirson <\EMAILOPi>}}
\hypersetup{pdftitle={INFO-H413 -- Implementation exercise 1 -- Permutation Flow Shop Scheduling Problem (PFSP)}}
\hypersetup{pdfsubject={Iterative improvement algorithms implementation for the Permutation Flow Shop Scheduling Problem (PFSP).}}
\hypersetup{pdfkeywords={heuristic,optimization,scheduling,PFSP,improvement,neighborhood,transpose,exchange,insert,VND}}

\lhead{INFO-H413 -- Implementation exercise 1 -- PFSP}
\rhead{Olivier \textsc{Pirson}}

\hyphenpenalty=10000
\sloppy

\setlength{\leftmargini}{1em}

\begin{document}
\begin{titlepage}%
  \newgeometry{margin=3cm,left=0cm,right=0cm}\center
  \textsc{\LARGE Université Libre de Bruxelles}\\[9ex]
  \textsc{\Large Computer Science Department}\\[2ex]
  \textsc{\Large\course}\\[2ex]

  \bfseries{\LARGE\makeatletter\@title\makeatother}\\[6ex]

  \vspace*{3ex}%
  \Large\makeatletter\@author\makeatother\\[3ex]
  \vfill
  \large\makeatletter\@date\makeatother
  \vfill
  \includegraphics[width=4cm,height=4cm]{img/ULB}%
  \vfill
\end{titlepage}
\restoregeometry



\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}%
Implementation of various
iterative improvement algorithms for the Permutation Flow Shop Scheduling Problem (PFSP).


\section*{Exercise 1.1 -- Iterative improvement algorithms}
\addcontentsline{toc}{section}{Exercise 1.1 -- Iterative improvement algorithms}%
\subsection*{1. Average results}
\addcontentsline{toc}{subsection}{1. Average results}%
\enlargethispage{1\baselineskip}%
For each combination algorithm,
the average deviation (in \%)
and the average computation time (in second),
calculated on all instances.
These tables 1 and 2 summary all the \textbf{12 combination algorithms} for exercise 1.1,
and also all the 4 combination VND algorithms for exercise 1.2.

\begin{table}[H]
  \begin{tabular}{|lll|r|r|}
    \hline
    Initial solution & Pivoting rule & Neighborhood & \textbf{Deviation} & Time\\
    \hline
    \input{../stats/average.tex}
    \hline
  \end{tabular}
  \caption{Average deviation (in \%) and average computation time (in second)\\
    sorted by average deviation.}
\end{table}

\begin{table}[H]
  \begin{tabular}{|lll|r|r|}
    \hline
    Initial solution & Pivoting rule & Neighborhood & Deviation & \textbf{Time}\\
    \hline
    \input{../stats/average_time.tex}
    \hline
  \end{tabular}
  \caption{Average deviation (in \%) and average computation time (in second)\\
    sorted by average computation time.}
\end{table}


\subsection*{2. Statistical analyze}
\addcontentsline{toc}{subsection}{2. Statistical tests}%
Significance level $\alpha$ choiced: 0.01.

\subsubsection*{(i) Initial solutions analyze}
\addcontentsline{toc}{subsubsection}{(i) Initial solutions analyze}%
p-values for \textsc{Student} and \textsc{Wilcoxon} statistical tests on deviations and computation times of
\textbf{random vs simplified RZ} initial solutions:\\
\begin{tabular}{|l|l||l|l||l|l|}
  \hline
  & & \multicolumn{2}{|c||}{Deviation} & \multicolumn{2}{|c|}{Time}\\
  \hline
  Pivoting rule & Neighborhood & \textsc{Student} & \textsc{Wilcoxon} & \textsc{Student} & \textsc{Wilcoxon}\\
  \hline
  first & transpose & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  first & exchange & 0.003374 & 0.007290 & 0.008282 & 0.003513\\
  first & insert & 0.000001 & 0.000002 & 0.000001 & $\sim$0\\
  \hline
  best & transpose & $\sim$0 & $\sim$0 & \textbf{0.091547} & \textbf{0.033074}\\
  best & exchange & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  best & insert & 0.000001 & 0.000004 & $\sim$0 & $\sim$0\\
  \hline
\end{tabular}

\medskip
The random and simplified RZ initial solutions
have statically different qualities for all combinations.

Also for computation times, except with best -- transpose.

\medskip
The \textbf{simplified RZ initial solution is preferable}
because it gives systematically a lesser deviation (see table 1).
It is also deterministic.


\subsubsection*{(ii) Pivoting rules analyze}
\addcontentsline{toc}{subsubsection}{(ii) Pivoting rules analyze}%
p-values for \textsc{Student} and \textsc{Wilcoxon} statistical tests on deviations and computation times of
\textbf{first vs best} pivoting rules:\\
\begin{tabular}{|l|l||l|l||l|l|}
  \hline
  & & \multicolumn{2}{|c||}{Deviation} & \multicolumn{2}{|c|}{Time}\\
  \hline
  Initial solution & Neighborhood & \textsc{Student} & \textsc{Wilcoxon} & \textsc{Student} & \textsc{Wilcoxon}\\
  \hline
  random & transpose & 0.000006 & 0.000035 & $\sim$0 & $\sim$0\\
  random & exchange & 0.000026 & 0.000056 & $\sim$0 & $\sim$0\\
  random & insert & \textbf{0.052420} & \textbf{0.077883} & $\sim$0 & $\sim$0\\
  \hline
  simplified RZ & transpose & \textbf{0.858615} & \textbf{0.659201} & \textbf{0.204281} & \textbf{0.110984}\\
  simplified RZ & exchange & \textbf{0.953812} & \textbf{0.904053} & $\sim$0 & $\sim$0\\
  simplified RZ & insert & \textbf{0.260386} & \textbf{0.234479} & $\sim$0 & $\sim$0\\
  \hline
\end{tabular}

\medskip
The first and best pivoting rules have statically different qualities
for random -- transpose, and for random -- exchange.

The computation times are statically different, except for simplified RZ -- transpose.

The table 1 shows also that deviation are similar.
But slightly lesser for first pivoting rule.
And mainly this rule is faster.
So the \textbf{first pivoting rule is preferable}.


\pagebreak
\subsubsection*{(iii) Neighborhoods analyze}
\addcontentsline{toc}{subsubsection}{(iii) Neighborhoods analyze}%
p-values for \textsc{Student} and \textsc{Wilcoxon} statistical tests on deviations and computation times of
\textbf{transpose vs exchange vs insert} neighborhoods:\\
\begin{tabular}{|l|l|l||l|l||l|l|}
  \hline
  & & & \multicolumn{2}{|c||}{Deviation} & \multicolumn{2}{|c|}{Time}\\
  \hline
  Initial solution & Pivoting rule & Neighborhoods tested & \textsc{Student} & \textsc{Wilcoxon} & \textsc{Stud.} & \textsc{Wil.}\\
  \hline
  random & first & transpose vs exchange & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  random & first & transpose vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  random & first & exchange vs insert & 0.000014 & 0.000042 & $\sim$0 & $\sim$0\\
  \hline
  random & best & transpose vs exchange & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  random & best & transpose vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  random & best & exchange vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  \hline
  simplified RZ & first & transpose vs exchange & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  simplified RZ & first & transpose vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  simplified RZ & first & exchange vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  \hline
  simplified RZ & best & transpose vs exchange & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  simplified RZ & best & transpose vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  simplified RZ & best & exchange vs insert & $\sim$0 & $\sim$0 & $\sim$0 & $\sim$0\\
  \hline
\end{tabular}

\medskip
All neighborhoods have statically different qualities and computation times.

\medskip
The table 1 shows that the insert neighborhood give the better solutions.
It seems obvious because it is the larger neighborhood.
Next exchange is the better. And finally transpose, the smaller neighborhood.

\medskip
For the computation time that is the opposite.

\medskip
With instances of test, the implementation is always very fast,
so the \textbf{insert neighborhood is preferable}.
But on bigger instances, that could become too slow.



\section*{Exercise 1.2 -- Variable Neighborhood Descent (VND)}
\addcontentsline{toc}{section}{Exercise 1.2 -- Variable Neighborhood Descent (VND)}%
\subsection*{1 Average results}
\addcontentsline{toc}{subsection}{1 Average results}%
See table 1 and 2 page 1 for average deviations and computation times.


\subsection*{2 Statistical analyze}
\addcontentsline{toc}{subsection}{2 Statistical analyze}%
p-values for \textsc{Student} and \textsc{Wilcoxon} statistical tests on deviations and computation times of
\textbf{random vs simplified RZ} initial solutions,
with first pivoting rule:\\
\begin{tabular}{|l||l|l||l|l|}
  \hline
  & \multicolumn{2}{|c||}{Deviation} & \multicolumn{2}{|c|}{Time}\\
  \hline
  Neighborhood & \textsc{Student} & \textsc{Wilcoxon} & \textsc{Student} & \textsc{Wilcoxon}\\
  \hline
  VND transpose-exchange-insert & \textbf{0.016708} & \textbf{0.010523} & 0.000342 & 0.000008\\
  VND transpose-insert-exchange & \textbf{0.016708} & \textbf{0.010523} & 0.006452 & 0.000057\\
  \hline
\end{tabular}

\medskip
With a significance level $\alpha$ = 0.01, statistical tests are not conclusive.

\smallskip
With a significance level $\alpha$ = 0.05,
random and simplified RZ initial solutions
have statically different qualities for all combinations.

\medskip
In both cases, computation times are statically different.

\medskip
The table 1 page 1 shows that simplified RZ initial solution give a lesser deviation
and is faster.
So the \textbf{simplified RZ initial solution} is preferable.


\pagebreak
\noindent
p-values for \textsc{Student} and \textsc{Wilcoxon} statistical tests on deviations and computation times of
\textbf{VND transpose-exchange-insert vs VND transpose-insert-exchange} neighborhoods,
with first pivoting rule:\\
\begin{tabular}{|l||l|l||l|l|}
  \hline
  & \multicolumn{2}{|c||}{Deviation} & \multicolumn{2}{|c|}{Time}\\
  \hline
  Initial solution & \textsc{Student} & \textsc{Wilcoxon} & \textsc{Student} & \textsc{Wilcoxon}\\
  \hline
  random & NaN & NaN & 0.000002 & 0.000003\\
  simplified RZ & NaN & NaN & 0.009473 & 0.001734\\
  \hline
\end{tabular}

\medskip
The statistical tests (in R language) failed for deviations.
The individuals results of deviation are identical.
The table 1 page 1 show also identical averages.

\medskip
Computation times are statically different.
But the table 1 shows that are very similar.

\medskip
Probably the \textbf{VND transpose-exchange-insert is preferable}.
That order begin with the smaller neighborhood,
then a bigger neighborhood,
and finally the bigger neighborhood.


\subsection*{3 Conclusion}
\addcontentsline{toc}{subsection}{3 Conclusion}%
The general better combination seems to be
simplified RZ initial solution with VND transpose-exchange-insert neighborhood.
That ensures a good initial solution,
that improved successively with increasing neighborhoods.
That gives good final solutions and fast computation time.

\medskip
On the test instances, local optimums are fairly quickly reached, regardless of methods.
Bigger instances would be useful to analyze more precisely.

\medskip
An important improvement would be to be able to leave from local optimums.



\section*{Appendices}
\addcontentsline{toc}{section}{Appendices}%
\subsection*{Implementation}
\addcontentsline{toc}{subsection}{Implementation}%
All algorithms are implemented in C++. Sources are available in \texttt{src/} directory.
In this directory are available these commands (\textit{among others}):
\begin{itemize}
\item
  \texttt{make ndebug all}
  --- compil all in \textbf{production mode}
  (before that, do \texttt{make distclean} to be sure to erase all development mode elements)
\item
  \texttt{make all}
  --- compil all in \textit{development mode} (a lot of assertions check code correction)
\item
  \texttt{make tests}
  --- compil and run \textit{partial} unit tests (require \href{http://cxxtest.com/}{CxxTest})

\bigskip
\item
  \texttt{pfsp\_stats}
  --- (after compilation) \textbf{main program} to produce statistics on one instance (see \texttt{pfsp\_stats --help}).
  Some useful informations are printed to the standard error stream,
  and main informations
  (filename, display precision, calculation time, weighted sum completion times)
  are printed to the standard output stream.
\item
  \texttt{pfsp\_solver}
  --- (after compilation) other program to solve one instance (see \texttt{pfsp\_solver --help})

\bigskip
\item
  \texttt{build\_stats\_1\_all.sh}
  --- (after compilation) script to rebuild all statistics for exercise 1.1
\item
  \texttt{build\_stats\_2\_all.sh}
  --- (after compilation) script to rebuild all statistics for exercise 1.2

\bigskip
\item
  \texttt{make analyse}
  --- run the R script \texttt{stats.r}
  that does \textsc{Student} and \textsc{Wilcoxon} statistical tests
  and writes results to \texttt{stats\_test\_results.txt}
\end{itemize}

A automatically generated source code HTML documentation
is available in \texttt{src/docs/html/} directory.

In the \texttt{graph/} directory, a little Python program \texttt{complete.py}
was used to complete some data and graphs from C++ results.


\subsubsection*{Code optimizations implemented}
\addcontentsline{toc}{subsubsection}{Code optimizations implemented}%
STL iterators on line of jobs
and on elements for each line are used,
instead systematic matrix access as \texttt{[i][j]}.

\medskip
To avoid complete calculation of weighted sum of completion times function,
the required table is stored in memory,
and only calculation are made from first modified line of jobs.

\medskip
Copies of structures (``matrix'' and array permutation)
are avoid.
When a possibility in the neighborhood is tried,
the permutation is directly modified (for example by an exchange($i$, $j$)).
If it must be rollback, the symmetric modification is perfomed
(for example again an exchange($i$, $j$)).
And finally, the calculation for the correction of the weighted sum of completion times function
is only performed during the next try.


\subsubsection*{Environment used}
\addcontentsline{toc}{subsubsection}{Environment used}%
All results was calculated on a Dell Precision T3500
with a processor Intel Xeon quad core W3520 \textbf{2,8 GHz}
(only one core was used by the implementation).

Operating system: Debian GNU/Linux 8 (Jessie) \textbf{64 bits}.

C++ compiler: \textbf{g++ 4.9.2}.


\subsection*{Cumulative frequency graphs}
\addcontentsline{toc}{subsection}{Cumulative frequency graphs}%
\mbox{}\\[-9ex]
\enlargethispage{1\baselineskip}%
\begin{figure}[H]
  \centering
  \null\hspace*{-2cm}%
  \includegraphics[width=0.6\textwidth]{../graph/cumul_freq_reduced.eps}%
  \includegraphics[width=0.6\textwidth]{../graph/cumul_freq.eps}
  \hspace*{-2cm}\null%
  \caption{Cumulative frequency of relative percentage deviation.\\
    {\small(The left graph is the most left part of the right graph.)}}
\end{figure}

\begin{figure}[H]
  \centering
  \null\hspace*{-2cm}%
  \includegraphics[width=0.6\textwidth]{../graph/cumul_time_reduced.eps}%
  \includegraphics[width=0.6\textwidth]{../graph/cumul_time.eps}
  \hspace*{-2cm}\null%
  \caption{Cumulative frequency of relative percentage deviation.\\
    {\small(The left graph is the most left part of the right graph.)}}
\end{figure}



\begin{thebibliography}{9}%
\addcontentsline{toc}{section}{References}%
\bibitem[\textsc{HO} 2017]{HO2017}
  Thomas \textsc{Stützle}.\\
  \textbf{\textit{Heuristic Optimization}}.\\
  2017,
  \hrefShow{http://iridia.ulb.ac.be/~stuetzle/Teaching/HO/}

\bibitem[\textsc{SLS} 2004]{SLS2004}
  Holger \textsc{Hoos},
  Thomas \textsc{Stützle}.\\
  \textbf{\textit{Stochastic Local Search: Foundations and Applications}}.\\
  Elsevier, 2004

\end{thebibliography}



\listoftodos

\tableofcontents

\bigskip
\null
\hrule

\end{document}
